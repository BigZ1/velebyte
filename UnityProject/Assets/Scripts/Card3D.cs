﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class Card3D : MonoBehaviour
{

    public TextMeshPro TitleText;
    public MeshRenderer ImageContainer;
    public TextMeshPro DescriptionText;
    public int Rarity;

    public string Title
    {
        get
        {
            return TitleText.text;
        }
        set
        {
            TitleText.text = value;
        }
    }

    public Texture2D Image
    {
        get
        {
            return (Texture2D)ImageContainer.material.mainTexture;
        }
        set
        {
            ImageContainer.material.mainTexture = value;
        }
    }

    public string Description {
        get
        {
            return DescriptionText.text;
        }
        set
        {
            DescriptionText.text = value;
        }
    }

}
