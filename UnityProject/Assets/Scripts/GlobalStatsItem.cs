﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GlobalStatsItem : MonoBehaviour
{

    public Text playerNameText;
    public Text playerELOText;

    public string PlayerName
    {
        get => playerNameText.text;
        set => playerNameText.text = value;
    }

    public string ELO
    {
        get => playerELOText.text;
        set => playerELOText.text = value;
    }

}
