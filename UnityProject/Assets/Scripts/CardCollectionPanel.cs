﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardCollectionPanel : MonoBehaviour
{

    public GameObject holder;

    public GameObject cardsContainer;

    public CardUI CommonCard;
    public CardUI RareCard;
    public CardUI EpicCard;
    public CardUI LegendaryCard;

    public static CardCollectionPanel main;

    private void Awake()
    {
        main = this;
    }

    private void Start()
    {
        main = this;
    }

    private void OnEnable()
    {
        main = this;
    }

    public void Close()
    {
        holder.SetActive(false);
        Utils.ClearChildren(cardsContainer.transform);
    }

    public void Show()
    {
        holder.SetActive(true);
        IzbrisiKorisnikaDto req = new IzbrisiKorisnikaDto();
        req.korisnickoIme = User.main.username;
        Client.main.GetPlayerCards(req.GetJSON());
    }

    public static GameObject AddNewCard(int rarity, string name, Texture2D image, string description, Transform container = null)
    {

        CardUI card = null;

        if (main == null)
            main = FindObjectOfType<CardCollectionPanel>(true);

        switch(rarity)
        {
            case 2:
                card = Instantiate(main.RareCard, container ? container : main.cardsContainer.transform);
                break;
            case 3:
                card = Instantiate(main.EpicCard, container ? container : main.cardsContainer.transform);
                break;
            case 4:
                card = Instantiate(main.LegendaryCard, container ? container : main.cardsContainer.transform);
                break;
            default:
                card = Instantiate(main.CommonCard, container ? container : main.cardsContainer.transform);
                break;
        }

        card.Title = name;
        card.Image = image;
        card.Description = description;

        return card.gameObject;
    } 

    public void ShowCard(string data)
    {
        KartaInfoDto ki = null;
        try
        {
            ki = JsonUtility.FromJson<KartaInfoDto>(data);
        } catch(Exception e)
        {
            UIController.ShowGameMessage(e.Message);
            return;
        }
        if(ki != null)
        {
            Texture2D tex = null;
            try
            {
                tex = Utils.Base64ToTexture(ki.slika);
            } catch(Exception e)
            {
                UIController.ShowGameMessage(e.Message);
            }
            AddNewCard((int)ki.raritet, ki.imeKarte, tex, ki.opis);
        }
    }

}
