﻿using Mapbox.Unity.Map;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCamera : MonoBehaviour
{

    public enum CameraStates
    {
        Idle,
        Playing,
        LookAtObjects
    }

    public StateMachine<CameraStates> CameraState;

    public Transform target;

    public Vector3 offset = Vector3.zero;

    public float smoothTime = 0.2f;
    public float lookAtSmoothTime = 0.1f;

    public float rotateAroundSpeed = 120f;

    public float yAngle = 0f;

    public float rotateAroundAreaYMin = 0.2f;
    public float rotateAroundAreaYMax = 0.8f;

    public static PlayerCamera main;

    private void Awake()
    {
        CameraState = new StateMachine<CameraStates>(CameraStates.Idle, new System.Action[] {
                Idle,
                Playing,
                LookAtObjects
            }
        );
        main = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        
        if(offset == Vector3.zero)
        {
            offset = transform.position - target.position;
        }
    }

    Vector3 vel;
    Vector3 velr;
    // Update is called once per frame
    void LateUpdate()
    {
        CameraState.Execute();
        
    }

    void Idle()
    {
        Vector3 targetPos = Quaternion.Euler(0, yAngle, 0) * offset + target.position;
        transform.position = Vector3.SmoothDamp(transform.position, targetPos, ref vel, smoothTime);
        transform.eulerAngles = Utils.SmoothDampAngles(transform.eulerAngles, Quaternion.LookRotation(target.position - transform.position).eulerAngles, ref velr, lookAtSmoothTime);
    }

    void Playing()
    {
        //yAngle += Input.GetAxisRaw("Horizontal") * rotateAroundSpeed * Time.deltaTime;
        for (int i = 0; i < Input.touchCount; i++)
        {
            Touch t = Input.touches[i];
            float tRelYPos = t.position.y / Screen.height;
            if(tRelYPos >= rotateAroundAreaYMin && tRelYPos <= rotateAroundAreaYMax && t.phase == TouchPhase.Moved)
            {
                yAngle += (t.deltaPosition.x / Screen.width) * rotateAroundSpeed * Time.deltaTime;
                break;
            } 
        }
        Vector3 targetPos = Quaternion.Euler(0, yAngle, 0) * offset + target.position;
        transform.position = Vector3.SmoothDamp(transform.position, targetPos, ref vel, smoothTime);
        transform.eulerAngles = Utils.SmoothDampAngles(transform.eulerAngles, Quaternion.LookRotation(target.position - transform.position).eulerAngles, ref velr, lookAtSmoothTime);
    }

    public List<GameObject> targetObjects = new List<GameObject>();

    public Vector3 FitToViewTargetObjects(Vector3 offsetDirection, out Bounds b)
    {

        Vector3 res = Vector3.zero;
        b = new Bounds();

        if (targetObjects == null || targetObjects.Count == 0)
        {
            return transform.position;
        }

        if(targetObjects.Count == 1 && targetObjects[0] != null)
        {
            b.center = targetObjects[0].transform.position;
            b.size = Vector3.one;
            return targetObjects[0].transform.position + offsetDirection.normalized * 4;
        }

        if(targetObjects[0] == null)
        {
            //transform.rotation = Quaternion.LookRotation(Vector3.down);
            return transform.position;
        }

        b.center = targetObjects[0].transform.position;
        for(int i = 0; i < targetObjects.Count; i++)
        {
            if (targetObjects[i] != null)
                b.Encapsulate(targetObjects[i].transform.position);
        }

        //Gizmos.DrawCube(b.center, b.size);
        res += b.center + offsetDirection.normalized * Mathf.Clamp(Mathf.Pow(b.size.sqrMagnitude, 1 / 1.5f), 25f, 400f);

        return res;
    }

    void LookAtObjects()
    {
        Bounds b;
        Vector3 t = FitToViewTargetObjects(offsetDir, out b);
        transform.position = Vector3.SmoothDamp(transform.position, t, ref vel, smoothTime);
        if (targetObjects.Count != 0)
        {
            transform.eulerAngles = Utils.SmoothDampAngles(transform.eulerAngles, Quaternion.LookRotation(b.center - transform.position).eulerAngles, ref velr, lookAtSmoothTime);
        } else
        {
            transform.rotation = Quaternion.LookRotation(Vector3.down);
        }
    }

    public Vector3 offsetDir = new Vector3(0, 1, -1);
    /*private void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        if(targetObjects != null && targetObjects.Count > 0)
        {
            Bounds bb;
            Vector3 pos = FitToViewTargetObjects(offsetDir, out bb);
            //print(pos);
            Gizmos.DrawCube(bb.center, bb.size);
            Gizmos.color = Color.green;
            Gizmos.DrawSphere(pos, 0.5f);
        }
    }*/

}
