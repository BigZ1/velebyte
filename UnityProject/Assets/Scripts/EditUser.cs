﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class EditUser : MonoBehaviour
{

    public GameObject holder;

    public Text playerNameText;
    public InputField usernameEdit;
    public InputField emailEdit;
    public InputField passwordEdit;
    public Image profileImageEdit;
    public InputField ELOEdit;
    public Toggle verifiedEdit;
    public InputField IBANEdit;
    public Image ICCardImageEdit;
    public Dropdown roleEdit;

    public void Close()
    {
        playerNameText.text = null;
        usernameEdit.text = null;
        emailEdit.text = null;
        //passwordEdit.text = null;
        profileImageEdit.sprite = null;
        ELOEdit.text = null;
        verifiedEdit.isOn = false;
        IBANEdit.text = null;
        ICCardImageEdit.sprite = null;
        roleEdit.SetValueWithoutNotify(0);
        holder.SetActive(false);
    }

    public void EditUserData()
    {
        UrediKorisnikaPodaciDto data = getData();
        if (data == null)
            return;
        Client.main.EditUser(data.GetJSON());
    }

    public void Show(UrediKorisnikaPodaciDto data)
    {
        if(data.korisnickoIme == "user deleted")
        {
            UIController.ShowGameMessage("User was already deleted");
            return;
        }



        playerNameText.text = data.korisnickoIme;
        usernameEdit.text = data.korisnickoIme;
        emailEdit.text = data.email;
        passwordEdit.text = data.password;
        profileImageEdit.sprite = Utils.Base64ToSprite(data.urlFotografije);
        ELOEdit.text = data.elo.ToString();
        verifiedEdit.isOn = data.verified;
        IBANEdit.text = data.iban;
        ICCardImageEdit.sprite = Utils.Base64ToSprite(data.slikaOsobne);
        roleEdit.SetValueWithoutNotify(data.ulogaKorisnika);

        holder.SetActive(true);

    }

    UrediKorisnikaPodaciDto getData()
    {

        UrediKorisnikaPodaciDto data = new UrediKorisnikaPodaciDto();

        data.originalnoKorisnickoIme = playerNameText.text;

        if (!string.IsNullOrEmpty(usernameEdit.text))
            data.korisnickoIme = usernameEdit.text;
        else
        {
            UIController.main.ShowError("New username cannot be empty");
            return null;
        }

        if (!string.IsNullOrEmpty(emailEdit.text))
            if (Register.EmailValidator.EmailIsValid(emailEdit.text))
                data.email = emailEdit.text;
            else
            {
                UIController.main.ShowError("New email is invalid");
                return null;
            }
        else
        {
            UIController.main.ShowError("New email cannot be empty");
            return null;
        }

        if(profileImageEdit.sprite != null)
        {
            string img = Utils.TextureToBase64(profileImageEdit.sprite.texture);
            data.urlFotografije = img;
            //data.urlFotografije = "no img";

        } else
        {
            data.urlFotografije = "no img";
            //TODO: add actual image?
        }

        if (!string.IsNullOrEmpty(ELOEdit.text))
            data.elo = int.Parse(ELOEdit.text);
        else
        {
            UIController.main.ShowError("New ELO cannot be empty");
            return null;
        }

        data.verified = verifiedEdit.isOn;

        if (roleEdit.value == 1)
        {
            if (!string.IsNullOrEmpty(IBANEdit.text) && IBANValidator.ValidateBankAccount(IBANEdit.text))
                data.iban = IBANEdit.text;
            else
            {
                UIController.main.ShowError("New IBAN is invalid");
                return null;
            }
        }

        if (ICCardImageEdit.sprite != null)
        {
            string img = Utils.TextureToBase64(ICCardImageEdit.sprite.texture);
            //TODO:
            data.slikaOsobne = img;
            //data.urlFotografije = "no img";

        }
        else
        {
            //TODO: add actual image?
            data.slikaOsobne = "no img";
        }

        data.ulogaKorisnika = roleEdit.value;

        return data;

    }

}
