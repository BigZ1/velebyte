﻿using Mapbox.Utils;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class Battle : MonoBehaviour
{

    public static Battle main;

    public CardUI[] playerCards;

    public GameObject cardUIHolder;

    public bool CanThrow = false;

    public Text messageText;

    public GameObject messagesHolder;

    public MapCardShow cardShower;

    public KartaInfoDto[] playerCardInfos;
    public KartaInfoDto[] enemyCardInfos;

    public List<CardUI> playerThrownCards = new List<CardUI>();
    public List<CardUI> enemyThrownCards = new List<CardUI>();

    public Text ConstantMessage;

    public PolygonDrawer polygonDrawerP1;
    public PolygonDrawer polygonDrawerP2;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if(playerThrownCards.Count + enemyThrownCards.Count > 0)
        {
            AdjustMapZoomAndCenter();
            cardShower.Reposition();
            if(playerThrownCards.Count >= 3)
            {
                Vector2d[] arr = new Vector2d[playerThrownCards.Count];
                for(int i = 0; i < arr.Length; i++)
                {
                    arr[i] = new Vector2d(playerThrownCards[i].lng, playerThrownCards[i].lat);
                }
                polygonDrawerP1.DrawPoints(arr, 0.5f);
            }
            if (enemyThrownCards.Count >= 3)
            {
                Vector2d[] arr = new Vector2d[enemyThrownCards.Count];
                for (int i = 0; i < arr.Length; i++)
                {
                    arr[i] = new Vector2d(enemyThrownCards[i].lng, enemyThrownCards[i].lat);
                }
                polygonDrawerP1.DrawPoints(arr, 0.25f);
            }
        }
    }

    IEnumerator Checker()
    {
        while(true)
        {
            BorbaPodaciDto b = new BorbaPodaciDto();
            b.kod = "INFO";
            b.posiljatelj = User.main.username;
            Client.main.BattleGet(b.GetJSON());
            yield return new WaitForSeconds(1f);
        }
    }

    public void Begin()
    {
        StartCoroutine(Checker());
    }

    private void OnDisable()
    {
        StopAllCoroutines();
    }

    public void TakeCards(CardUI[] cards)
    {
        playerCards = cards;
        for(int i = 0; i < playerCards.Length; i++)
        {
            playerCards[i].transform.SetParent(cardUIHolder.transform);
            playerCards[i].Selectable = true;

            playerCards[i].OnSelect.RemoveAllListeners();
            playerCards[i].OnSelect.AddListener(ThrowCard);
        }
    }

    public void ThrowCard(CardUI card)
    {
        if(CanThrow)
        {
            CanThrow = false;
            Vector2d point = new Vector2d(card.lng, card.lat);
            GameObject c = cardShower.PlaceOn(point, card.name, card.Image, card.Description, card.Rarity);
            PlayerCamera.main.targetObjects.Add(c);
            AdjustMapZoomAndCenter();
            cardShower.Reposition();
            playerThrownCards.Add(card);

            card.gameObject.SetActive(false);

        } else
        {
            ShowMessage("You cannot throw cards now");
        }
    }

    public void AdjustMapZoomAndCenter()
    {
        Vector2d[] targets = new Vector2d[playerThrownCards.Count + enemyThrownCards.Count];

        for(int i = 0; i < playerThrownCards.Count; i++)
        {
            targets[i] = new Vector2d(playerThrownCards[i].lng, playerThrownCards[i].lat);
        }

        for(int i = playerThrownCards.Count; i < playerThrownCards.Count + enemyThrownCards.Count; i++)
        {
            targets[i] = new Vector2d(enemyThrownCards[i - playerThrownCards.Count].lng, enemyThrownCards[i - playerThrownCards.Count].lat);
        }

        float zoom = (float)SceneController.main.CalculateMapZoomToFit(targets);
        SceneController.Map.SetZoom(zoom);
        SceneController.Map.SetCenterLatitudeLongitude(SceneController.main.getCenterOfPoints(targets));
        SceneController.Map.UpdateMap();
    }

    public void ShowMessage(string message, float time = 3f)
    {
        Text t = Instantiate(messageText, messagesHolder.transform);
        t.text = message;

        StartCoroutine(ShowMessageTimeout(t, time));

    }

    IEnumerator ShowMessageTimeout(Text t, float time, float timeout = 0.2f)
    {

        t.gameObject.SetActive(true);
        yield return new WaitForSeconds(time);

        float to = 0;

        while(to <= timeout)
        {
            Color c = t.color;
            c.a = 1f - to / timeout;
            t.color = c;
            to += Time.deltaTime;
            yield return null;
        }

        Destroy(t.gameObject);

    }

    public void GetData(BorbaPodaciDto data)
    {
        Client.main.BattleGet(data.GetJSON());
    }

    public void ProcessData(BorbaPodaciDto data)
    {
        print("Got borba data: " + data);
        if(data.kod == "WAIT")
        {
            ConstantMessage.text = "Waiting for other player";
        }

    }

}
