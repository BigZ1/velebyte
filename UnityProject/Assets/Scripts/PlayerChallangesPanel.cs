﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerChallangesPanel : MonoBehaviour
{

    public GameObject holder;

    public GameObject contentHolder;

    public GameObject challengeItem;


    public void Show(IgraceviIzazoviDto ch)
    {
        holder.SetActive(true);

        if(ch.korisnickaImena == null)
                return;

        for(int i = 0; i < ch.korisnickaImena.Length; i++)
        {
            GameObject g = Instantiate(challengeItem, contentHolder.transform);
            PlayerChallengeItem pci = g.GetComponent<PlayerChallengeItem>();
            pci.playerName = ch.korisnickaImena[i];
            pci.playerNameText.text = ch.korisnickaImena[i];
        }

    }

    public void Close()
    {
        holder.SetActive(false);
        Utils.ClearChildren(contentHolder.transform);
    }

}
