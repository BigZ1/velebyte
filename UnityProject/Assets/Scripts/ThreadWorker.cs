﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class ThreadWorker<T>
{

    public delegate T DoFunction();

    public int SleepInterval = 10;

    Queue<DoFunction> ToDoList = new Queue<DoFunction>();

    Mutex QueueLock = new Mutex();

    Mutex ResultsMutex = new Mutex();

    Queue<T> Results = new Queue<T>();

    public volatile bool Active = true;

    void Work()
    {
        ManualResetEvent mre = new ManualResetEvent(false);
        while (Active)
        {
            while(ToDoList.Count > 0)
            {
                QueueLock.WaitOne();
                DoFunction a = ToDoList.Dequeue();
                QueueLock.ReleaseMutex();
                T result = a.Invoke();
                ResultsMutex.WaitOne();
                Results.Enqueue(result);
                ResultsMutex.ReleaseMutex();
            }
            mre.WaitOne(SleepInterval);
        }
    }

    public ThreadWorker()
    {
        Thread t = new Thread(Work);
        t.Start();
    }

    public void AddWork(DoFunction a)
    {
        QueueLock.WaitOne();
        ToDoList.Enqueue(a);
        QueueLock.ReleaseMutex();
    }

    public T GetResult()
    {
        T res = default;
        ResultsMutex.WaitOne();
        if (Results.Count > 0)
        {
            res = Results.Dequeue();
        }
        ResultsMutex.ReleaseMutex();
        return res;
    }

    public void StopWorking()
    {
        Active = false;
    }

    ~ThreadWorker()
    {
        Active = false;
    }


}
