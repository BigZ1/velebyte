﻿using JetBrains.Annotations;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{

    public Canvas mainCanvas;

    public enum UIHolderType
    {
        Login,
        Register,
        StartUI,
        InGame,
        Battle
    }

    [Serializable]
    public struct UIRefefencePair
    {
        public UIHolderType Type;
    }

    public GameObject[] UIHolders;

    public UIRefefencePair[] References;

    [Serializable]
    public struct ErrorUIData
    {
        public GameObject ErrorUIObject;
        public Text Title;
        public Text Message;
    }

    public ErrorUIData ErrorUI;

    public GameObject StackedErrorUIObject;

    [SerializeField]
    UIHolderType startUI = UIHolderType.Login;

    public StateMachine<UIHolderType> UIState;

    public GameObject AdministratorUI;
    public Dropdown AdminToolsDropdown;

    public GameObject CartographerUI;
    public Dropdown CartographerToolsDropdown;

    public CartographerLocationPanel CartographerLocationPanel;

    public CartographerLocationPicks CartographerPicksPanel;

    //public UIHolderType CurrentUI { get { return currentUI; } set { currentUI = value; SwitchUI(); } }

    public static UIController main;

    [Serializable]
    public struct CartographerApplyUI
    {
        public GameObject holder;
        public Text cartographerNameText;
        public Image identityCardImage;
    }

    public CartographerApplyUI ApplyCartographerUI;

    [Serializable]
    public struct PlayerList
    {

        public GameObject playerViewItem;
        public GameObject playerViewItemContainer;
        public GameObject holder;

        public void ShowList(String[] users)
        {
            holder.SetActive(true);
            for (int i = 0; i < users.Length; i++)
            {
                GameObject item = Instantiate(playerViewItem, playerViewItemContainer.transform);
                PlayerItemView it = item.GetComponent<PlayerItemView>();
                it.Init(users[i], users[i] == User.main.username);
            }
        }

        public void HideList()
        {
            holder.SetActive(false);
            Utils.ClearChildren(playerViewItemContainer.transform);
        }

    }

    public PlayerList ActivePlayersList;

    public PlayerList AllUserList;

    public EditUser editUserPanel;

    public BanUser banUserPanel;

    public DebugInfo debugInfo;

    public GlobalStatsPanel GlobalStatsPanel;

    public Button hideNearPlayersButton;

    public ChallengePanel challengePanel;

    public PlayerChallangesPanel challengesPanel;

    public PreBattlePanel preBattlePanel;

    [Serializable]
    public struct DetailedPlayerInfoPanel
    {
        public GameObject Holder;
        public Text korisnickoIme, ELO, exparation, ulogaKorisnika;

        public void ShowData(DetaljneInfoOdgovorDto info)
        {
            Holder.SetActive(true);
            //print("h");
            korisnickoIme.text = info.korisnickoIme;
            ELO.text = info.elo.ToString();
            exparation.text = string.IsNullOrEmpty(info.exparation) ? "Not banned" : "Until " + info.exparation;
            ulogaKorisnika.text = info.ulogaKorisnika;
        }

    }

    public DetailedPlayerInfoPanel detailedPlayerInfoPanel;

    private void Awake()
    {
        main = this;

        //CurrentUI = currentUI;
        UIState = new StateMachine<UIHolderType>(startUI);

        UIState.StateEnterMethods[UIHolderType.Login] = ToLogin;
        UIState.StateEnterMethods[UIHolderType.Register] = ToRegister;
        UIState.StateEnterMethods[UIHolderType.InGame] = ToPlayerUI;
        UIState.StateEnterMethods[UIHolderType.Battle] = ToBattleUI;

        UIState.StateExitMethods[UIHolderType.InGame] = FromPlayerUI;

        UIState.State = UIHolderType.Login;

    }

    #region SWITCH_METHODS

    void ToLogin()
    {
        SwitchUI(UIHolderType.Login);
    }

    void ToRegister()
    {
        SwitchUI(UIHolderType.Register);
    }

    void ToPlayerUI()
    {
        SwitchUI(UIHolderType.InGame);
        if (User.main.Type == User.UserType.Administrator)
            AdministratorUI.SetActive(true);
        if (User.main.Type == User.UserType.Cartographer || User.main.Type == User.UserType.Administrator)
            CartographerUI.SetActive(true);
    }

    void FromPlayerUI()
    {
        AdministratorUI.SetActive(false);
        CartographerUI.SetActive(false);
        CloseActivePlayers();
        CloseUsersList();
    }

    void ToBattleUI()
    {
        SwitchUI(UIHolderType.Battle);
        AdministratorUI.SetActive(false);
        CartographerUI.SetActive(false);
        CloseActivePlayers();
        CloseUsersList();
    }

    #endregion

    void SwitchUI(UIHolderType newUI)
    {
        DisableAllUI();
        int inx = -1;
        for(int i = 0; i < References.Length; i++)
        {
            if(References[i].Type == newUI)
            {
                inx = i;
            }
        }
        if(inx >= 0)
        {
            UIHolders[inx].SetActive(true);
        } else
        {
#if UNITY_EDITOR
            print("error finding UI object");
#endif
        }
    }

    public void LoadUIForUser(User.UserType type)
    {
        switch (type)
        {
            case User.UserType.Player:
                break;
            case User.UserType.Cartographer:
                CartographerUI.SetActive(true);
                break;
            case User.UserType.Administrator:
                AdministratorUI.SetActive(true);
                CartographerUI.SetActive(true);
                break;
            default:
                break;
        }
    }

    void DisableAllUI()
    {
        foreach(GameObject o in UIHolders)
        {
            o.SetActive(false);
        }
        ErrorUI.ErrorUIObject.SetActive(false);
    }

    public void ShowError(string message, string title = "Error", bool stacked = false)
    {
        if (!stacked)
        {
            ErrorUI.ErrorUIObject.SetActive(true);
            ErrorUI.Message.text = message;
            ErrorUI.Title.text = title;
        } else
        {
            GameObject obji = Instantiate(StackedErrorUIObject, mainCanvas.transform);
            StackedErrorUI obj = obji.GetComponent<StackedErrorUI>();
            obj.Message = message;
            obj.Title = title;
            obj.gameObject.SetActive(true);
            obj.transform.SetParent(mainCanvas.transform);
        }
    }

    public static void ShowGameMessage(string message, string title = "Error", bool stacked = false)
    {
        main.ShowError(message, title, stacked);
    }

    public void AcceptCartographer(bool accepted)
    {
        string cartographer = ApplyCartographerUI.cartographerNameText.text;
        if (!string.IsNullOrEmpty(cartographer) || cartographer.Contains(" "))
        {
            print("cartographer" + (accepted ? "accepted" : "rejected"));
            UACOdgovor odg = new UACOdgovor();
            odg.korisnickoIme = cartographer;
            odg.prihvacen = accepted;
            Client.main.AdministratorAcceptCartographer(odg.GetJSON());
        } else
        {
            UIController.ShowGameMessage("No cartographers to apply", "Game message");
        }

    }

    public void OpenAcceptCartographerUI()
    {
        Client.main.RequestUnapplyedCartographerData("body..");

        ApplyCartographerUI.holder.SetActive(true);
    }

    public void AdminToolSelect(int tool)
    {
        tool = AdminToolsDropdown.value;
        switch(tool)
        {
            case 1: //accept cartographers
                OpenAcceptCartographerUI();
                break;
            case 0: //close
                break;
            case 2: //user list
                Client.main.GetUserList();
                break;
            case 3:
                if(debugInfo)
                {
                    debugInfo.Toggle();
                }
                break;
            default:
                break;
        }
        AdminToolsDropdown.SetValueWithoutNotify(0);
    }

    public void CartographerToolSelect(int tool)
    {
        tool = CartographerToolsDropdown.value;
        switch(tool)
        {
            //close
            case 0:
                break;
            //confirm locations
            case 1:
                CartographerRequestLocation req = new CartographerRequestLocation();
                req.username = User.main.username;
                Client.main.RequestLocationInfo(req.GetJSON());
                break;
            //picked locaitons
            case 2:
                CartographerRequestLocation reqp = new CartographerRequestLocation();
                reqp.username = User.main.username;
                Client.main.GetCartographerPicks(reqp.GetJSON());
                break;
        }
        CartographerToolsDropdown.SetValueWithoutNotify(0);
    }

    public void ShowActivePlayers(String[] activeUsers)
    {

        ActivePlayersList.ShowList(activeUsers);

    }

    public void ShowUserList(String[] users)
    {

        AllUserList.ShowList(users);

    }

    public void CloseActivePlayers()
    {
        ActivePlayersList.HideList();
    }

    public void RefreshUsersList()
    {
        Utils.ClearChildren(AllUserList.playerViewItemContainer.transform);
        Client.main.GetUserList();
    }

    public void CloseUsersList()
    {
        AllUserList.HideList();
    }

    public void RefreshActivePlayersList()
    {
        Utils.ClearChildren(ActivePlayersList.playerViewItemContainer.transform);
        Client.main.GetActiveUsersList();
    }

    public void ShowDetailedPlayerInfo()
    {

    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

}
