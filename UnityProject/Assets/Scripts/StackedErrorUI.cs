﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StackedErrorUI : MonoBehaviour
{

    public GameObject holder;

    public Text TitleText;

    public Text MessageText;

    public string Title
    {
        get
        {
            return TitleText.text;
        }
        set
        {
            TitleText.text = value;
        }
    }

    public string Message
    {
        get
        {
            return MessageText.text;
        }
        set
        {
            MessageText.text = value;
        }
    }

    public void Close()
    {
        Destroy(holder);
    }

}
