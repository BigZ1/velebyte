﻿using Mapbox.Utils;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapCardShow : MapPointShow
{

    public GameObject commonCard;           //rarity = 0
    public GameObject rareCard;             //rarity = 1
    public GameObject epicCard;             //rarity = 2
    public GameObject legendaryCard;        //rarity = 3

    GameObject SpawnNewCard(string name, Texture2D image, string description, int rarity)
    {
        GameObject g = null;

        switch(rarity)
        {
            case 1:
                g = Instantiate(commonCard);
                break;
            case 2:
                g = Instantiate(rareCard);
                break;
            case 3:
                g = Instantiate(epicCard);
                break;
            default:
                g = Instantiate(legendaryCard);
                break;
        }

        Card3D c = g.GetComponent<Card3D>();

        c.Title = name;
        c.Image = image;
        c.Description = description;

        g.transform.parent = transform;

        return g;
    }

    public GameObject PlaceOn(Vector2d point, string name, Texture2D image, string description, int rarity)
    {
        if(!ContainsPosition(point))
        {
            Vector3 pos = map.GeoToWorldPosition(point, false);
            PlacePoint pp = new PlacePoint
            {
                Place = point,
                Object = SpawnNewCard(name, image, description, rarity)
            };
            pp.Object.transform.position = pos + placeOffset;
            pointObjects.Add(pp);
            return pp.Object;
        }
        return null;
    }

    private void OnDisable()
    {
        Utils.ClearChildren(transform);
        pointObjects.Clear();
    }

    private void Update()
    {
        /*if (Input.GetKeyDown(KeyCode.S))
        {
            PlaceOn(new Vector2d(60.19219, 24.9673538), "REEE", null, "REEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEEE", 3);
            PlaceOn(new Vector2d(60.191925, 24.9669418), "BAD", null, "BAD BAD BAD BAD BAD BAD BAD BAD", 0);
        }*/
    }

}
