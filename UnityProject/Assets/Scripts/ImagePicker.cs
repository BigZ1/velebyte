﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.UI;

public class ImagePicker : MonoBehaviour
{

    public Button button;

    public Image image;

    static string[] wantedFileTypes = new string[]
    {
        "png",
        "jpg",
        "jpeg"
    };

    static string[] fileTypes;

    //TODO: fix crashing bug!
    void PickImage()
    {
        try
        {

            NativeFilePicker.PickFile((string path) =>
            {
                Texture2D tex = new Texture2D(1, 1);
                if (path == null)
                {
                    return;
                }
                try
                {
                    byte[] imgBytes = File.ReadAllBytes(path);
                    if (imgBytes == null)
                    {
                        //error loading image
                        UIController.main.ShowError("Image loading failed");
                        return;
                    }

                    if(tex == null)
                    {
                        UIController.main.ShowError("Image loading failed");
                        return;
                    }

                    bool loadOk = ImageConversion.LoadImage(tex, imgBytes);

                    if (!loadOk || tex == null)
                    {
                        UIController.ShowGameMessage("Image loading failed");
                        return;
                    }

                    if (image.sprite)
                    {
                        Destroy(image.sprite);
                    }
                    image.sprite = Sprite.Create(tex, new Rect(0f, 0f, tex.width, tex.height), Vector2.zero);
                }
                catch (Exception e)
                {
#if UNITY_EDITOR
                    print(e.Message);
#endif
                    UIController.main.ShowError("Image loading failed");
                }
            }, fileTypes);

            #region OLD_BUGGY_WAY
            //TODO: fix crashing bug!!!
            /*NativeFilePicker.Permission perm = NativeFilePicker.CheckPermission(true);
            if (perm == NativeFilePicker.Permission.Denied || perm == NativeFilePicker.Permission.Denied)
            {
                perm = NativeFilePicker.RequestPermission(true);
            }

            if (perm == NativeFilePicker.Permission.Granted)
            {

                NativeFilePicker.PickFile((string path) =>
                {
                    Texture2D tex = new Texture2D(1, 1);
                    if (path == null)
                    {
                        //print error?
                        return;
                    }
                    try
                    {
                        byte[] imgBytes = File.ReadAllBytes(path);
                        if(imgBytes == null)
                        {
                            //error loading image
                            UIController.main.ShowError("Image loading failed");
                            return;
                        }

                        ImageConversion.LoadImage(tex, imgBytes);

                        if(image.sprite)
                        {
                            Destroy(image.sprite);
                        }
                        image.sprite = Sprite.Create(tex, new Rect(0f, 0f, tex.width, tex.height), Vector2.zero);
                    }
                    catch (Exception e)
                    {
#if UNITY_EDITOR
                        print(e.Message);
#endif
                        UIController.main.ShowError("Image loading failed");
                    }
                }, fileTypes);
            }
            else
            {
                //TODO> cant load image if permission for file access
                UIController.main.ShowError("Cannot load image without permission");
            }*/
            #endregion

        }
        catch (Exception e)
        {
            UIController.ShowGameMessage(e.Message);
        }
    }

    public void Awake()
    {

        if(fileTypes == null)
        {
            fileTypes = new string[wantedFileTypes.Length];
            for(int i = 0; i < fileTypes.Length; i++)
            {
                fileTypes[i] = NativeFilePicker.ConvertExtensionToFileType(wantedFileTypes[i]);
            }
        }

        if(!button)
        {
            button = GetComponentInChildren<Button>();
        }
        if(!image)
        {
            image = GetComponentInChildren<Image>();
        }

        if(button)
        {
            button.onClick.AddListener(PickImage);
        }
    }

}
