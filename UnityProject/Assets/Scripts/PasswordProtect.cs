﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Security.Cryptography;
using System;

public static class PasswordProtect
{

    private static byte[] salt = { 32, 225, 126, 11, 162, 133, 208, 62, 13, 252, 187, 137, 92, 101, 68, 199, };

    /// <summary>
    /// Source: https://stackoverflow.com/questions/4181198/how-to-hash-a-password
    /// </summary>
    /// <param name="password"></param>
    /// <returns></returns>
    public static string protectPassword(string password)
    {
        var pbkdf2 = new Rfc2898DeriveBytes(password, salt, 25);
        byte[] hash = pbkdf2.GetBytes(20);
        return Convert.ToBase64String(hash);
    }

}
