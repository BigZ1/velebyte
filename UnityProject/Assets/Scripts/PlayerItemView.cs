﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerItemView : MonoBehaviour
{

    public Text playerNameText;

    public Dropdown playerOptionsDropdown;

    public string playerName;

    public virtual void ProcessOptionSelection(int state)
    {
        //state argument is useless!
        //so read option from dropdown!
        int pick = playerOptionsDropdown.value;
        if(pick == 1)
        {
            DetaljneInfoDto body = new DetaljneInfoDto();
            body.korisnickoIme = playerName;
            Client.main.GetDetailedPlayerInfo(body.GetJSON());
        }

        if(pick == 2)
        {
            //User.main.challangePanel.Show();
            //TODO: request player info for challange!

            Client.main.RequestUserInfo(playerName, ChallangeMenu);

            void ChallangeMenu(string data)
            {

                //print("Challange info: " + data);

                UserInfoDto info = null;

                try
                {
                    info = JsonUtility.FromJson<UserInfoDto>(data);
                }
                catch (Exception e)
                {
                    UIController.ShowGameMessage(e.Message);
                    return;
                }

                if (info != null)
                {
                    UIController.main.challengePanel.Show(info);
                }

            }

        }

        playerOptionsDropdown.SetValueWithoutNotify(0);
    }

    public void Init(string playerName, bool disabled = false)
    {
        playerNameText.text = playerName;
        this.playerName = playerName;
        playerOptionsDropdown.gameObject.SetActive(!disabled);
    }

}
