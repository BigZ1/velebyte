﻿using Mapbox.Unity.Map;
using Mapbox.Utils;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class PathDrawer : MonoBehaviour
{

    public AbstractMap map;
    public LineRenderer linesDrawer;

    public Mapbox.Utils.Vector2d[] points;

    public void DrawPath(Vector2d[] cords)
    {
        //Vector3[] pts = new Vector3[cords.Length];
        HashSet<Vector3> pts = new HashSet<Vector3>();
        for(int i = 0; i < cords.Length; i++)
        {
            pts.Add(map.GeoToWorldPosition(cords[i], false));
        }
        linesDrawer.positionCount = pts.Count;
        linesDrawer.SetPositions(pts.ToArray());
    }

    public void Clear()
    {
        linesDrawer.positionCount = 0;
    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Comma))
        {
            Client.main.GetOSRMData(points, DrawPath);
        }
    }
}
