﻿using Mapbox.Utils;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerMapInfo : MonoBehaviour
{

    public string playerName;
    public Vector2d cords;

    public Dropdown dropdown;

    public bool on = false;

    public bool On
    {
        get => on;
        set
        {
            on = value;
            if (on)
            {
                dropdown.gameObject.SetActive(true);
                dropdown.transform.position = Camera.main.WorldToScreenPoint(transform.position);
                dropdown.Show();
            }
            else
            {
                dropdown.Hide();
                dropdown.gameObject.SetActive(false);
            }
        }
    }

    public void OnMouseDown()
    {
        On = !On;
        //print("Click");   //ok, supports touches
    }

    private void Start()
    {
        dropdown.options[0].text = playerName;
    }

    private void OnDestroy()
    {
        Destroy(dropdown.gameObject);
    }

    private void Update()
    {
        if(On)
        {
            dropdown.transform.position = Camera.main.WorldToScreenPoint(transform.position);
        }
    }

    public void OnDropdownSelect(int value)
    {
        if(value == dropdown.options.Count - 1 || value == 0)
        {
            On = false;
            //print("off");
        }
        if(value == 1)
        {
            //print("info");
            DetaljneInfoDto body = new DetaljneInfoDto();
            body.korisnickoIme = playerName;
            Client.main.GetDetailedPlayerInfo(body.GetJSON());
        }
        if(value == 2)
        {

            Client.main.RequestUserInfo(playerName, ChallangeMenu);

            void ChallangeMenu(string data)
            {

                //print("Challange info: " + data);

                UserInfoDto info = null;

                try
                {
                    info = JsonUtility.FromJson<UserInfoDto>(data);
                }
                catch (Exception e)
                {
                    UIController.ShowGameMessage(e.Message);
                    return;
                }

                if (info != null)
                {
                    UIController.main.challengePanel.Show(info);
                }

            }

        }
        dropdown.SetValueWithoutNotify(0);
    }

    

}
