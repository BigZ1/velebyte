﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GlobalStatsPanel : MonoBehaviour
{

    public GameObject holder;

    public GameObject contentHolder;

    public GameObject ItemObject;

    public void Show(GlobalStatisticsDto stats)
    {
        holder.SetActive(true);
        if (stats.elo == null || stats.korisnickoIme == null || stats.elo.Length != stats.korisnickoIme.Length)
            return;
        for(int i = 0; i < stats.korisnickoIme.Length; i++)
        {
            if (!string.IsNullOrEmpty(stats.korisnickoIme[i]))
            {
                GameObject ItemObjecti = Instantiate(ItemObject);
                ItemObjecti.transform.SetParent(contentHolder.transform);
                GlobalStatsItem item = ItemObjecti.GetComponent<GlobalStatsItem>();
                item.PlayerName = stats.korisnickoIme[i];
                item.ELO = stats.elo[i].ToString();
            }
        }
    }

    public void Hide()
    {
        Utils.ClearChildren(contentHolder.transform);
        holder.SetActive(false);
    }

}
