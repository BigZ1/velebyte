﻿using JetBrains.Annotations;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;

public class ConvexHull : MonoBehaviour
{

    public List<Vector2> verts;

    /// <summary>
    /// 
    /// Source: https://en.wikipedia.org/wiki/Quickhull
    /// </summary>
    /// <param name="vs"></param>
    /// <returns></returns>
    public static HashSet<Vector2> getHull(HashSet<Vector2> vs)
    {

        if (vs.Count < 3)
            return null;

        HashSet<Vector2> hull = new HashSet<Vector2>();

        //find most left (x-) vertex and most right (x+) vertex
        Vector2 A = new Vector2(float.MaxValue, float.MaxValue), B = new Vector2(float.MinValue, float.MinValue);

        foreach(Vector2 v in vs)
        {
            if(A.x > v.x)
            {
                A = v;
            }
            if (B.x < v.x)
            {
                B = v;
            }
        }

        /*//Either all points are the same or are in line
        if (A == B)
            return null;*/

        //A and B are surely in hull
        hull.Add(A);
        hull.Add(B);

        //intersect set vs with A - B line
        HashSet<Vector2> vs2;

        (vs, vs2) = splitSetWithLine(vs, A, B);

        findHull(vs, A, B);
        findHull(vs2, B, A);

        return hull;

        ////////////////////////////////////////////////////////////////////////////////////////////////////
        ///Local methods
        ///////////////////////////////////////////////////////////////////////////////////////////////////////
        ///

        //Checks where p is according to p1 - p2 line
        //0 means it is on the line, +1 and -1 are opposite sides
        float findSide(Vector2 p1, Vector2 p2, Vector2 p)
        {
            float val = (p.y - p1.y) * (p2.x - p1.x) -
                      (p2.y - p1.y) * (p.x - p1.x);

            return val;
        }

        //Splits given set into two between points p and q
        (HashSet<Vector2>, HashSet<Vector2>) splitSetWithLine(HashSet<Vector2> s, Vector2 p, Vector2 q)
        {
            HashSet<Vector2> s1 = new HashSet<Vector2>(), s2 = new HashSet<Vector2>();
            foreach (Vector2 v in s)
            {
                if (findSide(p, q, v) > 0f)
                {
                    s1.Add(v);
                } else
                {
                    s2.Add(v);
                }
            }
            return (s1, s2);
        }

        float pointLineDistance(Vector2 p1, Vector2 p2, Vector2 p)
        {
            if (p1 == p2)
                return 0f;
            return Mathf.Abs((p2.y - p1.y) * p.x - (p2.x - p1.x) * p.y + p2.x * p1.y - p2.y * p1.x) / (Mathf.Sqrt((p2.y - p1.y) * (p2.y - p1.y) - (p2.x) * (p1.x)));
        }

        void findHull(HashSet<Vector2> s, Vector2 p, Vector2 q)
        {
            if (s.Count < 1)
                return;

            (Vector2 p, float dist) pd = (Vector2.zero, 0f);

            foreach(Vector2 v in s)
            {
                pd = (v, pointLineDistance(p, q, v));
                break;
            }

            foreach(Vector2 v in s)
            {
                float d = pointLineDistance(p, q, v);
                if (pd.dist < d)
                {
                    pd = (v, d);
                }
            }

            hull.Add(pd.p);

            HashSet<Vector2> s0, s1, s2;

            (s1, s0) = splitSetWithLine(s, p, pd.p);
            (s2, s0) = splitSetWithLine(s, pd.p, q);

            findHull(s1, p, pd.p);
            findHull(s2, pd.p, q);

        }

    }

    public static void sortListCounterClockwize(List<Vector2> vs)
    {
        if(vs != null)
        {

            Vector2 center = Vector2.zero;

            for(int i = 0; i < vs.Count; i++)
            {
                center += vs[i];
            }

            center /= vs.Count;

            vs.Sort((Vector2 a, Vector2 b) => {

                return -Mathf.CeilToInt(((Mathf.Atan2(a.x - center.x, a.y - center.y) * Mathf.Rad2Deg + 360) - (Mathf.Atan2(b.x - center.x, b.y - center.y) * Mathf.Rad2Deg + 360)));
            });
        }
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }

    float findSide(Vector2 p1, Vector2 p2, Vector2 p)
    {
        float val = (p.y - p1.y) * (p2.x - p1.x) -
                  (p2.y - p1.y) * (p.x - p1.x);

        return val;
    }

    (HashSet<Vector2>, HashSet<Vector2>) splitSetWithLine(HashSet<Vector2> s, Vector2 p, Vector2 q)
    {
        HashSet<Vector2> s1 = new HashSet<Vector2>(), s2 = new HashSet<Vector2>();
        foreach (Vector2 v in s)
        {
            if (findSide(p, q, v) > 0f)
            {
                s1.Add(v);
            }
            else
            {
                s2.Add(v);
            }
        }
        return (s1, s2);
    }

#if UNITY_EDITOR
    private void OnDrawGizmos()
    {

        if (verts == null || verts.Count < 3) return;

        /*HashSet<Vector2> set = new HashSet<Vector2>(verts);

        //find most left (x-) vertex and most right (x+) vertex
        Vector2 A = new Vector2(float.MaxValue, float.MaxValue), B = new Vector2(float.MinValue, float.MinValue);

        foreach (Vector2 v in set)
        {
            if (A.x > v.x)
            {
                A = v;
            }
            if (B.x < v.x)
            {
                B = v;
            }
        }

        Gizmos.color = Color.magenta;
        Gizmos.DrawLine(A, B);

        HashSet<Vector2> set2 = new HashSet<Vector2>(verts);

        (set, set2) = splitSetWithLine(set, A, B);

        Gizmos.color = Color.blue;

        foreach(Vector2 v in set)
        {
            Gizmos.DrawSphere(v, 0.25f);
        }

        Gizmos.color = Color.red;

        foreach (Vector2 v in set2)
        {
            Gizmos.DrawSphere(v, 0.25f);
        }*/

        for(int i = 0; i < verts.Count; i++)
        {
            Gizmos.DrawSphere(verts[i], 0.25f);
        }

        List<Vector2> poly = getHull(new HashSet<Vector2>(verts)).ToList();
        sortListCounterClockwize(poly);


        Gizmos.color = Color.blue;
        if(poly != null)
        {
            for(int i = 0; i < poly.Count; i++)
            {
                Gizmos.DrawSphere(poly[i], 0.25f);
                Handles.Label(poly[i] + Vector2.up, "" + i);
                if(i > 0)
                {
                    Gizmos.DrawLine(poly[i], poly[i - 1]);
                }
            }
            Gizmos.DrawLine(poly[0], poly[poly.Count - 1]);
        } else
        {
            print("err");
        }

    }
#endif

}
