﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChallengePanel : MonoBehaviour
{

    public GameObject holder;

    public Text playerNameText;
    public Text playerEloText;
    public Image playerProfileImage;
    public Dropdown battleTypeDropdown;
    public InputField townInput;
    public InputField countryInput;
    public Dropdown continentPick;

    public void battleTypePick(int value)
    {
        townInput.interactable = false;
        countryInput.interactable = false;
        continentPick.interactable = false;
        if(value == 0)
        {
            townInput.interactable = true;
        }
        if (value == 1)
        {
            countryInput.interactable = true;
        }
        if(value == 2)
        {
            continentPick.interactable = true;
        }

    }

    private void Start()
    {
        battleTypePick(0);
    }

    public void Show(UserInfoDto info)
    {

        holder.SetActive(true);

        battleTypePick(0);
        playerNameText.text = info.korisnickoIme;
        playerEloText.text = info.elo.ToString();

        try
        {
            playerProfileImage.sprite = Utils.Base64ToSprite(info.urlFotografije);
        }
        catch (Exception) { }



    }

    public void Hide()
    {

        holder.SetActive(false);

        playerNameText.text = "";
        playerEloText.text = "";
        playerProfileImage.sprite = null;
        townInput.text = "";
        countryInput.text = "";
    }

    public void Challange()
    {
        

        IzazovIgracaDto izazov = new IzazovIgracaDto();
        izazov.ciljniIgrac = playerNameText.text;
        izazov.izazivac = User.main.username;
        izazov.tipBorbe = battleTypeDropdown.value;
        int value = battleTypeDropdown.value;
        if (value == 0)
        {
            if(string.IsNullOrEmpty(townInput.text))
            {
                UIController.ShowGameMessage("Town name cannot be empty");
                return;
            }
            //townInput.interactable = true;
            izazov.grad = townInput.text;
        }
        if (value == 1)
        {
            //countryInput.interactable = true;
            izazov.drzava = countryInput.text;
            if (string.IsNullOrEmpty(countryInput.text))
            {
                UIController.ShowGameMessage("Country name cannot be empty");
                return;
            }

        }
        if (value == 2)
        {
            izazov.kontinent = continentPick.value;
        }
        Client.main.ChallangePlayer(izazov.GetJSON());

        Hide();

    }

}
