﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class CardUI : MonoBehaviour, IPointerClickHandler
{

    public Text TitleText;
    public Image ImageContainer;
    public Text DescriptionText;
    public int Rarity;
    public long id;

    public double lng, lat;

    public bool Selectable = false;

    public class OnSelectEvent : UnityEvent<CardUI>
    {

    }

    public OnSelectEvent OnSelect = null;

    public string Title
    {
        get
        {
            return TitleText.text;
        }
        set
        {
            TitleText.text = value;
        }
    }

    public Texture2D Image
    {
        get
        {
            return (Texture2D)ImageContainer.sprite.texture;
        }
        set
        {
            Sprite s = Sprite.Create(value, new Rect(0, 0, value.width, value.height), Vector2.zero);
            ImageContainer.sprite = s;
        }
    }

    public string Description
    {
        get
        {
            return DescriptionText.text;
        }
        set
        {
            DescriptionText.text = value;
        }
    }

    public void OnPointerClick(PointerEventData eventData)
    {
        //print("Card select");
        if(Selectable)
        {
            OnSelect?.Invoke(this);
        }
    }
}
