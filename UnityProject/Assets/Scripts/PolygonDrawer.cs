﻿using Mapbox.Unity.Map;
using Mapbox.Utils;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PolygonDrawer : MonoBehaviour
{

    public AbstractMap Map;

    public Mesh mesh;

    public Color meshColor;

    public Material meshMaterial;

    public MeshRenderer mr;

    public MeshFilter mf;

    public LineRenderer lineDrawer;

    public void DrawPoints(Vector2d[] points, float height = 0.2f)
    {

        HashSet<Vector2> pts = new HashSet<Vector2>();

        for(int i = 0; i < points.Length; i++)
        {
            Vector3 point = Map.GeoToWorldPosition(points[i]);
            pts.Add(new Vector2(point.x, point.z));
        }

    

        HashSet<Vector2> hull = ConvexHull.getHull(pts);
        
        if(hull != null && hull.Count >= 3)
        {

            List<Vector2> ptsL = new List<Vector2>(hull);

            ConvexHull.sortListCounterClockwize(ptsL);

            mesh = new Mesh();

            List<Vector3> verts = new List<Vector3>();

            List<int> tris = new List<int>();

            for(int i = 0; i < ptsL.Count; i++)
            {
                verts.Add(new Vector3(ptsL[i].x, height, ptsL[i].y));
            }

            mesh.vertices = verts.ToArray();

            if (lineDrawer) {
                lineDrawer.positionCount = mesh.vertices.Length;
                lineDrawer.SetPositions(mesh.vertices);
                /*Gradient g = new Gradient();
                g.colorKeys = new GradientColorKey[2];
                g.colorKeys[0].color = lineColor;
                g.colorKeys[0].time = 0f;
                g.colorKeys[1].color = lineColor;
                g.colorKeys[1].time = 1f;
                lineDrawer.colorGradient = g;*/
            }

            for(int i = 2; i < ptsL.Count; i++)
            {
                tris.Add(i);
                tris.Add(i - 1);
                tris.Add(0);
            }

            mesh.triangles = tris.ToArray();

            mesh.RecalculateNormals();

            mf.mesh = mesh;

            meshMaterial.color = meshColor;

            mr.sharedMaterial = meshMaterial;

            

        }

    }

    public void Clear()
    {
        mf.mesh = null;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
