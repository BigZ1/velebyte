﻿using Mapbox.Utils;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CartographerLocationPicks : MonoBehaviour
{

    public GameObject holder;

    public GameObject contentHolder;

    public GameObject buttonObject;

    public LocationPick[] locationPicks;

    public Button DisablePathButton;

    public struct LocationPick
    {

        public long ID;

        public Vector2d Coordinates;

        public Button Button;

        public void PickLocation()
        {
            //print("Pick: " + ID + " " + Coordinates.x + ", " + Coordinates.y);
            CartographerRequestLocation req = new CartographerRequestLocation();
            req.username = User.main.username;
            req.locationid = ID;
            Client.main.RequestLocationInfo(req.GetJSON(), true);
        }

    }

    private void Awake()
    {
        if (!holder)
            holder = gameObject;
    }

    public void Hide()
    {
        holder.SetActive(false);
        Utils.ClearChildren(contentHolder.transform);
        locationPicks = null;
    }

    public void Show(OdabraneLokacijeDto odabraneLokacijeDto)
    {
        if(odabraneLokacijeDto != null && odabraneLokacijeDto.ids != null && odabraneLokacijeDto.koordinate != null)
        {
            locationPicks = new LocationPick[odabraneLokacijeDto.ids.Length];
            long[] IDs = odabraneLokacijeDto.ids;
            double[] cords = odabraneLokacijeDto.koordinate;
            if (IDs.Length * 2 != cords.Length)
                return;
            for(int i = 0; i < locationPicks.Length; i++)
            {
                locationPicks[i].ID = IDs[i];
                locationPicks[i].Coordinates = new Vector2d(cords[i * 2], cords[i * 2 + 1]);
                locationPicks[i].Button = Instantiate(buttonObject, contentHolder.transform).GetComponent<Button>();
                locationPicks[i].Button.onClick.AddListener(new UnityEngine.Events.UnityAction(locationPicks[i].PickLocation));
                locationPicks[i].Button.GetComponentInChildren<Text>().text = "LocationID: " + locationPicks[i].ID + " Coordinates: " + locationPicks[i].Coordinates;
            }
            holder.SetActive(true);
        } else
        {
            UIController.ShowGameMessage("You don't have any picked locations for visit", "Game Message", true);
        }
    }

    public void ShowPath()
    {
        List<Vector2d> points = new List<Vector2d>();
        points.Add(PlayerLocator.main.Coords);
        for (int i = 0; i < locationPicks.Length; i++)
        {
            points.Add(locationPicks[i].Coordinates);
            SceneController.main.LocationTargetPointShow.PlaceOn(locationPicks[i].Coordinates);
        }

        Client.main.GetOSRMData(points.ToArray(), AfterOSRMGetLocation);

        void AfterOSRMGetLocation(Vector2d[] cords) {
            SceneController.main.pathDrawer.DrawPath(cords);
            //TODO: enable disable path button...
            Hide();
            DisablePathButton.gameObject.SetActive(true);
        }

    }

    public void RemoveDrawnPath()
    {
        SceneController.main.pathDrawer.Clear();
        SceneController.main.LocationTargetPointShow.Clear();
        DisablePathButton.gameObject.SetActive(false);
    }

}
