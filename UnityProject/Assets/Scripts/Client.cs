﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Net.Sockets;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System;
using System.Threading;
using System.Reflection;
using System.Net.Http;
using JetBrains.Annotations;
using Mapbox.Utils;
//using Mapbox.Unity.Utilities;
//using UnityEditor.iOS;

public class Client : MonoBehaviour
{

    public string httpURI = "https://geofighterserverreal.herokuapp.com";

    static HttpClient client = new HttpClient();

    public delegate void OnRequestDone(string response);

    #region HTTP_METHODS

    public string loginURI = "/geofighter/prijava";

    //uri is like /login - maybe /login/ - TODO: check
    public void Login(string body)
    {

        Task t = new Task(() => { postStuff(loginURI, body, AfterLogin); });
        t.Start();

        print("login");

        void AfterLogin(string data)
        {
            print("Got: " + data);

            OdgovorNaPrijavuDto response;

            try
            {
                response = JsonUtility.FromJson<OdgovorNaPrijavuDto>(data);
            } catch (Exception e)
            {
                UIController.ShowGameMessage(e.Message);
                return;
            }

            if (response.uspijeh)
            {

                User.main.Type = response.tipKorisnika;
                User.main.username = response.korisncikoIme;
                SceneController.main.CurrentState.State = SceneController.GameState.InGame;

                RequestUserInfo(User.main.username);

            } else
            {

                UIController.ShowGameMessage(response.opis);

            }

        }

    }

    public string logoffURI = "/geofighter/odjava";

    public void Logoff(string body)
    {

        print(body);

        Task t = new Task(() => { postStuff(logoffURI, body, AfterLogoff); });
        t.Start();

        print("logoff");

        void AfterLogoff(string data)
        {
            print("Got: " + data);

            OdgovorNaOdjavuDto response;

            SceneController.main.CurrentState.State = SceneController.GameState.Guest;

            try
            {
                response = JsonUtility.FromJson<OdgovorNaOdjavuDto>(data);
            } catch (Exception e)
            {
                UIController.ShowGameMessage(e.Message);
                return;
            }

            //print("got: " + response.korisnickoIme + " current: " + User.main.username);

            if (response.korisnickoIme != User.main.username)
            {
#if UNITY_EDITOR
                print("ERROR: got wrong username when logging off");
#endif
            }

            User.main.username = null;

        }

    }

    //TODO: add response
    public void RequestUserData(string body)
    {

        string uri = "/requestUserData";

        Task t = new Task(() => { postStuff(uri, body, AfterRequestUserData); });
        t.Start();

        print("request user data");

        void AfterRequestUserData(string data)
        {
            print("Got: " + data);

        }

    }

    public string registerPlayerURI = "/geofighter/registracija";

    public void RegisterPlayer(string body)
    {

        print(body);

        Task t = new Task(() => { postStuff(registerPlayerURI, body, AfterRegisterPlayer); });
        t.Start();

        print("register player");

        void AfterRegisterPlayer(string data)
        {
            print("Got: " + data);

            RegistracijskiOdgovorDto response;

            try
            {
                response = JsonUtility.FromJson<RegistracijskiOdgovorDto>(data);
            }
            catch (Exception e)
            {
                UIController.ShowGameMessage(e.Message);
                return;
            }

            if (string.IsNullOrEmpty(response.opis))
            {
                UIController.ShowGameMessage("An email was sent to: " + response.email + " to confirm your account. ", "Game message");
            } else
            {
                UIController.ShowGameMessage(response.opis);
            }

        }

    }

    public string registerCartographerURI = "/geofighter/registracijaKartografa";

    public void RegisterCartographer(string body)
    {



        Task t = new Task(() => { postStuff(registerCartographerURI, body, AfterRegisterCartographer); });
        t.Start();

        print("register cartographer");

        void AfterRegisterCartographer(string data)
        {
            print("Got: " + data);

            RegistracijskiOdgovorDto response;

            try
            {
                response = JsonUtility.FromJson<RegistracijskiOdgovorDto>(data);
            } catch (Exception e)
            {
                UIController.ShowGameMessage(e.Message);
                return;
            }

            if (string.IsNullOrEmpty(response.opis))
            {
                UIController.ShowGameMessage("An email was sent to: " + response.email + " to confirm your account. Additionaly you will need to be approved by an administrator.", "Game message");
            } else
            {
                UIController.ShowGameMessage(response.opis);
            }
        }

    }

    public string acceptCartographerURI = "/geofighter/potvrdaKartografa";

    public void AdministratorAcceptCartographer(string body)
    {

        Task t = new Task(() => { postStuff(acceptCartographerURI, body, AfterAdministratorAcceptCartographer); });
        t.Start();

        print("accept cartographer");

        void AfterAdministratorAcceptCartographer(string data)
        {
            print("Got: " + data);

            UACOdgovorInfo info = new UACOdgovorInfo();
            info.poruka = "greska";

            try
            {
                info = JsonUtility.FromJson<UACOdgovorInfo>(data);
            } catch (Exception e)
            {
                UIController.ShowGameMessage(e.Message);
            }

            UIController.ShowGameMessage(info.poruka, "Game Message");

            RequestUnapplyedCartographerData("");

        }

    }

    public void RequestUnapplyedCartographerData(string body)
    {

        string uri = "/geofighter/requestUACData";

        Task t = new Task(() => { getStuff(uri, AfterRequestUnapplyedCartographerData); });
        t.Start();

        print("request cartographer data");

        void AfterRequestUnapplyedCartographerData(string data)
        {
            print("Got: " + data);

            UAC response;

            try
            {
                response = JsonUtility.FromJson<UAC>(data);
            } catch (Exception e)
            {
                UIController.ShowGameMessage(e.Message);
                return;
            }

            if (response.korisnickoIme == null || string.IsNullOrEmpty(response.korisnickoIme))
            {
                UIController.main.ApplyCartographerUI.cartographerNameText.text = "No cartographers to apply!";
                UIController.main.ApplyCartographerUI.identityCardImage.sprite = null;
                return;
            } else
            {
                UIController.main.ApplyCartographerUI.cartographerNameText.text = response.korisnickoIme;

                try
                {
                    Texture2D tex = Utils.Base64ToTexture(response.slikaOsobneIskaznice);
                    UIController.main.ApplyCartographerUI.identityCardImage.sprite = Sprite.Create(tex, new Rect(Vector2.zero, new Vector2(tex.width, tex.height)), Vector2.zero);
                } catch (Exception e)
                {
                    UIController.ShowGameMessage(e.Message);
                }
            }

        }

    }

    public string userInfoURI = "/geofighter/userInfo";

    public void RequestUserInfo(string username, OnRequestDone onDone = null)
    {

        string uri = userInfoURI + "?username=" + username;

        print("Get user info: " + uri);

        Task t = new Task(() => { getStuff(uri, onDone == null ? AfterRequestUserInfo : onDone); });
        t.Start();

        void AfterRequestUserInfo(string data)
        {
            //print("Got: " + data);

            UserInfoDto info = null;

            try
            {
                info = JsonUtility.FromJson<UserInfoDto>(data);
            } catch (Exception e)
            {
                UIController.ShowGameMessage(e.Message);
                return;
            }

            if (info != null)
            {

                User.main.Type = (User.UserType)Enum.Parse(typeof(User.UserType), info.uloga);
                User.main.ELO = info.elo;

                if (User.main.Type == User.UserType.Administrator)
                {
                    UIController.main.AdministratorUI.SetActive(true);
                    UIController.main.CartographerUI.SetActive(true);
                    //print("Admin in");
                }

                if (User.main.Type == User.UserType.Cartographer) {
                    UIController.main.CartographerUI.SetActive(true);
                }

                SceneController.main.PlayerLocator.BeginSending();

                //print(User.main.Type);
                User.main.username = info.korisnickoIme;

                User.main.ApplyUIChanges();

                try
                {
                    User.main.userImage.sprite = Utils.Base64ToSprite(info.urlFotografije);
                } catch (Exception e)
                {
                    UIController.ShowGameMessage(e.Message);
                    return;
                }
            }

        }
    }

    public string getActiveUsersUI = "/geofighter/aktivniIgraci";

    public void GetActiveUsersList()
    {
        string uri = getActiveUsersUI;

        Task t = new Task(() => { getStuff(uri, AfterGetActiveUsersList); });
        t.Start();

        void AfterGetActiveUsersList(string data)
        {

            print("Got: " + data);

            ListKorisnikaDto info = null;

            try
            {
                info = JsonUtility.FromJson<ListKorisnikaDto>(data);
            }
            catch (Exception e)
            {
                UIController.ShowGameMessage(e.Message);
                return;
            }

            if (info != null)
            {
                UIController.main.ShowActivePlayers(info.ulogiraniKorisniciImena);
            } else
            {
                UIController.ShowGameMessage("User list receive error");
            }

        }

    }

    public string getDetailedPlayerInfoUri = "/geofighter/detaljneInformacije";

    public void GetDetailedPlayerInfo(string body)
    {

        string uri = getDetailedPlayerInfoUri;

        Task t = new Task(() => { postStuff(uri, body, AfterGetDetailedPlayerInfo); });
        t.Start();

        void AfterGetDetailedPlayerInfo(string data)
        {

            print("Got: " + data);

            DetaljneInfoOdgovorDto info = null;

            try
            {
                info = JsonUtility.FromJson<DetaljneInfoOdgovorDto>(data);
            }
            catch (Exception e)
            {
                UIController.ShowGameMessage(e.Message);
                return;
            }

            if (info != null)
            {
                UIController.main.detailedPlayerInfoPanel.ShowData(info);
            }
            else
            {
                UIController.ShowGameMessage("Player data receive error");
            }

        }

    }

    public string getUserListUri = "/geofighter/sviKorisnici";

    public void GetUserList()
    {

        string uri = getUserListUri;

        Task t = new Task(() => { getStuff(uri, AfterGetUserList); });
        t.Start();

        void AfterGetUserList(string data)
        {

            print("Got: " + data);

            ListKorisnikaDto info = null;

            try
            {
                info = JsonUtility.FromJson<ListKorisnikaDto>(data);
            }
            catch (Exception e)
            {
                UIController.ShowGameMessage(e.Message);
                return;
            }

            if (info != null)
            {
                UIController.main.ShowUserList(info.ulogiraniKorisniciImena);
            }
            else
            {
                UIController.ShowGameMessage("User list receive error");
            }

        }

    }


    public string deleteUserUri = "/geofighter/izbrisiKorisnika";

    public void DeletePlayer(string body)
    {

        Task t = new Task(() => { postStuff(deleteUserUri, body, AfterDeletePlayer); });
        t.Start();

        print("delete player");

        void AfterDeletePlayer(string data)
        {
            print("Got: " + data);

            UACOdgovorInfo info = new UACOdgovorInfo();
            info.poruka = "greska";

            try
            {
                info = JsonUtility.FromJson<UACOdgovorInfo>(data);
            }
            catch (Exception e)
            {
                UIController.ShowGameMessage(e.Message);
            }

            UIController.ShowGameMessage(info.poruka, "Game Message");
            UIController.main.RefreshUsersList();

            RequestUnapplyedCartographerData("");

        }

    }

    public string editUserStartUri = "/geofighter/dohvatPodatakaZaUredivanje";

    public void RequestEditUserData(string body)
    {

        print(body);

        Task t = new Task(() => { postStuff(editUserStartUri, body, AfterRequestEditUserData); });
        t.Start();

        print("request user edit");

        void AfterRequestEditUserData(string data)
        {
            print("Got: " + data);

            UrediKorisnikaPodaciDto info = new UrediKorisnikaPodaciDto();

            try
            {
                info = JsonUtility.FromJson<UrediKorisnikaPodaciDto>(data);
            }
            catch (Exception e)
            {
                UIController.ShowGameMessage(e.Message);
            }

            UIController.main.editUserPanel.Show(info);

        }

    }

    public string editUserUri = "/geofighter/urediKorisnika";

    public void EditUser(string body)
    {

        print(body);

        Task t = new Task(() => { postStuff(editUserUri, body, AfterEditUser); });
        t.Start();

        print("request user edit");

        void AfterEditUser(string data)
        {
            print("Got: " + data);

            UrediKorisnikaOdgovorDto info = new UrediKorisnikaOdgovorDto();

            try
            {
                info = JsonUtility.FromJson<UrediKorisnikaOdgovorDto>(data);
            }
            catch (Exception e)
            {
                UIController.ShowGameMessage(e.Message);
            }

            UIController.ShowGameMessage(info.poruka, "Game Message");
            //UIController.main.editUserPanel.Show(info);

        }

    }

    public string sendGeoCoordsUri = "/geofighter/provjeriKoordinate";

    public void SendGeoCoords(string body)
    {

        //print(body);

        Task t = new Task(() => { postStuff(sendGeoCoordsUri, body, AfterSendGeoCoords); });
        t.Start();

        //print("sent coords");

        void AfterSendGeoCoords(string data)
        {
            //print("Got: " + data);

            ProvjeriGeoOdgovorDto info = new ProvjeriGeoOdgovorDto();

            try
            {
                info = JsonUtility.FromJson<ProvjeriGeoOdgovorDto>(data);
            }
            catch (Exception e)
            {
                UIController.ShowGameMessage(e.Message);
            }

            if (info.kartaID != null)
            {
                for (int i = 0; i < info.kartaID.Length; i++)
                {
                    if (info.kartaID[i] >= 0)
                    {
                        //print("Got card");
                        RequestCardInfo(info.kartaID[i]);
                    }
                }
            }

            if (info.poznatiID != null && info.koordinateBlizu != null && info.poznatiID.Length * 2 == info.koordinateBlizu.Length)
            {
                for (int i = 0; i < info.poznatiID.Length; i++)
                {
                    Vector2d cords = new Vector2d(info.koordinateBlizu[i * 2], info.koordinateBlizu[i * 2 + 1]);
                    //SceneController.main.MapPointShow.Clear();
                    if (info.poznatiID[i] >= 0)
                    {
                        RequestCardInfo(info.poznatiID[i], OnGetCardInfo);

                        void OnGetCardInfo(string d)
                        {

                            //print("Got: " + data);

                            KartaInfoDto k = null;

                            SceneController.main.MapPointShow.RemoveOn(cords);

                            try
                            {
                                k = JsonUtility.FromJson<KartaInfoDto>(d);
                            }
                            catch (Exception e)
                            {
                                UIController.ShowGameMessage(e.Message);
                            }

                            if (k != null)
                            {
                                Texture2D tex = null;
                                try
                                {
                                    tex = Utils.Base64ToTexture(k.slika);
                                } catch (Exception)
                                {

                                }
                                SceneController.main.MapCardShow.PlaceOn(cords, k.imeKarte, tex, k.opis, (int)k.raritet);
                            }

                        }
                    } else
                    {
                        SceneController.main.MapPointShow.PlaceOn(cords);
                    }
                }
            }
            //UIController.main.editUserPanel.Show(info);

        }

    }

    //"/geofighter/karta?kartaID=23"
    public string requestCardInfoUri = "/geofighter/karta";

    public void RequestCardInfo(int cardID, OnRequestDone onDone = null)
    {

        string uri = requestCardInfoUri + "?kartaID=" + cardID;

        Task t = new Task(() => { getStuff(uri, onDone != null ? onDone : AfterRequestCardInfo); });
        t.Start();

        void AfterRequestCardInfo(string data)
        {

            print("Got: " + data);

            KartaInfoDto k = null;

            try
            {
                k = JsonUtility.FromJson<KartaInfoDto>(data);
            } catch (Exception e)
            {
                UIController.ShowGameMessage(e.Message);
            }

            if (k != null)
            {
                UIController.ShowGameMessage(k.imeKarte + "\n", "You got a new card");
            }

        }

    }

    public string okGetUri = "/geofighter/ok";

    public void CheckServerOK()
    {

        getStuff(okGetUri, AfterCheckServerOK);

        void AfterCheckServerOK(string data)
        {

            OdgovorOKDto odg = null;

            try
            {
                odg = JsonUtility.FromJson<OdgovorOKDto>(data);
            } catch (Exception e)
            {
                UIController.ShowGameMessage(e.Message);
            }

            if (odg.poruka != "ok")
            {
                UIController.ShowGameMessage("Server is not responding");
            }/* else
            {
                print("Server ok");
            }*/

        }

    }

    public string bannedUserUri = "/geofighter/banned";

    public void BannedCheck(string body)
    {

        Task t = new Task(() => { postStuff(bannedUserUri, body, AfterBannedCheck); });
        t.Start();

        void AfterBannedCheck(string data)
        {

            print("Got: " + data);

            BanKorisnikaDto bi = null;

            try
            {
                bi = JsonUtility.FromJson<BanKorisnikaDto>(data);
            } catch (Exception e)
            {
                UIController.ShowGameMessage(e.Message);
            }

            if (bi != null)
            {
                if (bi.korisnickoIme != null)
                {
                    UIController.main.banUserPanel.Show(bi);
                }
            }

        }

    }

    public string banUserUri = "/geofighter/ban";

    public void BanUser(string body)
    {

        print("Sending: " + body);

        Task t = new Task(() => { postStuff(banUserUri, body, AfterBanUser); });
        t.Start();

        void AfterBanUser(string data)
        {

            print("Got: " + data);

            OdgovorOKDto odg = null;

            try
            {
                odg = JsonUtility.FromJson<OdgovorOKDto>(data);
            } catch (Exception e)
            {
                UIController.main.ShowError(e.Message);
            }

            if (odg != null)
            {
                UIController.ShowGameMessage(odg.poruka, "Game message");
            }

        }

    }

    public delegate void CoordsGot(Mapbox.Utils.Vector2d[] cords);

    public void GetOSRMData(Mapbox.Utils.Vector2d[] coords, CoordsGot afterGot)
    {

        print("OSRM request");

        Task t = new Task(() => { getOSRMStuff(coords, AfterOSRMGet); });
        t.Start();

        void AfterOSRMGet(string data)
        {

            print("Got: " + data);

            OSRMData odg = null;

            try
            {
                odg = JsonUtility.FromJson<OSRMData>(data);
            }
            catch (Exception e)
            {
                UIController.main.ShowError(e.Message);
            }

            if (odg != null)
            {
                List<Mapbox.Utils.Vector2d> crds = new List<Mapbox.Utils.Vector2d>();
                OSRMData.Trip[] trips = odg.trips;
                for(int i = 0; i < trips.Length; i++)
                {
                    OSRMData.Leg[] legs = trips[i].legs;
                    for(int j = 0; j < legs.Length; j++)
                    {
                        OSRMData.Step[] steps = legs[j].steps;
                        for(int k = 0; k < steps.Length; k++)
                        {
                            OSRMData.Intersection[] intersections = steps[k].intersections;
                            for(int l = 0; l < intersections.Length; l++)
                            {
                                crds.Add(new Vector2d(intersections[l].location[1], intersections[l].location[0]));
                            }
                            crds.Add(new Vector2d(steps[k].maneuver.location[1], steps[k].maneuver.location[0]));
                        }
                    }
                }
                /*for (int i = 0; i < ws.Length; i++)
                {
                    
                    crds.Add(new Mapbox.Utils.Vector2d(ws[i].location[0], ws[i].location[1]));
                }*/
                afterGot?.Invoke(crds.ToArray());
            }

        }

    }

    public string addLocationUri = "/geofighter/dodajLokaciju";

    public void AddLocation(string body)
    {

        print("Add location: " + body);

        Task t = new Task(() => { postStuff(addLocationUri, body, AfterAddLocation); });
        t.Start();

        void AfterAddLocation(string data)
        {

            AddLocationOdgovorDto odg = null;

            print("Add location response: " + data);

            try
            {
                odg = JsonUtility.FromJson<AddLocationOdgovorDto>(data);
            } catch (Exception e)
            {
                UIController.ShowGameMessage(e.Message);
            }

            if (odg != null)
            {
                UIController.ShowGameMessage(odg.poruka, "Game Message");
            }

        }

    }

    public string cartographerLocationRequestUri = "/geofighter/zahtjevLokacije";

    public void RequestLocationInfo(string body, bool final = false)
    {

        print("Request location: " + body);

        if (final)
            UIController.main.CartographerPicksPanel.Hide();

        Task t = new Task(() => { postStuff(cartographerLocationRequestUri, body, AfterRequestLocation); });
        t.Start();

        void AfterRequestLocation(string data)
        {

            print("Got: " + data);

            RequestCartographerLocationData loc = null;

            try
            {
                loc = JsonUtility.FromJson<RequestCartographerLocationData>(data);
            } catch (Exception e)
            {
                UIController.ShowGameMessage(e.Message);
            }

            if (loc.locationID >= 0)
            {
                //ok
                UIController.main.CartographerLocationPanel.Show(loc, final);
            } else
            {
                UIController.main.CartographerLocationPanel.Close();
                if(final)
                    UIController.main.CartographerPicksPanel.Hide();
                UIController.ShowGameMessage("No locations to confirm", "Game Message");
            }

        }

    }

    public string cartographerConfirmLocationUri = "/geofighter/prihvatiLokaciju";

    public void CartographerAcceptLocation(string body)
    {
        print("Request location: " + body);

        Task t = new Task(() => { postStuff(cartographerConfirmLocationUri, body, AfterAcceptLocation); });
        t.Start();

        void AfterAcceptLocation(string data)
        {
            AddLocationOdgovorDto o = null;

            try
            {
                o = JsonUtility.FromJson<AddLocationOdgovorDto>(data);
            } catch (Exception e)
            {
                UIController.ShowGameMessage(e.Message);
            }

            if (o != null)
            {
                UIController.ShowGameMessage(o.poruka, "Game Message", true);
                CartographerRequestLocation req = new CartographerRequestLocation();
                req.username = User.main.username;
                main.RequestLocationInfo(req.GetJSON());
            } else
            {
                UIController.ShowGameMessage("Data error");
            }

        }

    }

    public string getPlayerCardsUri = "/geofighter/dohvatiKolekciju";

    public void GetPlayerCards(string body, OnRequestDone onDone = null)
    {

        print("Request cards: " + body);

        Task t = new Task(() => { postStuff(getPlayerCardsUri, body, onDone == null ? AfterRequestCards : onDone); });
        t.Start();

        void AfterRequestCards(string data)
        {

            print("Request Cards Result: " + data);

            PlayerCardsDto cards = null;

            try
            {
                cards = JsonUtility.FromJson<PlayerCardsDto>(data);
            } catch (Exception e)
            {
                UIController.ShowGameMessage(e.Message);
            }

            if (cards != null)
            {
                if (cards.idKarata != null)
                {
                    for (int i = 0; i < cards.idKarata.Length; i++)
                    {
                        RequestCardInfo((int)cards.idKarata[i], User.main.cardPanel.ShowCard);
                    }
                }
            } else
            {
                UIController.ShowGameMessage("Card receive error");
            }

        }

    }

    public string getCartographerPicksUri = "/geofighter/pickedLocations";

    public void GetCartographerPicks(string body)
    {

        print("Request Picks: " + body);

        Task t = new Task(() => { postStuff(getCartographerPicksUri, body, AfterGetCartographerPics); });
        t.Start();

        void AfterGetCartographerPics(string data)
        {

            print("Picks: " + data);

            OdabraneLokacijeDto d = null;

            try
            {
                d = JsonUtility.FromJson<OdabraneLokacijeDto>(data);
            } catch (Exception e)
            {
                UIController.ShowGameMessage(e.Message);
            }

            if (d != null)
            {
                UIController.main.CartographerPicksPanel.Show(d);
            } else
            {
                UIController.ShowGameMessage("Error getting picked locations");
            }

        }

    }

    public string cartographerAddLocationForCheckUri = "/geofighter/dodajLokacijuZaPregled";

    public void AddCartographerLocationCheck(string body)
    {

        print("Add Check: " + body);

        Task t = new Task(() => { postStuff(cartographerAddLocationForCheckUri, body, AfterAddCartographerLocationCheck); });
        t.Start();

        void AfterAddCartographerLocationCheck(string data)
        {

            print("Check response: " + data);

            AddLocationOdgovorDto odg = null;

            try
            {
                odg = JsonUtility.FromJson<AddLocationOdgovorDto>(data);
            } catch (Exception e)
            {
                UIController.ShowGameMessage(e.Message);
            }

            if (odg != null)
            {
                if (odg.poruka.ToUpper() == "OK")
                {
                    UIController.ShowGameMessage("Location added to checklist", "Game Message", true);
                } else
                {
                    UIController.ShowGameMessage(odg.poruka, "Game Message", true);
                }
                CartographerRequestLocation req = new CartographerRequestLocation();
                req.username = User.main.username;
                main.RequestLocationInfo(req.GetJSON());

            } else
            {
                UIController.ShowGameMessage("Error adding location to checklist", "Error", true);
            }

        }

    }

    public string cartographerRejectLocation = "/geofighter/odbijLokaciju";

    public void RejectLocation(string body)
    {

        print("Reject Location: " + body);

        Task t = new Task(() => { postStuff(cartographerRejectLocation, body, AfterRejectLocation); });
        t.Start();

        void AfterRejectLocation(string data)
        {

            print("Location Reject got: " + data);

            PorukaDto p = null;

            try
            {
                p = JsonUtility.FromJson<PorukaDto>(data);
            } catch (Exception e)
            {
                UIController.ShowGameMessage(e.Message, "Error", true);
                return;
            }

            if(p != null)
            {
                UIController.ShowGameMessage(p.poruka, "Game Message", true);
            }

        }
    }

    public string getNearUsersUrl = "/geofighter/korisniciBlizu";

    public void GetNearUsers(string body)
    {

        print("Get Near: " + body);

        Task t = new Task(() => { postStuff(getNearUsersUrl, body, AfterGetNearUsers); });
        t.Start();


        void AfterGetNearUsers(string data)
        {

            print("Got Near: " + data);

            KorisniciBlizuDto korb = null;

            try
            {
                korb = JsonUtility.FromJson<KorisniciBlizuDto>(data);
            } catch(Exception e)
            {
                UIController.ShowGameMessage(e.Message, "Error", true);
            }

            if(korb != null)
            {
                SceneController.main.ShowNearPlayers(korb);
            }

        }

    }

    public string getGlobalStatsUri = "/geofighter/globalnaStatistika";

    public void GetGlobalStats()
    {

        Task t = new Task(() => { getStuff(getGlobalStatsUri, AfterGetGlobalStats); });
        t.Start();

        void AfterGetGlobalStats(string data)
        {

            print("Global stats: " + data);

            GlobalStatisticsDto d = null;

            try
            {
                d = JsonUtility.FromJson<GlobalStatisticsDto>(data);
            } catch(Exception e)
            {
                UIController.ShowGameMessage(e.Message);
            }

            if(d != null)
            {
                UIController.main.GlobalStatsPanel.Show(d);
            }

        }

    }

    public string challangePlayerUri = "/geofighter/izazoviIgraca";

    public void ChallangePlayer(string body)
    {

        print("Challenge player: " + body);

        Task t = new Task(() => { postStuff(challangePlayerUri, body, AfterChallangePlayer); });
        t.Start();

        void AfterChallangePlayer(string data)
        {
            PorukaDto p = null;

            try
            {
                p = JsonUtility.FromJson<PorukaDto>(data);
            } catch (Exception e)
            {
                UIController.ShowGameMessage(e.Message, "Error", true);
            }

            if(p != null)
            {
                UIController.ShowGameMessage(p.poruka, "Game Message", true);
            }

        }

    }

    public string playersChallangesUri = "/geofighter/mojiIzazovi";

    public void RequestPlayersChallanges(string body)
    {

        print("Challanges requested: " + body);

        Task t = new Task(() => { postStuff(playersChallangesUri, body, AfterChallangesGot); });
        t.Start();

        void AfterChallangesGot(string data)
        {

            print("Challanges got: " + data);

            IgraceviIzazoviDto d = null;

            try
            {
                d = JsonUtility.FromJson<IgraceviIzazoviDto>(data);
            } catch (Exception e)
            {
                UIController.ShowGameMessage(e.Message, "Error", true);
            }

            if(d != null)
            {
                UIController.main.challengesPanel.Show(d);
            }

        }
    }

    public string playerBattleCheckUri = "/geofighter/provjeriIzazove";

    public void CheckBattle(string body)
    {

        //print("Battle check: " + body);

        Task t = new Task(() => { postStuff(playerBattleCheckUri, body, AfterCheckGot); });
        t.Start();

        void AfterCheckGot(string data)
        {

            NoviIzazoviDto d = null;

            //print("Battle Check Got: " + data);

            try
            {
                d = JsonUtility.FromJson<NoviIzazoviDto>(data);
            }
            catch (Exception e)
            {
                UIController.ShowGameMessage(e.Message, "Error", true);
            }

            if (d != null)
            {
                if (d.kod == "NULL")
                    return;
                if(d.kod == "CHALLENGE")
                {
                    UIController.ShowGameMessage("New challenge from: " + d.korisnickoIme, "Game Message", true);
                    return;
                }
                if(d.kod == "CH_CHALLENGE")
                {
                    UIController.ShowGameMessage("User " + d.korisnickoIme + " has " + (d.prihvacenIzazov ? "accepted" : "rejected") + " your challenge",
                                        "Game Message", true);
                    return;
                }
                if(d.kod == "BATTLE_BEGIN")
                {
                    UIController.ShowGameMessage("Battle begins with: " + d.korisnickoIme, "Game Message", true);

                    IzbrisiKorisnikaDto bd = new IzbrisiKorisnikaDto();
                    bd.korisnickoIme = User.main.username;
                    RequestBattlePrepsInfo(bd.GetJSON());
                }
                if(d.kod == "BATTLE_REJECTED")
                {
                    UIController.ShowGameMessage("User " + d.korisnickoIme + " has cancelled the battle", "Game Message", true);
                    UIController.main.preBattlePanel.Close();
                }
            }

        }

    }

    public string cancelBattleUri = "/geofighter/ponistiBorbu";

    public void RejectBattle(string body)
    {

        print("Reject battle: " + body);

        Task t = new Task(() => { postStuff(cancelBattleUri, body, AfterRejectBattle); });
        t.Start();

        void AfterRejectBattle(string data)
        {

            print("After Reject battle: " + data);

            PorukaDto p = null;

            try
            {
                p = JsonUtility.FromJson<PorukaDto>(data);
            } catch(Exception e)
            {
                UIController.ShowGameMessage(e.Message, "Error parsing JSON", true);
            }

            if(p != null)
            {
                UIController.ShowGameMessage(p.poruka, "Game Message", true);
            }

        }

    }

    public string AnswerChallengeUri = "/geofighter/odgovorNaIzazov";

    public void AnswerChallenge(string body)
    {

        print("Answer battle: " + body);

        Task t = new Task(() => { postStuff(AnswerChallengeUri, body, AfterAnswerChallenge); });
        t.Start();

        void AfterAnswerChallenge(string data)
        {

            print("After answer battle: " + data);

            PorukaDto p = null;

            try
            {
                p = JsonUtility.FromJson<PorukaDto>(data);
            }
            catch (Exception e)
            {
                UIController.ShowGameMessage(e.Message, "Error", true);
            }

            if (p != null)
            {
                UIController.ShowGameMessage(p.poruka, "Game Message", true);
            }

        } 

    }

    public string battlePrepsUri = "/geofighter/preparacijeBorba";

    public void RequestBattlePrepsInfo(string body)
    {

        print("Request battle preps: " + body);

        Task t = new Task(() => { postStuff(battlePrepsUri, body, AfterGotBattlePreps); });
        t.Start();

        void AfterGotBattlePreps(string data)
        {

            print("Got battle preps: " + data);

            BorbaPrepInfoDto d = null;

            try
            {
                d = JsonUtility.FromJson<BorbaPrepInfoDto>(data);
            } catch (Exception e)
            {
                UIController.ShowGameMessage(e.Message, "Error parsing JSON", true);
            }

            if(d != null)
            {

                UIController.main.preBattlePanel.Show(d);

            } else
            {
                UIController.ShowGameMessage("Battle data receive error", "Error", true);
            }

        }

    }

    public string startBattleUri = "/geofighter/zapocniBorbu";

    public void StartBattle(string body)
    {

        print("Starting Battle: " + body);

        Task t = new Task(() => { postStuff(startBattleUri, body, AfterStartBattle); });
        t.Start();

        void AfterStartBattle(string data)
        {

            PorukaDto p = null;

            try
            {
                p = JsonUtility.FromJson<PorukaDto>(data);
            } catch(Exception e)
            {
                UIController.ShowGameMessage(e.Message, "Error", true);
            }

            if(p != null)
            {
                UIController.ShowGameMessage(p.poruka, "Game Message", true);
                UIController.main.preBattlePanel.Close();

                SceneController.main.CurrentState.State = SceneController.GameState.Battle;

            }

        }

    }

    public string borbaUri = "/geofighter/borba";

    public void BattleGet(string body)
    {

        print("Battle: " + body);

        Task t = new Task(() => { postStuff(borbaUri, body, BattleGet); });
        t.Start();

        void BattleGet(string data)
        {

            BorbaPodaciDto d = null;

            try
            {
                d = JsonUtility.FromJson<BorbaPodaciDto>(data);
            } catch(Exception e)
            {
                UIController.ShowGameMessage(e.Message, "Error", true);
            }

            if(d != null)
            {
                Battle.main.ProcessData(d);
            }

        }

    }

    #endregion

    async void getStuff(string uri, OnRequestDone onDone)
    {
        try
        {

            string fullUri = httpURI + uri;

            //print("GET at: " + fullUri);

            HttpResponseMessage response = await client.GetAsync(fullUri);
            response.EnsureSuccessStatusCode();
            string responseBody = await response.Content.ReadAsStringAsync();
            // Above three lines can be replaced with new helper method below
            // string responseBody = await client.GetStringAsync(uri);

            //Console.WriteLine(responseBody);
            FinishedRequestsMutex.WaitOne();
            FinishedRequests.Enqueue((onDone, responseBody));
            FinishedRequestsMutex.ReleaseMutex();

        }
        catch (HttpRequestException e)
        {
            UIController.main.ShowError("Connection error: " + e.Message);
        }

    }

    async void postStuff(string uri, string data, OnRequestDone onDone)
    {
        try
        {

            string fullUri = httpURI + uri;

            StringContent content = new StringContent(data, Encoding.UTF8, "application/json");
            HttpResponseMessage response = await client.PostAsync(fullUri, content);
            response.EnsureSuccessStatusCode();
            string responseBody = await response.Content.ReadAsStringAsync();
            // Above three lines can be replaced with new helper method below
            // string responseBody = await client.GetStringAsync(uri);

            //Console.WriteLine(responseBody);
            FinishedRequestsMutex.WaitOne();
            FinishedRequests.Enqueue((onDone, responseBody));
            FinishedRequestsMutex.ReleaseMutex();

        }
        catch (HttpRequestException e)
        {
            UIController.main.ShowError("Connection error: " + e.Message);
        }
    }

    async void getOSRMStuff(Vector2d[] coords, OnRequestDone onDone)
    {
        try
        {

            string st = "http://router.project-osrm.org/trip/v1/foot/";
            string crds = cordsToString(coords);
            string opts = "?overview=false&source=first&destination=last&steps=true";

            //string fullUri = "http://router.project-osrm.org/route/v1/driving/13.388860,52.517037;13.397634,52.529407;13.428555,52.523219?overview=false";

            string fullUri = st + crds + opts;

            print("OSRM GET at: " + fullUri);

            HttpResponseMessage response = await client.GetAsync(fullUri);
            response.EnsureSuccessStatusCode();
            string responseBody = await response.Content.ReadAsStringAsync();
            // Above three lines can be replaced with new helper method below
            // string responseBody = await client.GetStringAsync(uri);

            //Console.WriteLine(responseBody);
            FinishedRequestsMutex.WaitOne();
            FinishedRequests.Enqueue((onDone, responseBody));
            FinishedRequestsMutex.ReleaseMutex();

            string cordsToString(Mapbox.Utils.Vector2d[] cords)
            {
                StringBuilder sb = new StringBuilder(512);
                for (int i = 0; i < cords.Length; i++)
                {
                    sb.Append(cords[i].y).Append(",").Append(cords[i].x);
                    if (i + 1 < cords.Length)
                    {
                        sb.Append(";");
                    }
                }
                return sb.ToString();
            }

        }
        catch (HttpRequestException e)
        {
            UIController.main.ShowError("Connection error: " + e.Message);
        }

    }

    public string deviceID { get; private set; }

    public static Client main;

    public static string DeviceID { get { return main.deviceID; } }

    public Mutex FinishedRequestsMutex = new Mutex();
    public Queue<(OnRequestDone method, string res)> FinishedRequests = new Queue<(OnRequestDone method, string res)>();

    private void Awake()
    {

        main = this;

        deviceID = SystemInfo.deviceUniqueIdentifier;

    }

    private void Start()
    {

    }

    private void OnDestroy()
    {
        StopAllCoroutines();

        FinishedRequestsMutex?.Close();
        FinishedRequestsMutex = null;

    }

   /*public void coordsGott(Vector2d[] cords)
    {
        print("Coords: " + string.Join(", ", cords));
    }*/

    private void Update()
    {
        /*if(Input.GetKeyDown(KeyCode.O))
        {
            GetOSRMData(new Vector2d[] { new Vector2d(60.191967, 24.9681816), new Vector2d(60.1918373, 24.96694), }, coordsGott);
        }*/
        while(FinishedRequests.Count > 0)
        {
            FinishedRequestsMutex.WaitOne();
            (OnRequestDone method, string res) res = FinishedRequests.Dequeue();
            FinishedRequestsMutex.ReleaseMutex();
            //print(res);
            res.method?.Invoke(res.res);
        }
    }

}
