﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RegistracijaKartografaDto
{

    public String korisnickoIme;
    public String email;
    public String sazetakLozinke;
    public String korisnickaSlika;
    public String IBAN;
    public String slikaOsobneIskaznice;

    public string GetJSON()
    {
        return JsonUtility.ToJson(this);
    }

}

public class RegistracijaIgracaDto
{

    public String korisnickoIme;
    public String email;
    public String sazetakLozinke;
    public String korisnickaSlika;

    public string GetJSON()
    {
        return JsonUtility.ToJson(this);
    }

}


public class RegistracijskiOdgovorDto
{

    public String email;
    public String opis;

    public string GetJSON()
    {
        return JsonUtility.ToJson(this);
    }

}

public class PrijavaDto
{

    public String IDUredaja;
    public String korisnickoIme;
    public String sazetakLozinke;

    public string GetJSON()
    {
        return JsonUtility.ToJson(this);
    }

}

//TODO: add admin control
public class PotvrdaKartografaDto
{

    public bool odobren;
    public String opis;

    public string GetJSON()
    {
        return JsonUtility.ToJson(this);
    }

}

public class OdgovorNaPrijavuDto
{

    public String korisncikoIme;
    public bool uspijeh;
    public String opis;
    public User.UserType tipKorisnika;

    public string GetJSON()
    {
        return JsonUtility.ToJson(this);
    }

}

public class OdgovorNaOdjavuDto
{

    public String korisnickoIme;

    public string GetJSON()
    {
        return JsonUtility.ToJson(this);
    }

}

public class OdjavaDto
{

    public String korisnickoIme;

    public string GetJSON()
    {
        return JsonUtility.ToJson(this);
    }

}

public class UserInfoDto
{

    public String korisnickoIme;
    public String urlFotografije;
    public String uloga;
    public int elo;

    public string GetJSON()
    {
        return JsonUtility.ToJson(this);
    }

}

public class UAC
{

    public String korisnickoIme;
    public String slikaOsobneIskaznice;

    public string GetJSON()
    {
        return JsonUtility.ToJson(this);
    }

}

public class UACOdgovor
{

    public String korisnickoIme;
    public bool prihvacen;

    public string GetJSON()
    {
        return JsonUtility.ToJson(this);
    }

}

public class UACOdgovorInfo
{

    public string poruka;

    public string GetJSON()
    {
        return JsonUtility.ToJson(this);
    }

}

public class ListKorisnikaDto
{

    public String[] ulogiraniKorisniciImena;

    public string GetJSON()
    {
        return JsonUtility.ToJson(this);
    }

}

public class DetaljneInfoDto
{

    public string korisnickoIme;

    public string GetJSON()
    {
        return JsonUtility.ToJson(this);
    }

}

public class DetaljneInfoOdgovorDto
{

    public string korisnickoIme;
    public string exparation;
    public string ulogaKorisnika;
    public int elo;

    public string GetJSON()
    {
        return JsonUtility.ToJson(this);
    }

}

public class IzbrisiKorisnikaDto
{

    public string korisnickoIme;

    public string GetJSON()
    {
        return JsonUtility.ToJson(this);
    }

}

public class UrediKorisnikaPodaciDto
{

    public string korisnickoIme;
    public string originalnoKorisnickoIme;
    public string urlFotografije;
    public string email;
    public int elo;
    public string password;
    public bool verified;
    public string iban;
    public string slikaOsobne;
    public int ulogaKorisnika;

    public string GetJSON()
    {
        return JsonUtility.ToJson(this);
    }

}

public class UrediKorisnikaOdgovorDto
{

    public string poruka;

    public string GetJSON()
    {
        return JsonUtility.ToJson(this);
    }

}

public class ProvjeriGeoDto
{

    public string korisnickoIme;
    public double longitude;
    public double latitude;

    public string GetJSON()
    {
        return JsonUtility.ToJson(this);
    }

}

public class ProvjeriGeoOdgovorDto
{

    public int[] kartaID;

    public double[] koordinateBlizu;
    public int[] poznatiID;

    public string GetJSON()
    {
        return JsonUtility.ToJson(this);
    }

}

public class KartaInfoDto
{

    public string imeKarte;
    public long raritet;
    public string slika;
    public string opis;
    public long id;

    public double longitude;
    public double latitude;

    public string GetJSON()
    {
        return JsonUtility.ToJson(this);
    }

}

public class OdgovorOKDto
{

    public string poruka;

    public string GetJSON()
    {
        return JsonUtility.ToJson(this);
    }

}

public class BanKorisnikaDto
{

    public string korisnickoIme;

    public string exparation;

    public bool banned;

    public string GetJSON()
    {
        return JsonUtility.ToJson(this);
    }

}

[Serializable]
public class OSRMData
{

    [Serializable]
    public class Waypoint
    {
        public string hint;
        public double distance;
        [SerializeField]
        public double[] location;
        public string name;
    }

    public string code;

    public Waypoint[] waypoints;

    public Trip[] trips;

    [Serializable]
    public class Trip
    {

        public double weight;
        public double distance;
        public double duration;
        public Leg[] legs;

    }

    [Serializable]
    public class Leg
    {

        public Step[] steps;

    }

    [Serializable]
    public class Step
    {
        public Intersection[] intersections;
        public Maneuver maneuver;
    }

    [Serializable]
    public class Intersection
    {

        //0 - lat, 1 - lng
        public double[] location;

    }

    [Serializable]
    public class Maneuver
    {

        public double[] location;

    }

}

public class AddLocationDto
{

    public double lng;
    public double lat;
    public string opis;
    public string imageB64;
    public long continent;
    public string country;
    public string town;

    public string playerName;

    public string GetJSON()
    {
        return JsonUtility.ToJson(this);
    }

}

public class AddLocationOdgovorDto
{

    public string poruka;

    public string GetJSON()
    {
        return JsonUtility.ToJson(this);
    }

}

public class CartographerRequestLocation
{

    public string username;
    //-1 daje "random" nepotvrdjenu lokaciju, >= 0 daje lokaciju s tim ID-em
    public long locationid = -1;

    public string GetJSON()
    {
        return JsonUtility.ToJson(this);
    }

}

public class RequestCartographerLocationData
{

    //-1 ako nema lokacija za potvrdu
    public long locationID;

    public double lng;
    public double lat;
    public string opis;
    public string imageB64;
    public long continent;
    public string country;
    public string town;

    public string GetJSON()
    {
        return JsonUtility.ToJson(this);
    }

}

public class AddLocationCartographerDto
{

    public long locationID;
    public double lng;
    public double lat;
    public string opis;
    public string imageB64;
    public long continent;
    public string country;
    public string town;
    public long locationType;
    public string cardName;
    public long cardRarity;
    public double radius;

    public string username;

    public string GetJSON()
    {
        return JsonUtility.ToJson(this);
    }

}

public class PlayerCardsDto {

    public long[] idKarata;

    public string GetJSON()
    {
        return JsonUtility.ToJson(this);
    }

}

[Serializable]
public class OdabraneLokacijeDto
{

    public long[] ids;

    public double[] koordinate;

    public string GetJSON()
    {
        return JsonUtility.ToJson(this);
    }

}

public class OdbijLokacijuDto
{

    public long id;

    public string korisnickoIme;

    public string GetJSON()
    {
        return JsonUtility.ToJson(this);
    }

}

class PorukaDto
{

    public string poruka;
    public string GetJSON()
    {
        return JsonUtility.ToJson(this);
    }

}

public class DohvatiBlizuDto
{

    public string korisnickoIme;

    public double lng;

    public double lat;

    public string GetJSON()
    {
        return JsonUtility.ToJson(this);
    }

}

public class KorisniciBlizuDto
{

    public string[] korisnici;
    public double[] cords;

}

public class GlobalStatisticsDto
{

    public string[] korisnickoIme;
    public int[] elo;

}

public class IzazovIgracaDto
{
    public string izazivac;
    public string ciljniIgrac;

    public int tipBorbe;
    public string grad;
    public string drzava;
    public long kontinent;

    public string GetJSON()
    {
        return JsonUtility.ToJson(this);
    }

}

public class ZahtjevIgracevihIzazovaDto
{
    public string korisnickoIme;

    public string GetJSON()
    {
        return JsonUtility.ToJson(this);
    }

}

public class IgraceviIzazoviDto
{
    public string[] korisnickaImena;
}

[Serializable]
public class OdgovorNaIzazovIgracaDto
{
    public string iz;
    public string ci;
    public bool prihvacen;

    public string GetJSON()
    {
        return JsonUtility.ToJson(this);
    }

}

public class NoviIzazoviDto
{
    //NULL - ništa, CHALLANGE (izazvan od korisnickoIme), CH_CHANGE (korisnickoIme je prihvatilo ili odbilo izazov na temelju prihvacenIzazov)
    public string kod;
    public string korisnickoIme;
    public bool prihvacenIzazov;
}

public class BorbaPrepInfoDto
{

    public string protivnik;
    public int tipBorbe;
    public string imeBorilista;    //ime grada, drzave ili kontinenta

}

public class KorisniciUBorbiDto
{

    public string prviKorisnik;

    public string drugiKorisnik;

    public string GetJSON()
    {
        return JsonUtility.ToJson(this);
    }

}

public class PocetakBorbeDto
{
    public string korisnickoIme;
    public long[] OdabraneKarte;

    public string GetJSON()
    {
        return JsonUtility.ToJson(this);
    }

}

public class BorbaPodaciDto
{

    // KOD MOŽE BITI:
    //             0: igrac karta bacena - IKBA
    //             1: karta bacena info - KBAI
    //             2: info - INFO
    //             3: kraj borbe - END
    //             4: pocetak borbe - BEGIN
    //             5: cekaj drugog - WAIT

    public String kod;
    public long[] baceneKartePrvi;
    public long[] baceneKarteDrugi;
    public double povrsinaPrvog;
    public double povrsinaDrugog;
    public string posiljatelj;
    public long bacenaKarta;

    public string GetJSON()
    {
        return JsonUtility.ToJson(this);
    }

}