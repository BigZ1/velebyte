﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class User : MonoBehaviour
{

    public enum UserType
    {
        Player,
        Cartographer,
        Administrator
    }

    public static User main;

    public UserType Type = UserType.Player;

    public string username;

    public int ELO;

    public Dropdown OptionPicker;

    public Image userImage;

    public Text usernameText;

    public Text usertypeText;

    public CardCollectionPanel cardPanel;

    public LocationAddPanel locationAddPanel;

    private void Awake()
    {
        main = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void UserOptionPick(int option)
    {
        int picked = OptionPicker.value;
        if (picked == OptionPicker.options.Count - 1)
            Logoff();
        
        //active players
        if(picked == 1)
        {
            Client.main.GetActiveUsersList();
        }

        if(picked == 2)
        {
            cardPanel.Show();
        }


        if(picked == 3)
        {
            if (User.main.Type == UserType.Player)
            {
                if (ELO >= 100)
                {
                    locationAddPanel.Show();
                }
                else
                {
                    UIController.ShowGameMessage("You don't have enough ELO points to add locations", "Game Message", true);
                }
            } else
            {
                locationAddPanel.Show();
            }
        }

        if(picked == 4)
        {
            DohvatiBlizuDto d = new DohvatiBlizuDto();
            d.korisnickoIme = username;
            d.lng = PlayerLocator.main.longitude;
            d.lat = PlayerLocator.main.latitude;
            Client.main.GetNearUsers(d.GetJSON());
        }

        if(picked == 5)
        {
            //global stats...
            Client.main.GetGlobalStats();
        }

        if(picked == 6)
        {
            ZahtjevIgracevihIzazovaDto z = new ZahtjevIgracevihIzazovaDto();
            z.korisnickoIme = username;
            Client.main.RequestPlayersChallanges(z.GetJSON());
        }

        OptionPicker.SetValueWithoutNotify(0);  //0 is just close text
    }

    public void Logoff()
    {
        //print("logoff");
        //Client.main.WriteLine("LGO " + Convert.ToBase64String(Encoding.UTF8.GetBytes(username)) + " " + Client.DeviceID);
        //start logoff overlay?...

        OdjavaDto loff = new OdjavaDto();

        loff.korisnickoIme = username;

        Client.main.Logoff(loff.GetJSON());

        //username = null;
        Type = UserType.Player;
        SceneController.main.CurrentState.State = SceneController.GameState.Guest;
        ClearUI();
    }

    public void AddB64Image(string image)
    {
        try
        {
            //print(image);
            //byte[] imgBytes = Convert.FromBase64String(image);
            Texture2D tex = Utils.Base64ToTexture(image);

            if (tex != null)
            {
                userImage.sprite = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), Vector2.zero);
            }
            else
            {
                UIController.ShowGameMessage("Player image loading failed: conversion error");
            }

        }
        catch (Exception e)
        {
            UIController.ShowGameMessage("Player image loading failed: " + e.Message);
        }
    }

    public void ApplyUIChanges()
    {
        usernameText.text = username;
        usertypeText.text = Type.ToString();
    }

    //call after logoff
    public void ClearUI()
    {
        usernameText.text = "User name";
        usertypeText.text = "User type";
        userImage.sprite = null;
    }

}
