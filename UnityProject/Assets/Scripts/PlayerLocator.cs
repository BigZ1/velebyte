﻿using Mapbox.Unity.Location;
using Mapbox.Utils;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerLocator : MonoBehaviour
{


    public float sendLocationInterval = 1f;

    public double longitude;
    public double latitude;

    public static PlayerLocator main;

    public Text enableLocationText;

    public bool Enabled
    {
        get => Input.location.status != LocationServiceStatus.Failed && Input.location.status != LocationServiceStatus.Stopped;
    }

    ILocationProvider _locationProvider;
    ILocationProvider LocationProvider
    {
        get
        {
            if (_locationProvider == null)
            {
                _locationProvider = LocationProviderFactory.Instance.DefaultLocationProvider;
            }

            return _locationProvider;
        }
    }

    public Vector2d Coords
    {
        get => new Vector2d(longitude, latitude);
    }

    //Not used yet:
    /*
    [System.NonSerialized]
    public int locatorRequests = 0;*/

    IEnumerator locationSender = null;
    public void BeginSending()
    {
        if (User.main.Type != User.UserType.Administrator)
        {
            StartCoroutine(locationSender = SendLocation());
        }
    }

    IEnumerator SendLocation()
    {
        WaitForSeconds wi = new WaitForSeconds(sendLocationInterval);

        while(gameObject.activeSelf)
        {
            ProvjeriGeoDto coords = new ProvjeriGeoDto();
            coords.korisnickoIme = User.main.username;
            coords.longitude = longitude;
            coords.latitude = latitude;
            Client.main.SendGeoCoords(coords.GetJSON());
            //print("Coords sent");
            yield return wi;
        }

        yield break;
    }

    private void Awake()
    {
        main = this;
    }

    // Update is called once per frame

    double lng, lat;
    void Update()
    {
        if (User.main.Type != User.UserType.Administrator)
        {
            Vector2d loc = LocationProvider.CurrentLocation.LatitudeLongitude;
            longitude = loc.x;
            latitude = loc.y;
        }

#if !UNITY_EDITOR
        if(!Enabled)
        {
            enableLocationText.gameObject.SetActive(true);
        }
#endif
    }

    private void OnDisable()
    {
        StopAllCoroutines();
    }

}
