﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LocationAddPanel : MonoBehaviour
{

    public GameObject holder;

    public InputField LatInput;
    public InputField LngInput;

    public InputField DescriptionInput;

    public Image LocationImage;

    public Dropdown ContintentInput;

    public InputField CountryName;

    public InputField CityName;

    public void AddLocation()
    {

        AddLocationDto addLocationDto = new AddLocationDto();

        if(!double.TryParse(LatInput.text, out addLocationDto.lat))
        {
            UIController.ShowGameMessage("Wrong latitude value");
            return;
        }

        double lng;

        if (!double.TryParse(LngInput.text, out addLocationDto.lng))
        {
            UIController.ShowGameMessage("Wrong longitude value");
            return;
        }

        addLocationDto.opis = DescriptionInput.text;

        try
        {
            Texture2D tex = LocationImage.sprite.texture;
            TextureScale.Bilinear(tex, 64, 64);
            addLocationDto.imageB64 = Utils.TextureToBase64(tex);
        } catch(Exception)
        {
            UIController.ShowGameMessage("Location needs to have an image");
            return;
        }

        addLocationDto.continent = ContintentInput.value;

        addLocationDto.country = CountryName.text;

        addLocationDto.town = CityName.text;

        addLocationDto.playerName = User.main.username;

        Client.main.AddLocation(addLocationDto.GetJSON());

    }

    public void Show()
    {
        holder.SetActive(true);
        LatInput.text = PlayerLocator.main.latitude.ToString();
        LngInput.text = PlayerLocator.main.longitude.ToString();
    }

    public void Hide()
    {
        holder.SetActive(false);
        LatInput.text = "";
        LngInput.text = "";
        DescriptionInput.text = "";
        LocationImage.sprite = null;
        CountryName.text = "";
        CityName.text = "";
    }

}
