﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;
using System.Text.RegularExpressions;

public class Register : MonoBehaviour
{

    public GameObject playerRegister;
    public GameObject cartographerRegister;

    [Serializable]
    public struct RegisterPlayerControls
    {

        public InputField username;
        public InputField email;
        public InputField password1;
        public InputField password2;
        public ImagePicker image;


        public void Clear()
        {
            username.text = "";
            email.text = "";
            password1.text = "";
            password2.text = "";
            image.image.sprite = null;
        }

    }

    [Serializable]
    public struct RegisterCartographerControls
    {

        public InputField username;
        public InputField email;
        public InputField password1;
        public InputField password2;
        public ImagePicker image;
        public InputField iban;
        public ImagePicker identityCard;

        public void Clear()
        {
            username.text = "";
            email.text = "";
            password1.text = "";
            password2.text = "";
            image.image.sprite = null;
            iban.text = "";
            identityCard.image.sprite = null;
        }

    }

    public RegisterPlayerControls plReg;

    public RegisterCartographerControls crReg;

    public void BackToLogin()
    {
        UIController.main.UIState.State = UIController.UIHolderType.Login;
        plReg.Clear();
        crReg.Clear();
    }

    public void SwitchToRegisterPlayer()
    {
        cartographerRegister.SetActive(false);
        playerRegister.SetActive(true);
        crReg.Clear();
    }

    public void SwitchToRegisterCartographer()
    {
        cartographerRegister.SetActive(true);
        playerRegister.SetActive(false);
        plReg.Clear();
    }

    public void RegisterPlayer()
    {

        RegistracijaIgracaDto newPlayer = new RegistracijaIgracaDto();

        string username = plReg.username.text;
        if(string.IsNullOrEmpty(username) || string.IsNullOrWhiteSpace(username))
        {
            UIController.ShowGameMessage("Username cannot be empty", "Game message");
            return;
        }

        newPlayer.korisnickoIme = username;

        string email = plReg.email.text;
        if (string.IsNullOrEmpty(email) || string.IsNullOrWhiteSpace(email))
        {
            UIController.ShowGameMessage("Email cannot be empty", "Game message");
            return;
        }
        if(!EmailValidator.EmailIsValid(email))
        {
            UIController.ShowGameMessage("Invalid email", "Game message");
            return;
        }

        newPlayer.email = email;

        string password1 = plReg.password1.text;
        if (string.IsNullOrEmpty(password1) || string.IsNullOrWhiteSpace(password1))
        {
            UIController.ShowGameMessage("Password cannot be empty", "Game message");
            return;
        }
        string password2 = plReg.password2.text;
        if(password1 != password2)
        {
            UIController.ShowGameMessage("Given passwords do not match", "Game message");
            return;
        }
        string pp = PasswordProtect.protectPassword(password1);

        newPlayer.sazetakLozinke = pp;

        Texture2D image;

        if(plReg.image.image.sprite != null && plReg.image.image.sprite.texture != null)
        {
            image = plReg.image.image.sprite.texture;
        } else
        {
            UIController.ShowGameMessage("You need profile picture to make account", "Game message");
            return;
        }

        //sb.Append(Utils.TextureToBase64(image));

        //TODO: slika
        TextureScale.Bilinear(image, 64, 64);
        newPlayer.korisnickaSlika = Utils.TextureToBase64(image);
        //newPlayer.korisnickaSlika = "no img";

        //newPlayer.korisnickaSlika = "no img";

        //print("register player: " + sb.Length);

        //Client.main.WriteLine(sb.ToString());


        print(newPlayer.GetJSON());
        Client.main.RegisterPlayer(newPlayer.GetJSON());

        //Client.main.ReadLine(Protocol.main.ProcessMessage);

    }

    public void RegisterCartographer()
    {

        /*StringBuilder sb = new StringBuilder(2048);
        sb.Append("RGC ");*/

        RegistracijaKartografaDto newCartographer = new RegistracijaKartografaDto();

        string username = crReg.username.text;
        if (string.IsNullOrEmpty(username) || string.IsNullOrWhiteSpace(username))
        {
            UIController.ShowGameMessage("Username cannot be empty", "Game message");
            return;
        }
        //username = Convert.ToBase64String(Encoding.UTF8.GetBytes(username));

        newCartographer.korisnickoIme = username;

        string email = crReg.email.text;
        if (string.IsNullOrEmpty(email) || string.IsNullOrWhiteSpace(email))
        {
            UIController.ShowGameMessage("Email cannot be empty", "Game message");
            return;
        }
        if (!EmailValidator.EmailIsValid(email))
        {
            UIController.ShowGameMessage("Invalid email", "Game message");
            return;
        }

        newCartographer.email = email;

        string password1 = crReg.password1.text;
        if (string.IsNullOrEmpty(password1) || string.IsNullOrWhiteSpace(password1))
        {
            UIController.ShowGameMessage("Password cannot be empty", "Game message");
            return;
        }
        string password2 = crReg.password2.text;
        if (password1 != password2)
        {
            UIController.ShowGameMessage("Given passwords do not match", "Game message");
            return;
        }
        string pp = PasswordProtect.protectPassword(password1);

        newCartographer.sazetakLozinke = pp;

        Texture2D image;

        if (crReg.image.image.sprite != null && crReg.image.image.sprite.texture != null)
        {
            image = crReg.image.image.sprite.texture;
        }
        else
        {
            UIController.ShowGameMessage("You need profile picture to make account", "Game message");
            return;
        }

        /*sb.Append(Utils.TextureToBase64(image));
        sb.Append(' ');*/

        //TODO: slika
        TextureScale.Bilinear(image, 64, 64);
        newCartographer.korisnickaSlika = Utils.TextureToBase64(image);
        //newCartographer.korisnickaSlika = "noimg";

        string iban = crReg.iban.text;

        if(string.IsNullOrEmpty(iban) ||string.IsNullOrWhiteSpace(iban))
        {
            UIController.ShowGameMessage("IBAN cannot be empty", "Game message");
            return;
        }
        if(!IBANValidator.ValidateBankAccount(iban))
        {
            UIController.ShowGameMessage("Given IBAN is invalid", "Game message");
            return;
        }

        /*sb.Append(iban);
        sb.Append(' ');*/

        newCartographer.IBAN = iban;

        Texture2D image2;

        if (crReg.identityCard.image.sprite != null && crReg.identityCard.image.sprite.texture != null)
        {
            image2 = crReg.identityCard.image.sprite.texture;
        }
        else
        {
            UIController.ShowGameMessage("You need to give idenitiy card picture to make cartographer account", "Game message");
            return;
        }

        //sb.Append(Utils.TextureToBase64(image2));

        TextureScale.Bilinear(image2, 128, 128);
        newCartographer.slikaOsobneIskaznice = Utils.TextureToBase64(image2);
        print(newCartographer.slikaOsobneIskaznice);
        //newCartographer.slikaOsobneIskaznice = "no img";

        //print(newCartographer.GetJSON());
        Client.main.RegisterCartographer(newCartographer.GetJSON());

        //Client.main.ReadLine(Protocol.main.ProcessMessage);

    }

    /// <summary>
    /// Source: https://stackoverflow.com/questions/1365407/c-sharp-code-to-validate-email-address
    /// </summary>
    public static class EmailValidator
    {

        static Regex ValidEmailRegex = CreateValidEmailRegex();

        /// <summary>
        /// Taken from http://haacked.com/archive/2007/08/21/i-knew-how-to-validate-an-email-address-until-i.aspx
        /// </summary>
        /// <returns></returns>
        private static Regex CreateValidEmailRegex()
        {
            string validEmailPattern = @"^(?!\.)(""([^""\r\\]|\\[""\r\\])*""|"
                + @"([-a-z0-9!#$%&'*+/=?^_`{|}~]|(?<!\.)\.)*)(?<!\.)"
                + @"@[a-z0-9][\w\.-]*[a-z0-9]\.[a-z][a-z\.]*[a-z]$";

            return new Regex(validEmailPattern, RegexOptions.IgnoreCase);
        }

        public static bool EmailIsValid(string emailAddress)
        {
            bool isValid = ValidEmailRegex.IsMatch(emailAddress);

            return isValid && !string.IsNullOrEmpty(emailAddress);
        }
    }


}
