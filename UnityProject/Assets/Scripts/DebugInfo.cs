﻿using Mapbox.Unity.Location;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DebugInfo : MonoBehaviour
{

    public Text DebugText;

    public static DebugInfo main;

    ILocationProvider _locationProvider;
    ILocationProvider LocationProvider
    {
        get
        {
            if (_locationProvider == null && LocationProviderFactory.Instance != null)
            {
                _locationProvider = LocationProviderFactory.Instance.DefaultLocationProvider;
            }

            return _locationProvider;
        }
    }

    WaitForSeconds w1 = new WaitForSeconds(1f);

    private void Awake()
    {
        main = this;
    }

    // Start is called before the first frame update
    void Start()
    {
        StartCoroutine(UpdateDebug());
    }

    IEnumerator UpdateDebug()
    {
        while (true)
        {
            
            DebugText.text = "";
            DebugText.text += "FPS: " + (1f / Time.unscaledDeltaTime) + "\n";
            if (LocationProvider != null)
            {
                Mapbox.Utils.Vector2d loc = LocationProvider.CurrentLocation.LatitudeLongitude;
                DebugText.text += "Current Location: lng: " + loc.x + ", lat: " + loc.y + "\n";
            }
            yield return w1;
        }
    }

    public void Toggle()
    {
        gameObject.SetActive(!gameObject.activeSelf);
    }

}
