﻿using Mapbox.Unity.Map;
using Mapbox.Utils;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MapPointShow : MonoBehaviour
{

    public AbstractMap map;

    public GameObject pointObject;

    public Vector3 placeOffset = Vector3.up;

    [System.Serializable]
    public struct PlacePoint
    {
        public Vector2d Place;
        public GameObject Object;
    }

    public List<PlacePoint> pointObjects = new List<PlacePoint>();

    public virtual GameObject PlaceOn(Vector2d point)
    {
        if (!ContainsPosition(point))
        {
            Vector3 pos = map.GeoToWorldPosition(point, false);
            PlacePoint pp = new PlacePoint
            {
                Place = point,
                Object = Instantiate(pointObject, pos + placeOffset, Quaternion.identity, transform)
            };
            pointObjects.Add(pp);
            return pp.Object;
        }
        return null;
    }

    public virtual void TakeAway(GameObject g)
    {
        for(int i = 0; i < pointObjects.Count; i++)
        {
            if(pointObjects[i].Object == g)
            {
                pointObjects.RemoveAt(i);
                return;
            }
        }
    }

    public virtual void RemoveOn(Vector2d point, double deltaDistance = 0.0001f)
    {
        for (int i = 0; i < pointObjects.Count; i++)
        {
            if (Utils.LngLatDistance(point, pointObjects[i].Place) <= deltaDistance /*pointObjects[i].Place.x == point.x && pointObjects[i].Place.y == point.y*/)
            {
                Destroy(pointObjects[i].Object);
                pointObjects.RemoveAt(i);
                return;
            }
        }

        

    }

    public virtual void Clear()
    {
        Utils.ClearChildren(transform);
        pointObjects.Clear();
    }

    private void OnDisable()
    {
        Utils.ClearChildren(transform);
        pointObjects.Clear();
    }

    public bool ContainsPosition(Vector2d pos, double deltaDist = 0.0001)
    {
        for (int i = 0; i < pointObjects.Count; i++)
        {
            if (Utils.LngLatDistance(pos, pointObjects[i].Place) <= deltaDist)
            {
                return true;
            }
        }
        return false;
    }

    public void Reposition()
    {
        for(int i = 0; i < pointObjects.Count; i++)
        {
            pointObjects[i].Object.transform.position = map.GeoToWorldPosition(pointObjects[i].Place, false) + placeOffset;
        }
    }

    private void Update()
    {
        /*if (Input.GetKeyDown(KeyCode.S))
        {
            PlaceOn(new Vector2d(60.19219, 24.9673538));
            PlaceOn(new Vector2d(60.191925, 24.9669418));
        }*/
    }

}
