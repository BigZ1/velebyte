﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BanUser : MonoBehaviour
{

    public GameObject holder;

    public Text UserEditText;

    public InputField ExparationInput;
    public Toggle Banned;

    public void Show(BanKorisnikaDto data)
    {
        holder.SetActive(true);

        UserEditText.text = data.korisnickoIme;
        ExparationInput.text = data.exparation;
        Banned.isOn = data.banned;

    }

    public void Hide()
    {
        holder.SetActive(false);
    }

    public void Apply() {
        BanKorisnikaDto b = new BanKorisnikaDto();
        b.korisnickoIme = UserEditText.text;
        b.exparation = ExparationInput.text;
        b.banned = Banned.isOn;
        Client.main.BanUser(b.GetJSON());
    }

}
