﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CartographerLocationPanel : MonoBehaviour
{

    public GameObject holder;

    public InputField LatitiudeInput;
    public InputField LongitudeInput;

    public InputField DescriptionInput;
    public Image ImageInput;
    public Dropdown ContinentInput;
    public InputField CountryNameInput;
    public InputField TownNameInput;
    public InputField CardNameInput;
    public Dropdown RarityInput;
    public Dropdown LocationTypeinput;
    public InputField RadiusInput;

    public Button AddPicksButton;

    public long LocationID = -1;

    private void Awake()
    {
        if (!holder)
            holder = gameObject;
    }

    public void Close()
    {
        LocationID = -1;
        holder.SetActive(false);

        LatitiudeInput.text = "";
        LongitudeInput.text = "";
        DescriptionInput.text = "";
        ImageInput.sprite = null;
        CountryNameInput.text = "";
        TownNameInput.text = "";
        CardNameInput.text = "";
        RadiusInput.text = "";

    }

    public void Show(RequestCartographerLocationData data, bool final = false)
    {
        if (data != null)
        {

            holder.SetActive(true);

            AddPicksButton.gameObject.SetActive(!final);

            LocationID = data.locationID;
            LatitiudeInput.text = data.lat.ToString();
            LongitudeInput.text = data.lng.ToString();
            DescriptionInput.text = data.opis;
            try
            {
                ImageInput.sprite = Utils.Base64ToSprite(data.imageB64);
            }
            catch (Exception e)
            {
                UIController.ShowGameMessage(e.Message);
            }

            ContinentInput.value = (int)data.continent;
            CountryNameInput.text = data.country;
            TownNameInput.text = data.town;
        }
    }

    /*public void NextRequest()
    {
        LocationID = -1;
        CartographerRequestLocation req = new CartographerRequestLocation();
        req.username = User.main.username;
        Client.main.RequestLocationInfo(req.GetJSON());
    }*/

    public void Accept()
    {
        AddLocationCartographerDto loc = new AddLocationCartographerDto();

        double val;
        
        loc.locationID = LocationID;

        if(double.TryParse(LatitiudeInput.text, out val))
        {
            loc.lat = val;
        } else
        {
            UIController.ShowGameMessage("Wrong latitude");
            return;
        }

        if (double.TryParse(LongitudeInput.text, out val))
        {
            loc.lng = val;
        }
        else
        {
            UIController.ShowGameMessage("Wrong longitude");
            return;
        }

        if(!string.IsNullOrEmpty(DescriptionInput.text))
        {
            loc.opis = DescriptionInput.text;
        } else
        {
            UIController.ShowGameMessage("Description cannot be empty");
            return;
        }

        try
        {
            loc.imageB64 = Utils.TextureToBase64(ImageInput.sprite.texture);
        } catch(Exception e)
        {
            UIController.ShowGameMessage(e.Message);
        }

        loc.continent = ContinentInput.value;

        if (!string.IsNullOrEmpty(CountryNameInput.text))
        {
            loc.country = CountryNameInput.text;
        }
        else
        {
            UIController.ShowGameMessage("Country name cannot be empty");
            return;
        }

        loc.town = TownNameInput.text;

        loc.locationType = LocationTypeinput.value;

        if (!string.IsNullOrEmpty(CardNameInput.text))
        {
            loc.cardName = CardNameInput.text;
        }
        else
        {
            UIController.ShowGameMessage("Card name cannot be empty");
            return;
        }

        loc.cardRarity = RarityInput.value;

        if (double.TryParse(RadiusInput.text, out val))
        {
            loc.radius = val;
        }
        else
        {
            UIController.ShowGameMessage("Wrong radius");
            return;
        }

        loc.username = User.main.username;

        Client.main.CartographerAcceptLocation(loc.GetJSON());

    }

    public void AddToVisitList()
    {
        AddLocationCartographerDto loc = new AddLocationCartographerDto();

        double val;

        loc.locationID = LocationID;

        if (double.TryParse(LatitiudeInput.text, out val))
        {
            loc.lat = val;
        }
        else
        {
            UIController.ShowGameMessage("Wrong latitude");
            return;
        }

        if (double.TryParse(LongitudeInput.text, out val))
        {
            loc.lng = val;
        }
        else
        {
            UIController.ShowGameMessage("Wrong longitude");
            return;
        }

        if (!string.IsNullOrEmpty(DescriptionInput.text))
        {
            loc.opis = DescriptionInput.text;
        }
        else
        {
            UIController.ShowGameMessage("Description cannot be empty");
            return;
        }

        try
        {
            loc.imageB64 = Utils.TextureToBase64(ImageInput.sprite.texture);
        }
        catch (Exception e)
        {
            UIController.ShowGameMessage(e.Message);
        }

        loc.continent = ContinentInput.value;

        if (!string.IsNullOrEmpty(CountryNameInput.text))
        {
            loc.country = CountryNameInput.text;
        }
        else
        {
            UIController.ShowGameMessage("Country name cannot be empty");
            return;
        }

        loc.town = TownNameInput.text;

        loc.locationType = LocationTypeinput.value;

        if (!string.IsNullOrEmpty(CardNameInput.text))
        {
            loc.cardName = CardNameInput.text;
        }
        else
        {
            //UIController.ShowGameMessage("Card name cannot be empty");
            //return;
        }

        loc.cardRarity = RarityInput.value;

        if (double.TryParse(RadiusInput.text, out val))
        {
            loc.radius = val;
        }
        else
        {
            //UIController.ShowGameMessage("Wrong radius");
            //return;
        }

        loc.username = User.main.username;

        Client.main.AddCartographerLocationCheck(loc.GetJSON());
        Close();

    }


    public void Reject()
    {
        OdbijLokacijuDto o = new OdbijLokacijuDto();
        o.id = LocationID;
        o.korisnickoIme = User.main.username;
        Client.main.RejectLocation(o.GetJSON());
        Close();
    }

}
