﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class MapPointerObject : MonoBehaviour
{

    public Vector3 angularVeclocity;

    public UnityEvent OnPlayerEnter;

    public float disappearTime = 1f;

    private void Update()
    {
        transform.eulerAngles += angularVeclocity * Time.deltaTime;
    }

    IEnumerator Disappear(float time)
    {
        float t = 0;
        Vector3 originalScale = transform.localScale;
        while(t <= time)
        {
            transform.localScale = originalScale * (1 - t / time);
            t += Time.deltaTime;
            yield return null;
        }
        SceneController.main.MapPointShow.TakeAway(gameObject);
        Destroy(gameObject);
    }

    public void DestroyItself()
    {
        StartCoroutine(Disappear(disappearTime));
    }

    private void OnTriggerEnter(Collider other)
    {
        PlayerMapObject p = other.GetComponent<PlayerMapObject>();
        if(p)
        {
            if (OnPlayerEnter != null)
                OnPlayerEnter.Invoke();
        }
    }

}
