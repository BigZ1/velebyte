﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
using System;
using System.Text;
/*
public class Protocol : MonoBehaviour
{

    public enum ProtocolState
    {
        Guest,
        LoggedIn,
        BattlePrep,
        Battle
    }

    public static Protocol main;

    public ProtocolState startState = ProtocolState.Guest;

    Dictionary<string, Command> knownCommands;
    private void Awake()
    {
        main = this;

        knownCommands = new Dictionary<string, Command> {
            { "LGS", main.LoginSuccess },
            { "LGF", main.LoginFail },
            { "RPR", main.RegisterPlayerReceived },
            { "RPE", main.RegisterPlayerError },
            { "RCR", main.RegisterCartographerReceived },
            { "RCE", main.RegisterCartographerError },
            { "LOR", main.LogOffReceived },
            { "SUD", main.SendUserData },
        };

    }

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    #region COMMAND_METHODS

    public void LoginSuccess(string[] parts)
    {
        if (parts.Length > 0)
        {
            if(parts.Length > 3)
            {
                UIController.ShowGameMessage("Login fail");
            } else
            {
                
                if (parts[1] == Client.DeviceID)
                {
#if UNITY_EDITOR
                    //print("logged in as: " + parts[2]);
#endif
                    User.main.Type = (User.UserType)Enum.Parse(typeof(User.UserType), parts[2]);

                    //TODO: load "logged in UI"

                    //Client.main.WriteLine("RUD " + " " + Client.DeviceID);



                    SceneController.main.CurrentState.State = SceneController.GameState.InGame;

                }
                else
                {
#if UNITY_EDITOR
                    print("Wrong device ID");
#endif
                    UIController.ShowGameMessage("Wrong device ID");
                }
            }
        } else
        {
            //print("login error");
            UIController.ShowGameMessage("Login error");
        }
    }

    public void LoginFail(string[] parts)
    {
        //print("login fail " + string.Join(" ", parts));
        if(parts.Length < 2)
        {
            UIController.ShowGameMessage("Login error");
        } else
        {
            switch(parts[1])
            {
                case "username":
                    UIController.ShowGameMessage("Wrong username", "Game message");
                    break;
                case "password":
                    UIController.ShowGameMessage("Wrong password", "Game message");
                    break;
                case "acc_not_confirmed":
                    UIController.ShowGameMessage("This account has not been confirmed", "Game message");
                    break;
                case "already_loggedin":
                    UIController.ShowGameMessage("This user is already logged in", "Game message");
                    break;
                case "banned":
                    if(parts.Length < 3)
                    {
                        //pernament ban
                        UIController.ShowGameMessage("This account is pernamently banned.", "Game message");
                        break;
                    }
                    string date = parts[2];
                    UIController.ShowGameMessage("This account is banned. Ban will expire after: " + date, "Game message");
                    break;
                default:
                    UIController.ShowGameMessage("Login error: unkonwn response");
                    break;
            }
        }
    }

    public void RegisterPlayerReceived(string[] parts)
    {
        if(parts.Length < 2)
        {
            UIController.ShowGameMessage("Register error");
            return;
        }

        UIController.ShowGameMessage("An email was sent to: " + parts[1] + " for conformation.",  "Game Message");

    }

    public void RegisterPlayerError(string[] parts)
    {

        if(parts.Length < 2)
        {
            UIController.ShowGameMessage("Register error");
            return;
        }

        switch(parts[1])
        {
            case "username":
                UIController.ShowGameMessage("Given username is taken");
                break;
            case "email":
                UIController.ShowGameMessage("Given email is taken");
                break;
            case "bad_req":
                UIController.ShowGameMessage("Bad request");
                break;
            default:
                UIController.ShowGameMessage("Register error: unknown response");
                break;
        }

    }

    public void RegisterCartographerReceived(string[] parts)
    {
        if (parts.Length < 2)
        {
            UIController.ShowGameMessage("Register error");
            return;
        }

        UIController.ShowGameMessage("An email was sent to: " + parts[1] + 
                                        " for conformation. You will also need for an administrator to confirm your account", "Game Message");

    }

    public void RegisterCartographerError(string[] parts)
    {

        if (parts.Length < 2)
        {
            UIController.ShowGameMessage("Register error");
            return;
        }

        switch (parts[1])
        {
            case "username":
                UIController.ShowGameMessage("Given username is taken");
                break;
            case "email":
                UIController.ShowGameMessage("Given email is taken");
                break;
            case "bad_req":
                UIController.ShowGameMessage("Bad request");
                break;
            default:
                UIController.ShowGameMessage("Register error: unknown response");
                break;
        }

    }

    public void LogOffReceived(string[] parts)
    {
        if(parts[1] == Client.DeviceID)
        {
            UIController.ShowGameMessage("Log off successfull!", "Game Message");
        } else
        {
            //Error
#if UNITY_EDITOR
            print("Got wrong deviceID when logging off");
#endif
        }
    }

    public void SendUserData(string[] parts)
    {
        if(parts.Length < 4)
        {
            UIController.ShowGameMessage("Game data receive error");
            return;
        }
        string username = Encoding.UTF8.GetString(Convert.FromBase64String(parts[1]));
        //print("Got username: " + username + " And image: " + parts[2]);
        User.main.AddB64Image(parts[2]);
        User.main.username = username;
        User.main.Type = (User.UserType)Enum.Parse(typeof(User.UserType), parts[3]);
        User.main.ApplyUIChanges();
    }

    #endregion

    delegate void Command(string[] parts);


    char[] splitters = new char[]
    {
        ' ',
    };

    public void ProcessMessage(string message)
    {
        if (message == null)
            return;

        message = message.Replace("\n", "").Replace("\r", "");

        //print('\"' + message + '\"');

        string[] parts = message.Split(splitters, StringSplitOptions.RemoveEmptyEntries);
        if(parts.Length > 0)
        {
            if(knownCommands.ContainsKey(parts[0]))
            {
                knownCommands[parts[0]].Invoke(parts);
            } else
            {
                print("Unknown command: " + parts[0]);
            }
        }
    }

}
*/