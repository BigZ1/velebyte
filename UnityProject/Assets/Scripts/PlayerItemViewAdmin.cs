﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerItemViewAdmin : PlayerItemView
{

    public override void ProcessOptionSelection(int state)
    {
        int pick = playerOptionsDropdown.value;


        //info
        if(pick == 1)
        {
            DetaljneInfoDto body = new DetaljneInfoDto();
            body.korisnickoIme = playerName;
            Client.main.GetDetailedPlayerInfo(body.GetJSON());
        }

        //ban
        if(pick == 2)
        {
            DetaljneInfoDto body = new DetaljneInfoDto();
            body.korisnickoIme = playerName;
            Client.main.BannedCheck(body.GetJSON());
        }

        //edit data
        if(pick == 3)
        {
            DetaljneInfoDto body = new DetaljneInfoDto();
            body.korisnickoIme = playerName;
            Client.main.RequestEditUserData(body.GetJSON());
        }

        //delete user
        if(pick == 4)
        {
            IzbrisiKorisnikaDto obj = new IzbrisiKorisnikaDto();
            obj.korisnickoIme = playerName;
            Client.main.DeletePlayer(obj.GetJSON());
        }

        playerOptionsDropdown.SetValueWithoutNotify(0);
    }

}
