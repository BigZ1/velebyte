﻿using Mapbox.Utils;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Utils
{
    
    public static string TextureToBase64(Texture2D tex)
    {
        byte[] imgBytes = tex.EncodeToPNG();
        return System.Convert.ToBase64String(imgBytes);
    }

    public static Texture2D Base64ToTexture(string imgData)
    {
        byte[] imgBytes = System.Convert.FromBase64String(imgData);
        Texture2D tex = new Texture2D(1, 1);
        ImageConversion.LoadImage(tex, imgBytes);
        return tex;
    }

    public static void ClearChildren(Transform t)
    {
        for (int i = t.childCount - 1; i >= 0; i--)
        {
            UnityEngine.Object.Destroy(t.GetChild(i).gameObject);
        }
    }

    public static Sprite Base64ToSprite(string imgData)
    {
        try
        {
            Texture2D tex = Base64ToTexture(imgData);
            return Sprite.Create(tex, new Rect(Vector2.zero, new Vector2(tex.width, tex.height)), Vector2.zero);
        }
        catch (Exception e)
        {
            UIController.ShowGameMessage(e.Message);
        }
        return null;
    }

    public static Vector3 SmoothDampAngles(Vector3 s, Vector3 e, ref Vector3 v, float t)
    {
        return new Vector3(Mathf.SmoothDampAngle(s.x, e.x, ref v.x, t),
                            Mathf.SmoothDampAngle(s.y, e.y, ref v.y, t),
                            Mathf.SmoothDampAngle(s.z, e.z, ref v.z, t));
    }

    public static double LngLatDistance(Vector2d p1, Vector2d p2)
    {
        double rad(double angle) => angle * 0.017453292519943295769236907684886127d; // = angle * Math.Pi / 180.0d
        double havf(double diff) => Math.Pow(Math.Sin(rad(diff) / 2d), 2); // = sin²(diff / 2)
        return 12745.6 * Math.Asin(Math.Sqrt(havf(p2.y - p1.y) + Math.Cos(rad(p1.y)) * Math.Cos(rad(p2.y)) * havf(p2.x - p1.x))); // earth radius 6.372,8‬km x 2 = 12745.6
    }

    //Source: https://forum.unity.com/threads/re-map-a-number-from-one-range-to-another.119437/
    public static double MapRange(this double value, double from1, double to1, double from2, double to2)
    {
        return (value - from1) / (to1 - from1) * (to2 - from2) + from2;
    }

}
