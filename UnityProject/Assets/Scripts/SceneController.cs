﻿using Mapbox.Unity.Map;
using Mapbox.Utils;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SceneController : MonoBehaviour
{
    
    public enum GameState
    {
        Guest,  //login or register
        InGame, //logged in
        Battle, //in battle
    }

    public static SceneController main;

    public GameState startState = GameState.Guest;

    public StateMachine<GameState> CurrentState;

    public PlayerCamera PlayerCamera;

    public PlayerLocator PlayerLocator;

    public GameObject MapWorld;

    public MapPointShow MapPointShow;

    public MapCardShow MapCardShow;

    public MapPointShow MapUsersShow;

    public Dropdown mapUsersDropdown;

    public Canvas mainCanvas;

    public PathDrawer pathDrawer;

    public MapPointShow LocationTargetPointShow;

    public AbstractMap sceneMap;

    public float defaultZoomValue = 17;

    public static AbstractMap Map
    {
        get => main.sceneMap;
    }

    private void Awake()
    {

        main = this;
        //print("awake");
        Battle.main = FindObjectOfType<Battle>(true);
        //print(Battle.main);

        CurrentState = new StateMachine<GameState>(startState);

        CurrentState.StateEnterMethods[GameState.Guest] = ToGuest;
        CurrentState.StateEnterMethods[GameState.InGame] = ToGame;
        CurrentState.StateEnterMethods[GameState.Battle] = ToBattle;

        if (!PlayerCamera)
            PlayerCamera = FindObjectOfType<PlayerCamera>();

        if (!PlayerLocator)
            PlayerLocator = FindObjectOfType<PlayerLocator>(true);

        MapWorld.SetActive(false);
        sceneMap.SetZoom(defaultZoomValue);
        //sceneMap.UpdateMap();
    }

    private void Start()
    {
        Client.main.CheckServerOK();
    }

    #region STATE_SWITCHES

    void ToGuest()
    {
        UIController.main.UIState.State = UIController.UIHolderType.Login;
        PlayerCamera.main.targetObjects.Clear();
        PlayerCamera.CameraState.State = PlayerCamera.CameraStates.Idle;
        MapWorld.SetActive(false);
        Map.SetZoom(defaultZoomValue);
        StopAllCoroutines();
        challengeChecker = null;
    }

    void ToGame()
    {
        UIController.main.UIState.State = UIController.UIHolderType.InGame;
        UIController.main.LoadUIForUser(User.main.Type);
        PlayerCamera.CameraState.State = PlayerCamera.CameraStates.Playing;
        MapWorld.SetActive(true);
        if(challengeChecker == null)
        {
            StartCoroutine(challengeChecker = CheckChallenges());
        }
        PlayerMapObject.main.gameObject.SetActive(true);
        Map.SetExtent(MapExtentType.CameraBounds);
    }

    void ToBattle()
    {
        print("To Battle!");
        UIController.main.UIState.State = UIController.UIHolderType.Battle;
        pathDrawer.Clear();
        LocationTargetPointShow.Clear();
        MapPointShow.Clear();
        MapCardShow.Clear();
        MapWorld.SetActive(true);
        //Battle.main.gameObject.SetActive(true);
        if (challengeChecker != null)
            StopCoroutine(challengeChecker);
        challengeChecker = null;
        PlayerMapObject.main.gameObject.SetActive(false);
        PlayerCamera.main.targetObjects.Clear();
        PlayerCamera.CameraState.State = PlayerCamera.CameraStates.LookAtObjects;
        Map.SetExtent(MapExtentType.RangeAroundCenter);
        Battle.main.Begin();
    }

    #endregion

    IEnumerator challengeChecker = null;
    IEnumerator CheckChallenges()
    {
        while(true)
        {
            ZahtjevIgracevihIzazovaDto z = new ZahtjevIgracevihIzazovaDto();
            z.korisnickoIme = User.main.username;
            Client.main.CheckBattle(z.GetJSON());
            yield return new WaitForSeconds(1.5f);
        }
    }

    public void ShowNearPlayers(KorisniciBlizuDto data)
    {
        if(data != null && data.korisnici != null && data.cords != null && data.korisnici.Length > 0)
        {
            for(int i = 0; i < data.korisnici.Length; i++)
            {
                Vector2d cord = new Vector2d(data.cords[i * 2], data.cords[i * 2 + 1]);
                GameObject g = MapUsersShow.PlaceOn(cord);
                
                if (g != null)
                {
                    PlayerCamera.main.targetObjects.Add(g);
                    PlayerMapInfo pmi = g.GetComponent<PlayerMapInfo>();
                    pmi.playerName = data.korisnici[i];
                    pmi.cords = cord;

                    pmi.dropdown = Instantiate(mapUsersDropdown, mainCanvas.transform);
                    pmi.dropdown.gameObject.SetActive(false);
                    pmi.dropdown.onValueChanged.AddListener(pmi.OnDropdownSelect);
                }

            }
            UIController.main.hideNearPlayersButton.gameObject.SetActive(true);
            PlayerCamera.main.targetObjects.Add(PlayerMapObject.main.gameObject);
            Vector2d[] targets = new Vector2d[PlayerCamera.main.targetObjects.Count];
            for(int i = 0; i < targets.Length; i++)
            {
                targets[i] = Map.WorldToGeoPosition(PlayerCamera.main.targetObjects[i].transform.position);
            }
            float zoom = (float)CalculateMapZoomToFit(targets);
            Map.SetZoom(zoom);
            Map.UpdateMap();
            PlayerMapObject.main.transform.position = Map.GeoToWorldPosition(PlayerLocator.main.Coords);
            MapUsersShow.Reposition();
            MapCardShow.Reposition();
            MapPointShow.Reposition();
            pathDrawer.linesDrawer.positionCount = 0;
            PlayerCamera.main.CameraState.State = PlayerCamera.CameraStates.LookAtObjects;
        }
    }

    public void HideNearPlayers()
    {
        UIController.main.hideNearPlayersButton.gameObject.SetActive(false);
        MapUsersShow.Clear();
        Map.SetZoom(defaultZoomValue);
        Map.UpdateMap();
        PlayerMapObject.main.transform.position = Map.GeoToWorldPosition(PlayerLocator.main.Coords);
        MapCardShow.Reposition();
        MapPointShow.Reposition();
        PlayerCamera.main.targetObjects.Clear();
        PlayerCamera.CameraState.State = PlayerCamera.CameraStates.Playing;
    }

    public double CalculateMapZoomToFit(Vector2d[] targetPoints, double minZoom = 0, double maxZoom = 17)
    {
        double res = 0;

        if (targetPoints == null || targetPoints.Length == 0)
            return res;

        Vector2d min = targetPoints[0], max = targetPoints[0];

        for(int i = 0; i < targetPoints.Length; i++)
        {
            Vector2d p = targetPoints[i];

            if (p.x < min.x)
                min.x = p.x;
            if (p.y < min.y)
                min.y = p.y;

            if (p.x > max.x)
                max.x = p.x;
            if (p.y > max.y)
                max.y = p.y;

        }

        double dist = Utils.LngLatDistance(min, max);

        //print(dist);

        const double baseL = 2.2;
        const double height = 12.65;

        //y=13.706696−1.442695⋅lnx

        //res = -Mathd.Log(dist, baseL) + height;

        res = 13.706696 - 1.442695 * Math.Log(dist);

        res = Mathd.Clamp(res, minZoom, maxZoom);

        //print(res);

        return res;

    }

    public Vector2d getCenterOfPoints(Vector2d[] points)
    {
        Vector3 sum = Vector3.zero;

        for(int i = 0;i < points.Length; i++)
        {
            sum += Map.GeoToWorldPosition(points[i]);
        }

        sum /= points.Length;

        return Map.WorldToGeoPosition(sum);
    }

    /*public Vector2d[] testPts;
    private void OnDrawGizmos()
    {
        if(testPts != null && testPts.Length > 0)
        {

            Gizmos.color = Color.red;

            double zoom = CalculateMapZoomToFit(testPts);
            print(zoom);

            Map.SetZoom((float)zoom);
            Map.UpdateMap();

            for(int i = 0; i < testPts.Length; i++)
            {
                Gizmos.DrawSphere(Map.GeoToWorldPosition(testPts[i]), 0.2f);
            }
        }
    }*/

}
