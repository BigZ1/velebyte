﻿using NativeFilePickerNamespace;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.UI;

public class Login : MonoBehaviour
{

    public InputField username;

    public InputField password;

    public void ApplyLogin()
    {

        PrijavaDto login = new PrijavaDto();

        if(string.IsNullOrEmpty(username.text) || string.IsNullOrWhiteSpace(username.text))
        {
            UIController.ShowGameMessage("Username cannot be empty", "Game message");
            return;
        }

        login.korisnickoIme = username.text;

        if (string.IsNullOrEmpty(password.text) || string.IsNullOrWhiteSpace(password.text))
        {
            UIController.ShowGameMessage("You forgot to type in your password", "Game message");
            return;
        }

        string protectedPassword = PasswordProtect.protectPassword(password.text);

        login.sazetakLozinke = protectedPassword;

        //Client.main.WriteLine("LOG " + userB64 + " " + protectedPassword + " " + Client.DeviceID);

        login.IDUredaja = Client.DeviceID;

        //TODO: add login body
        //print(login.GetJSON());
        Client.main.Login(login.GetJSON());

        User.main.ClearUI();

        //User.main.username = username.text;
        User.main.usernameText.text = username.text;

        //Client.main.ReadLine(Protocol.main.ProcessMessage);

    }

    public void Register()
    {
        UIController.main.UIState.State = UIController.UIHolderType.Register;
    }
    
}
