﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;

public class PreBattlePanel : MonoBehaviour
{

    public GameObject holder;

    public Text OpponoentNameText;

    public Text BattleTypeText;

    public Text BattlegroundNameText;

    public GameObject PlayersCardsHolder;

    public GameObject PlayersPickedCardsHolder;

    public HashSet<CardUI> playersCards = new HashSet<CardUI>();
    public HashSet<CardUI> playersPickedCards = new HashSet<CardUI>();

    public int battleType;

    public void Show(BorbaPrepInfoDto info)
    {

        if (info == null)
            return;

        SceneController.main.HideNearPlayers();

        OpponoentNameText.text = info.protivnik;
        BattleTypeText.text = getBattleType(info.tipBorbe);
        BattlegroundNameText.text = info.imeBorilista;

        //TODO: load all player cards
        //...

        IzbrisiKorisnikaDto req = new IzbrisiKorisnikaDto();
        req.korisnickoIme = User.main.username;

        battleType = info.tipBorbe;

        Client.main.GetPlayerCards(req.GetJSON(), AfterGetPlayerCards);

        void AfterGetPlayerCards(string data)
        {

            print("Request Cards Result: " + data);

            PlayerCardsDto cards = null;

            try
            {
                cards = JsonUtility.FromJson<PlayerCardsDto>(data);
            }
            catch (Exception e)
            {
                UIController.ShowGameMessage(e.Message);
            }

            if (cards != null)
            {
                if (cards.idKarata != null)
                {
                    for (int i = 0; i < cards.idKarata.Length; i++)
                    {
                        Client.main.RequestCardInfo((int)cards.idKarata[i], AfterCardInfoGot);
                    }
                }
            }
            else
            {
                UIController.ShowGameMessage("Card receive error");
            }

            void AfterCardInfoGot(string cardData)
            {

                print("Got card data: " + cardData);

                KartaInfoDto ki = null;
                try
                {
                    ki = JsonUtility.FromJson<KartaInfoDto>(cardData);
                }
                catch (Exception e)
                {
                    UIController.ShowGameMessage(e.Message);
                    return;
                }
                if (ki != null)
                {
                    Texture2D tex = null;
                    try
                    {
                        tex = Utils.Base64ToTexture(ki.slika);
                    }
                    catch (Exception e)
                    {
                        UIController.ShowGameMessage(e.Message, "Err ld img");
                    }
                    GameObject card = CardCollectionPanel.AddNewCard((int)ki.raritet, ki.imeKarte, tex, ki.opis, PlayersCardsHolder.transform);
                    CardUI cc = card.GetComponent<CardUI>();
                    cc.id = ki.id;
                    cc.Selectable = true;
                    cc.lng = ki.longitude;
                    cc.lat = ki.latitude;

                    playersCards.Add(cc);
                    if (cc.OnSelect == null)
                        cc.OnSelect = new CardUI.OnSelectEvent();
                    cc.OnSelect.AddListener(SelectCard);
                }

            }

        }

        holder.SetActive(true);

        string getBattleType (int val) {
            switch(val)
            {
                case 0: return "Town";
                case 1: return "Country";
                case 2: return "Continent";
                default: return "World";
            }
        }

    }

    public void ToBattle()
    {

        if(playersPickedCards.Count < 3)
        {
            UIController.ShowGameMessage("You need to pick at least 3 cards", "Game Message", true);
            return;
        }

        int maxCount = 8;

        switch(battleType)
        {
            case 1:
                maxCount = 12;
                break;
            case 2:
                maxCount = 20;
                break;
            case 3:
                maxCount = 32;
                break;
        }

        if(playersPickedCards.Count > maxCount)
        {
            UIController.ShowGameMessage("You picked more cards that allowed (" + maxCount + ")", "Game Message", true);
            return;
        }

        PocetakBorbeDto p = new PocetakBorbeDto();

        p.korisnickoIme = User.main.username;

        long[] cards = new long[playersPickedCards.Count];
        int i = 0;

        foreach(CardUI cu in playersPickedCards)
        {
            cards[i++] = cu.id;
        }

        p.OdabraneKarte = cards;

        Battle.main.TakeCards(playersPickedCards.ToArray());

        Client.main.StartBattle(p.GetJSON());

    }

    public void CancelBattle()
    {

        KorisniciUBorbiDto data = new KorisniciUBorbiDto();
        data.prviKorisnik = User.main.username;
        data.drugiKorisnik = OpponoentNameText.text;
        Client.main.RejectBattle(data.GetJSON());

        holder.SetActive(false);
        OpponoentNameText.text = "";
        BattleTypeText.text = "";
        BattlegroundNameText.text = "";
        Utils.ClearChildren(PlayersCardsHolder.transform);
        Utils.ClearChildren(PlayersPickedCardsHolder.transform);
        //TODO: tell server that the battle was rejected
        
    }

    public void Close()
    {
        holder.SetActive(false);
        OpponoentNameText.text = "";
        BattleTypeText.text = "";
        BattlegroundNameText.text = "";
        Utils.ClearChildren(PlayersCardsHolder.transform);
        Utils.ClearChildren(PlayersPickedCardsHolder.transform);
    }

    public void SelectCard(CardUI card)
    {
        //print("Card selected");
        if(playersCards.Contains(card))
        {
            playersCards.Remove(card);

            playersPickedCards.Add(card);
            card.transform.SetParent(PlayersPickedCardsHolder.transform);
        }
        else if(playersPickedCards.Contains(card))
        {
            playersPickedCards.Remove(card);

            playersCards.Add(card);
            card.transform.SetParent(PlayersCardsHolder.transform);
        }
    }

}
