﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DistTest : MonoBehaviour
{

    public GameObject g1, g2;

    public float zoomDelta = 0.1f;

    IEnumerator getDists()
    {
        string outp = "";
        while (SceneController.Map.Zoom >= 0 && zoomDelta > 0f)
        {

            double dist = Utils.LngLatDistance(SceneController.Map.WorldToGeoPosition(g1.transform.position),
                                                SceneController.Map.WorldToGeoPosition(g2.transform.position));

            outp += dist + "\t" + SceneController.Map.Zoom + "\n";
            //print(dist + "\t" + SceneController.Map.Zoom + "\n");

            SceneController.Map.SetZoom(SceneController.Map.Zoom - zoomDelta);
            if (SceneController.Map.Zoom < 0)
            {
                SceneController.Map.SetZoom(0);
                break;
            }
            SceneController.Map.UpdateMap();
            yield return new WaitForSeconds(0.1f);
        }
        print(outp);
        yield break;
    }

    private void Update()
    {

        if(Input.GetKeyDown(KeyCode.C))
        {
            StartCoroutine(getDists());
        }
        if(Input.GetKeyDown(KeyCode.D))
        {
            double dist = Utils.LngLatDistance(SceneController.Map.WorldToGeoPosition(g1.transform.position),
                                                SceneController.Map.WorldToGeoPosition(g2.transform.position));
            print(dist);
        }
    }

}
