﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerChallengeItem : MonoBehaviour
{

    public Text playerNameText;

    public Dropdown playerOptionsDropdown;

    public string playerName;

    public void selectOption(int value)
    {

        if(value == 1)
        {
            //accept
            OdgovorNaIzazovIgracaDto o = new OdgovorNaIzazovIgracaDto();
            o.ci = User.main.username;
            o.iz = playerNameText.text;
            o.prihvacen = true;
            Client.main.AnswerChallenge(o.GetJSON());

            UIController.main.challengesPanel.Close();

        }

        if(value == 2)
        {
            //reject
            OdgovorNaIzazovIgracaDto o = new OdgovorNaIzazovIgracaDto();
            o.ci = User.main.username;
            o.iz = playerNameText.text;
            o.prihvacen = false;
            Client.main.AnswerChallenge(o.GetJSON());

            UIController.main.challengesPanel.Close();
        }

        playerOptionsDropdown.SetValueWithoutNotify(0);
    }

}
