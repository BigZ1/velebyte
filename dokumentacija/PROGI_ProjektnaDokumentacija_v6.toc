\babel@toc {croatian}{}
\contentsline {chapter}{\numberline {1}Dnevnik promjena dokumentacije}{3}{chapter.1}%
\contentsline {chapter}{\numberline {2}Opis projektnog zadatka}{4}{chapter.2}%
\contentsline {chapter}{\numberline {3}Specifikacija programske potpore}{8}{chapter.3}%
\contentsline {section}{\numberline {3.1}Funkcionalni zahtjevi}{8}{section.3.1}%
\contentsline {subsection}{\numberline {3.1.1}Obrasci uporabe}{10}{subsection.3.1.1}%
\contentsline {subsubsection}{Opis obrazaca uporabe}{10}{subsubsection*.2}%
\contentsline {subsubsection}{Dijagrami obrazaca uporabe}{22}{subsubsection*.3}%
\contentsline {subsection}{\numberline {3.1.2}Sekvencijski dijagrami}{25}{subsection.3.1.2}%
\contentsline {section}{\numberline {3.2}Ostali zahtjevi}{27}{section.3.2}%
\contentsline {chapter}{\numberline {4}Arhitektura i dizajn sustava}{28}{chapter.4}%
\contentsline {section}{\numberline {4.1}Baza podataka}{29}{section.4.1}%
\contentsline {subsection}{\numberline {4.1.1}Opis tablica}{29}{subsection.4.1.1}%
\contentsline {subsection}{\numberline {4.1.2}Dijagram baze podataka}{33}{subsection.4.1.2}%
\contentsline {section}{\numberline {4.2}Dijagram razreda}{34}{section.4.2}%
\contentsline {section}{\numberline {4.3}Dijagram stanja}{39}{section.4.3}%
\contentsline {section}{\numberline {4.4}Dijagram aktivnosti}{41}{section.4.4}%
\contentsline {section}{\numberline {4.5}Dijagram komponenti}{42}{section.4.5}%
\contentsline {chapter}{\numberline {5}Implementacija i korisničko sučelje}{43}{chapter.5}%
\contentsline {section}{\numberline {5.1}Korištene tehnologije i alati}{43}{section.5.1}%
\contentsline {section}{\numberline {5.2}Ispitivanje programskog rješenja}{44}{section.5.2}%
\contentsline {subsection}{\numberline {5.2.1}Ispitivanje komponenti}{44}{subsection.5.2.1}%
\contentsline {subsection}{\numberline {5.2.2}Ispitivanje sustava}{48}{subsection.5.2.2}%
\contentsline {section}{\numberline {5.3}Dijagram razmještaja}{61}{section.5.3}%
\contentsline {section}{\numberline {5.4}Upute za puštanje u pogon}{62}{section.5.4}%
\contentsline {chapter}{\numberline {6}Zaključak i budući rad}{64}{chapter.6}%
\contentsline {chapter}{Indeks slika i dijagrama}{67}{chapter*.4}%
\contentsline {chapter}{Dodatak: Prikaz aktivnosti grupe}{68}{chapter*.5}%
\contentsline {chapter}{Popis literature}{73}{chapter*.6}%
