PGDMP     *    7                 y         
   GeoFighter    12.2    12.2 2    `           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            a           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            b           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            c           1262    49786 
   GeoFighter    DATABASE     �   CREATE DATABASE "GeoFighter" WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'Croatian_Croatia.1250' LC_CTYPE = 'Croatian_Croatia.1250';
    DROP DATABASE "GeoFighter";
                postgres    false            �            1259    50022    borba    TABLE     �   CREATE TABLE public.borba (
    pobjednik bigint NOT NULL,
    idborbe bigint NOT NULL,
    dobivenielo integer NOT NULL,
    prvikorisnik bigint NOT NULL,
    drugikorisnik bigint NOT NULL
);
    DROP TABLE public.borba;
       public         heap    postgres    false            �            1259    49963    drzava    TABLE     g   CREATE TABLE public.drzava (
    iddrzave bigint NOT NULL,
    imedrzave character varying NOT NULL
);
    DROP TABLE public.drzava;
       public         heap    postgres    false            �            1259    49971    grad    TABLE     c   CREATE TABLE public.grad (
    idgrada bigint NOT NULL,
    imegrada character varying NOT NULL
);
    DROP TABLE public.grad;
       public         heap    postgres    false            �            1259    50037    karta    TABLE     �   CREATE TABLE public.karta (
    imekarte character varying NOT NULL,
    idkarte bigint NOT NULL,
    idrariteta bigint NOT NULL
);
    DROP TABLE public.karta;
       public         heap    postgres    false            �            1259    49955 	   kontinent    TABLE     r   CREATE TABLE public.kontinent (
    idkontinenta bigint NOT NULL,
    imekontinenta character varying NOT NULL
);
    DROP TABLE public.kontinent;
       public         heap    postgres    false            �            1259    50005    korisnik    TABLE     �  CREATE TABLE public.korisnik (
    idkorisnika bigint NOT NULL,
    korisnickoime text NOT NULL,
    urlfotografije character varying NOT NULL,
    email text NOT NULL,
    elo integer NOT NULL,
    password character varying NOT NULL,
    verified boolean NOT NULL,
    banned boolean NOT NULL,
    exparation timestamp without time zone,
    iban text,
    slikaosobne character varying,
    iduloge bigint NOT NULL
);
    DROP TABLE public.korisnik;
       public         heap    postgres    false            �            1259    50065    lokacija    TABLE     �  CREATE TABLE public.lokacija (
    opislokacije character varying NOT NULL,
    brojposjeta integer NOT NULL,
    urlfotografije character varying NOT NULL,
    idkarte bigint NOT NULL,
    idkontinenta bigint NOT NULL,
    iddrzave bigint NOT NULL,
    idgrada bigint,
    idtipa bigint NOT NULL,
    idstatusa bigint NOT NULL,
    geocordlng double precision,
    geocordlat double precision,
    radijus double precision,
    idlokacije bigint
);
    DROP TABLE public.lokacija;
       public         heap    postgres    false            �            1259    49987    raritet    TABLE     �   CREATE TABLE public.raritet (
    idrariteta bigint NOT NULL,
    nazivrariteta character varying NOT NULL,
    multiplier double precision NOT NULL
);
    DROP TABLE public.raritet;
       public         heap    postgres    false            �            1259    49997    status    TABLE     i   CREATE TABLE public.status (
    idstatusa bigint NOT NULL,
    imestatusa character varying NOT NULL
);
    DROP TABLE public.status;
       public         heap    postgres    false            �            1259    49979    tiplokacije    TABLE     h   CREATE TABLE public.tiplokacije (
    idtipa bigint NOT NULL,
    imetipa character varying NOT NULL
);
    DROP TABLE public.tiplokacije;
       public         heap    postgres    false            �            1259    49947    uloga    TABLE     d   CREATE TABLE public.uloga (
    iduloge bigint NOT NULL,
    imeuloge character varying NOT NULL
);
    DROP TABLE public.uloga;
       public         heap    postgres    false            [          0    50022    borba 
   TABLE DATA           ]   COPY public.borba (pobjednik, idborbe, dobivenielo, prvikorisnik, drugikorisnik) FROM stdin;
    public          postgres    false    210   �;       U          0    49963    drzava 
   TABLE DATA           5   COPY public.drzava (iddrzave, imedrzave) FROM stdin;
    public          postgres    false    204   �;       V          0    49971    grad 
   TABLE DATA           1   COPY public.grad (idgrada, imegrada) FROM stdin;
    public          postgres    false    205   D       \          0    50037    karta 
   TABLE DATA           >   COPY public.karta (imekarte, idkarte, idrariteta) FROM stdin;
    public          postgres    false    211   �D       T          0    49955 	   kontinent 
   TABLE DATA           @   COPY public.kontinent (idkontinenta, imekontinenta) FROM stdin;
    public          postgres    false    203   QF       Z          0    50005    korisnik 
   TABLE DATA           �   COPY public.korisnik (idkorisnika, korisnickoime, urlfotografije, email, elo, password, verified, banned, exparation, iban, slikaosobne, iduloge) FROM stdin;
    public          postgres    false    209   �F       ]          0    50065    lokacija 
   TABLE DATA           �   COPY public.lokacija (opislokacije, brojposjeta, urlfotografije, idkarte, idkontinenta, iddrzave, idgrada, idtipa, idstatusa, geocordlng, geocordlat, radijus, idlokacije) FROM stdin;
    public          postgres    false    212   �G       X          0    49987    raritet 
   TABLE DATA           H   COPY public.raritet (idrariteta, nazivrariteta, multiplier) FROM stdin;
    public          postgres    false    207   �K       Y          0    49997    status 
   TABLE DATA           7   COPY public.status (idstatusa, imestatusa) FROM stdin;
    public          postgres    false    208   �K       W          0    49979    tiplokacije 
   TABLE DATA           6   COPY public.tiplokacije (idtipa, imetipa) FROM stdin;
    public          postgres    false    206   L       S          0    49947    uloga 
   TABLE DATA           2   COPY public.uloga (iduloge, imeuloge) FROM stdin;
    public          postgres    false    202   �L       �
           2606    50026    borba borba_pkey 
   CONSTRAINT     S   ALTER TABLE ONLY public.borba
    ADD CONSTRAINT borba_pkey PRIMARY KEY (idborbe);
 :   ALTER TABLE ONLY public.borba DROP CONSTRAINT borba_pkey;
       public            postgres    false    210            �
           2606    49970    drzava drzava_pkey 
   CONSTRAINT     V   ALTER TABLE ONLY public.drzava
    ADD CONSTRAINT drzava_pkey PRIMARY KEY (iddrzave);
 <   ALTER TABLE ONLY public.drzava DROP CONSTRAINT drzava_pkey;
       public            postgres    false    204            �
           2606    49978    grad grad_pkey 
   CONSTRAINT     Q   ALTER TABLE ONLY public.grad
    ADD CONSTRAINT grad_pkey PRIMARY KEY (idgrada);
 8   ALTER TABLE ONLY public.grad DROP CONSTRAINT grad_pkey;
       public            postgres    false    205            �
           2606    50044    karta karta_pkey 
   CONSTRAINT     S   ALTER TABLE ONLY public.karta
    ADD CONSTRAINT karta_pkey PRIMARY KEY (idkarte);
 :   ALTER TABLE ONLY public.karta DROP CONSTRAINT karta_pkey;
       public            postgres    false    211            �
           2606    49962    kontinent kontinent_pkey 
   CONSTRAINT     `   ALTER TABLE ONLY public.kontinent
    ADD CONSTRAINT kontinent_pkey PRIMARY KEY (idkontinenta);
 B   ALTER TABLE ONLY public.kontinent DROP CONSTRAINT kontinent_pkey;
       public            postgres    false    203            �
           2606    66490    korisnik korisnik_email_key 
   CONSTRAINT     W   ALTER TABLE ONLY public.korisnik
    ADD CONSTRAINT korisnik_email_key UNIQUE (email);
 E   ALTER TABLE ONLY public.korisnik DROP CONSTRAINT korisnik_email_key;
       public            postgres    false    209            �
           2606    66488 #   korisnik korisnik_korisnickoime_key 
   CONSTRAINT     g   ALTER TABLE ONLY public.korisnik
    ADD CONSTRAINT korisnik_korisnickoime_key UNIQUE (korisnickoime);
 M   ALTER TABLE ONLY public.korisnik DROP CONSTRAINT korisnik_korisnickoime_key;
       public            postgres    false    209            �
           2606    50012    korisnik korisnik_pkey 
   CONSTRAINT     ]   ALTER TABLE ONLY public.korisnik
    ADD CONSTRAINT korisnik_pkey PRIMARY KEY (idkorisnika);
 @   ALTER TABLE ONLY public.korisnik DROP CONSTRAINT korisnik_pkey;
       public            postgres    false    209            �
           2606    50072    lokacija lokacija_pkey 
   CONSTRAINT     Y   ALTER TABLE ONLY public.lokacija
    ADD CONSTRAINT lokacija_pkey PRIMARY KEY (idkarte);
 @   ALTER TABLE ONLY public.lokacija DROP CONSTRAINT lokacija_pkey;
       public            postgres    false    212            �
           2606    49996 !   raritet raritet_nazivrariteta_key 
   CONSTRAINT     e   ALTER TABLE ONLY public.raritet
    ADD CONSTRAINT raritet_nazivrariteta_key UNIQUE (nazivrariteta);
 K   ALTER TABLE ONLY public.raritet DROP CONSTRAINT raritet_nazivrariteta_key;
       public            postgres    false    207            �
           2606    49994    raritet raritet_pkey 
   CONSTRAINT     Z   ALTER TABLE ONLY public.raritet
    ADD CONSTRAINT raritet_pkey PRIMARY KEY (idrariteta);
 >   ALTER TABLE ONLY public.raritet DROP CONSTRAINT raritet_pkey;
       public            postgres    false    207            �
           2606    50004    status status_pkey 
   CONSTRAINT     W   ALTER TABLE ONLY public.status
    ADD CONSTRAINT status_pkey PRIMARY KEY (idstatusa);
 <   ALTER TABLE ONLY public.status DROP CONSTRAINT status_pkey;
       public            postgres    false    208            �
           2606    49986    tiplokacije tip_pkey 
   CONSTRAINT     V   ALTER TABLE ONLY public.tiplokacije
    ADD CONSTRAINT tip_pkey PRIMARY KEY (idtipa);
 >   ALTER TABLE ONLY public.tiplokacije DROP CONSTRAINT tip_pkey;
       public            postgres    false    206            �
           2606    49954    uloga uloga_pkey 
   CONSTRAINT     S   ALTER TABLE ONLY public.uloga
    ADD CONSTRAINT uloga_pkey PRIMARY KEY (iduloge);
 :   ALTER TABLE ONLY public.uloga DROP CONSTRAINT uloga_pkey;
       public            postgres    false    202            �
           2606    50032    borba borba_drugikorisnik_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.borba
    ADD CONSTRAINT borba_drugikorisnik_fkey FOREIGN KEY (drugikorisnik) REFERENCES public.korisnik(idkorisnika);
 H   ALTER TABLE ONLY public.borba DROP CONSTRAINT borba_drugikorisnik_fkey;
       public          postgres    false    2756    210    209            �
           2606    50027    borba borba_prvikorisnik_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.borba
    ADD CONSTRAINT borba_prvikorisnik_fkey FOREIGN KEY (prvikorisnik) REFERENCES public.korisnik(idkorisnika);
 G   ALTER TABLE ONLY public.borba DROP CONSTRAINT borba_prvikorisnik_fkey;
       public          postgres    false    2756    209    210            �
           2606    50045    karta karta_idrariteta_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.karta
    ADD CONSTRAINT karta_idrariteta_fkey FOREIGN KEY (idrariteta) REFERENCES public.raritet(idrariteta);
 E   ALTER TABLE ONLY public.karta DROP CONSTRAINT karta_idrariteta_fkey;
       public          postgres    false    2748    211    207            �
           2606    50017    korisnik korisnik_iduloge_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.korisnik
    ADD CONSTRAINT korisnik_iduloge_fkey FOREIGN KEY (iduloge) REFERENCES public.uloga(iduloge);
 H   ALTER TABLE ONLY public.korisnik DROP CONSTRAINT korisnik_iduloge_fkey;
       public          postgres    false    202    209    2736            �
           2606    50083    lokacija lokacija_iddrzave_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.lokacija
    ADD CONSTRAINT lokacija_iddrzave_fkey FOREIGN KEY (iddrzave) REFERENCES public.drzava(iddrzave);
 I   ALTER TABLE ONLY public.lokacija DROP CONSTRAINT lokacija_iddrzave_fkey;
       public          postgres    false    212    2740    204            �
           2606    50088    lokacija lokacija_idgrada_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.lokacija
    ADD CONSTRAINT lokacija_idgrada_fkey FOREIGN KEY (idgrada) REFERENCES public.grad(idgrada);
 H   ALTER TABLE ONLY public.lokacija DROP CONSTRAINT lokacija_idgrada_fkey;
       public          postgres    false    205    212    2742            �
           2606    50073    lokacija lokacija_idkarte_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.lokacija
    ADD CONSTRAINT lokacija_idkarte_fkey FOREIGN KEY (idkarte) REFERENCES public.karta(idkarte);
 H   ALTER TABLE ONLY public.lokacija DROP CONSTRAINT lokacija_idkarte_fkey;
       public          postgres    false    2760    211    212            �
           2606    50078 #   lokacija lokacija_idkontinenta_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.lokacija
    ADD CONSTRAINT lokacija_idkontinenta_fkey FOREIGN KEY (idkontinenta) REFERENCES public.kontinent(idkontinenta);
 M   ALTER TABLE ONLY public.lokacija DROP CONSTRAINT lokacija_idkontinenta_fkey;
       public          postgres    false    212    2738    203            �
           2606    50098     lokacija lokacija_idstatusa_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.lokacija
    ADD CONSTRAINT lokacija_idstatusa_fkey FOREIGN KEY (idstatusa) REFERENCES public.status(idstatusa);
 J   ALTER TABLE ONLY public.lokacija DROP CONSTRAINT lokacija_idstatusa_fkey;
       public          postgres    false    2750    212    208            �
           2606    50093    lokacija lokacija_idtipa_fkey    FK CONSTRAINT     �   ALTER TABLE ONLY public.lokacija
    ADD CONSTRAINT lokacija_idtipa_fkey FOREIGN KEY (idtipa) REFERENCES public.tiplokacije(idtipa);
 G   ALTER TABLE ONLY public.lokacija DROP CONSTRAINT lokacija_idtipa_fkey;
       public          postgres    false    2744    206    212            [      x������ � �      U   :  x�m��r����Sp9�8)���\z|_F�d��T6���0&	H�GZ�eR��	Ry�|MJ�����q!�G���V�N~�7�ruc*����T�躖�2^�h���4V'%`i�hfJo�D�T+��)h��3�+�9�1a����2r��Db�	�v�t�N�ڲR1�D(���� �m�S����ކ�q?a��٘��:��́պ0+[ot2Pbլ|��X}��	-0�vm���@��:�*�HƀP
�d��l��WX�{�F�5_�k��|w�+�v�_�^C,��~5�����<���f�
=�|p��7�u�rx����wn�>��Z����΄{m
S�!�bm�+C8���х���2n9M3ujʅ_ɶ\��Wz4 W��b��FO6��%�v%��!1�S^���N~�a�`���pK=�ӍY�ј���� .<J� �q�w��/}}��8�Z~75_�r!�5_������0=Nz�76:��_C��1�����X=����o�!���A�q���_�ll��~��|c�Y�BYbo�66��`f����^{�ܼ_�3[�&<�I��~��o�'Cu�KW������d�Η-1�d��׻m�'�:/Ȱ⥟����� �_������s�" Ӂ:�/Y����8���S<L�����(�g�Y޾9(����t���{�t�.���V�)�V\��iv���bW�Z,�ǹ�������:�K� ��=p��K��Z����F�r#F���t���D]k!���9f����ru�+m�ۭ��@F��c���҈$a��Xmw:�b��|t@�}q$U�s�;��Oԕ!'u��+k�ї]��-����]4�L]�hfm��IⒷ>u3>�<gC�"���@��?�bu�V���������S�H����:��&~R��LA�J6�u�r��������A|����<��_�V�"�_mň�WV��Xݘ�y�t�"F~ol��Su�[p[�����̜Z�-l�1C٘6~�[
�@ݴ�8��]X���Ad������O�gȞFd9F�o-��˧cp훍Oԭ[t�,F��rT�֑Ѝ���J�@�o]�iM_���â�m���;�Db�dW��6d�2kS/	��&��+~A�C���I��dK&����)��Ȋ�i&)��Wk
����	���е�Wx�|��FH�?��h�[�c ��w�X��� ��]�@jy�'�n����,Ú�6<�|�:/�3t���%�;~��	T���{S�EW��`*h���������8\ٽ��ѩ)��Eze�5}t#��Dm0��bFt(��G]0��އ�8��2�KԝN�囧�a_�+%���y�;��B�; � [�,B}��Tb�-���>�L��N{���N����R1�v�.�#�S�,i)��I��qFq�pn��2�v��5^��G[�lD%c�������r�(�/� Abh����++��m�Zi���S#�Y��c
[B���la�QC����4u'����I��Vn�eg4?���>JĖ�g:��ay�*�˪�P!���~��Q��/�<[�K~�04��Fc}f��p
�>C2��ϝ2�t��8�{�+�Ӆ%2?� ߢ�3�n����rc����]���n-YDG	{�����������rճ�y�i���
��Y�FD]]����3iy�{����3ޭ��!'��u1�&���-�,>X�:,���>��"�	�`��9�̂�nM�������=!��a�b�9����s���>��J�*1�P$f���,@��5�>��y'����]�J�ܸ��h��y�P��w��P�s�C1�;���uD.A���0A����Jr&��!�o'j�]�����~a�ݩ��'uw>4蜩z	�/P~��D������z��w�R�-��"��q�eRB)x|�!��<V������#��|:>.�E+/�@&ǩ�c�M�ϐ�ѷ�)v|��H�C۩KB	y�/�ѓ�'S�A���D��I1xR8�?FhC�[F�3fG�;{����X�N�r�g.ڦ��̉�]�%�����T��bK������K(.�]�0�W���'�� �y�      V   �   x���n�0Eg�+�A$;�5)�!C�
�Pt�m"`�J�dk�חC����7M��h��{lF��B��T���INUf����#t����::AG�ޣD<����hv�RPa�@5��es�iDc���$48)�Ѵ
=GQ܃�Ryf4p������K^�u��%�{�ϲ*�;��)K�kk�N9h~�f�c��z�6p%���[��8�w��"�FQ�      \   L  x�M��N�0��ݧ��%��]���&M�] n��+�m2�dh<=.0�M}��sNz�ÁnI��I��',����{s�ؐ�!b|�$��`�gj�ˈ��Q�[N��*����!�=�Y����fGZ*�?H�����u#�~��N�g*ة.]�2x���̴yA��>&K0Qrs}��3�9�p�>�� s%�c���6��	�D��=�t+.�wj59��ɷ�߷[u�`�UK׆�P܁u�^%4p�ѭ(&	M����,�oN�ׄU�K���\���{U5̹⇧g���{WK 3̽���ĒZ�#\�S��5V;�s�����'h�SE/gEQ|�֒K      T   O   x�3�t--�/H�2�t,�L�2�t,-.)J��M8ӊ2��L9��J2sS�|3���R$�9�c^IbQr	����� ��0      Z   4  x����k�P�?_�����ϧև����,3C�ᚥs�����{o�� ؇s��s��}C^V0L�J�r���pg���ً�﹩3�<$!�h֛�&�y�h:��x�Sm> �5,�`H�2QT��?m�@����5-�-�y:��3H{�QG�a����V�U|������i�Bd,���I f��m����'��_q}�7[.�	����.}$�,X���Lun�u8q��$��Ιΐ�?x��w,(�ԫ�<��L"���M��_��
��3�<�+t+#�����Wkӵie.��j'�����ʶ¥(�T3�\      ]   �  x�u�Ko"9���Wx9l�ߏ%�F�4�"j)r��1�*T�&�~n����B��O���s=?�˃��}�ѝ��s%���s^��/Ư�t������H�ZF��&zVi�;�t(��ݱF��da P�X�NF������P���7yqv~q�*������Y�TI�p�B�����>*{ �t8�ϼ���\�"O`O�e6w;"�qK)4c��M����9�>����7�K�q�4��F��je��@�L(��DC�V���r�{\��w�}H��l0o���h{%s"m$�6\���GY�
�9�x��1�0{������L�3�SM��G����jt��rx�~��%�Q�[�(4�H���J��aD�mv������6)����ä��PKN�h�M\}��ݱ|��	l�_��s7����qC��8ŵ
�P�&�O5M�H
J�\�BS���,�@)��n��Oݡ��J��u�:�?��ʾJ3�(�i0�J�heh�C����.N����3�xJS��XM�ر����w� ^���X�d�B��B�Y��
�~���x��!0��DBR�L$Ø6q��MO���(?��M��G��Un�bǣ������_��R�k�����o�/�E�K6
q�7����Q
~MGoH����=-����x
{��Z!ZI�2 �V&d����\��k�E��)���LΦ�$�m�`�ƠX	�ov�CY�|��4�I=yCٚl3�d�Sk����yX���s�q{��p+9�?�?��~~�F'�k�Јvq�`H#����×�䋽��ܧ�"�=tc�4�窉�*b�6^��f����\,��)�*4J���8Z��g�U6��eSn]�/G��p�`���f��E��~�Q��0� &~?�5�	��?y]d�r*tu&ooCa�"b��!��ǰ�Y      X   8   x�3�L������4�2�,J,J�4�2�L-�L�4�2��IMO�KI,��4����� 9�<      Y   &   x�3��O�O*J���2��KU��9K�RK�b���� �R      W   �   x�=O�j1;k�b %v��&���%t����b2;^�%_�О$$!!�W�
yGe�h��X��(�����[,�9-p��Ŀ��G��Dk�´���A,�������1��|r��d��v2J�l��b�Vw�w�!ks�t%������jd�����5Q�����c����)t}��c���m��� hG�      S   2   x�3��I�L-�2�tN,*�O/JL�8Sr3�2�K�K�b���� ;q�     