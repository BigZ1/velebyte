package hr.fer.Geofighter.tester;

import hr.fer.Geofighter.service.IzracunavanjaService;
import org.assertj.core.util.Arrays;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class IzracunavanjeServiceTest {
    @Test
    public void testGetPovrsinaWithNull(){
        IzracunavanjaService i = new IzracunavanjaService();
        double expected = 0;
        double actual = i.getPovrsina(null);

        assertEquals(expected, actual, 1E-8);
    }

    @Test
    public void testGetPovrsinaWithTrokut(){
        IzracunavanjaService i = new IzracunavanjaService();
        HashSet<IzracunavanjaService.Vector2D> koordinate = new HashSet<>();
        koordinate.add(new IzracunavanjaService.Vector2D(0,0));
        koordinate.add(new IzracunavanjaService.Vector2D(0,1));
        koordinate.add(new IzracunavanjaService.Vector2D(1,0));
        double expected = 0.5;
        double actual = i.getPovrsina(koordinate);

        assertEquals(expected, actual, 1E-8);
    }

    @Test
    public void testGetPovrsinaWithTrokut2(){
        IzracunavanjaService i = new IzracunavanjaService();
        HashSet<IzracunavanjaService.Vector2D> koordinate = new HashSet<>();
        koordinate.add(new IzracunavanjaService.Vector2D(0,0));
        koordinate.add(new IzracunavanjaService.Vector2D(0,1));
        koordinate.add(new IzracunavanjaService.Vector2D(1,0));
        koordinate.add(new IzracunavanjaService.Vector2D(0.5,0.1));
        double expected = 0.5;
        double actual = i.getPovrsina(koordinate);

        assertEquals(expected, actual, 1E-8);
    }

    @Test
    public void testGetPovrsinaWithTrokut3(){
        IzracunavanjaService i = new IzracunavanjaService();
        HashSet<IzracunavanjaService.Vector2D> koordinate = new HashSet<>();
        koordinate.add(new IzracunavanjaService.Vector2D(0,0));
        koordinate.add(new IzracunavanjaService.Vector2D(0,1));
        koordinate.add(new IzracunavanjaService.Vector2D(1,0));
        koordinate.add(new IzracunavanjaService.Vector2D(0.5,0));
        double expected = 0.5;
        double actual = i.getPovrsina(koordinate);

        assertEquals(expected, actual, 1E-8);
    }

    @Test
    public void testGetPovrsinaWithTrokut4(){
        IzracunavanjaService i = new IzracunavanjaService();
        HashSet<IzracunavanjaService.Vector2D> koordinate = new HashSet<>();
        koordinate.add(new IzracunavanjaService.Vector2D(0,0));
        koordinate.add(new IzracunavanjaService.Vector2D(0,1));
        koordinate.add(new IzracunavanjaService.Vector2D(1,0));
        koordinate.add(new IzracunavanjaService.Vector2D(0.5,0.1));
        koordinate.add(new IzracunavanjaService.Vector2D(0.2,0.2));
        double expected = 0.5;
        double actual = i.getPovrsina(koordinate);

        assertEquals(expected, actual, 1E-8);
    }

    @Test
    public void testGetPovrsinaWithCetverokut(){
        IzracunavanjaService i = new IzracunavanjaService();
        HashSet<IzracunavanjaService.Vector2D> koordinate = new HashSet<>();
        koordinate.add(new IzracunavanjaService.Vector2D(0,0));
        koordinate.add(new IzracunavanjaService.Vector2D(0,1));
        koordinate.add(new IzracunavanjaService.Vector2D(1,0));
        koordinate.add(new IzracunavanjaService.Vector2D(1,1));
        koordinate.add(new IzracunavanjaService.Vector2D(0.2,0.2));
        double expected = 1;
        double actual = i.getPovrsina(koordinate);

        assertEquals(expected, actual, 1E-8);
    }

    @Test
    public void testGetPovrsinaWithCetverokut2(){
        IzracunavanjaService i = new IzracunavanjaService();
        HashSet<IzracunavanjaService.Vector2D> koordinate = new HashSet<>();
        koordinate.add(new IzracunavanjaService.Vector2D(0,0));
        koordinate.add(new IzracunavanjaService.Vector2D(0,1));
        koordinate.add(new IzracunavanjaService.Vector2D(1,0));
        koordinate.add(new IzracunavanjaService.Vector2D(1,1));
        koordinate.add(new IzracunavanjaService.Vector2D(0.2,0.2));
        koordinate.add(new IzracunavanjaService.Vector2D(0.2,0.3));
        koordinate.add(new IzracunavanjaService.Vector2D(0.1,0.2));
        double expected = 1;
        double actual = i.getPovrsina(koordinate);

        assertEquals(expected, actual, 1E-8);
    }

    @Test
    public void testGetPovrsinaWithCetverokut3(){
        IzracunavanjaService i = new IzracunavanjaService();
        HashSet<IzracunavanjaService.Vector2D> koordinate = new HashSet<>();
        koordinate.add(new IzracunavanjaService.Vector2D(0,0));
        koordinate.add(new IzracunavanjaService.Vector2D(0,1));
        koordinate.add(new IzracunavanjaService.Vector2D(1,0));
        koordinate.add(new IzracunavanjaService.Vector2D(1,2));
        koordinate.add(new IzracunavanjaService.Vector2D(0.2,0.2));
        koordinate.add(new IzracunavanjaService.Vector2D(0.2,0.3));
        koordinate.add(new IzracunavanjaService.Vector2D(0.1,0.2));
        double expected = 1.5;
        double actual = i.getPovrsina(koordinate);

        assertEquals(expected, actual, 1E-8);
    }

    @Test
    public void testGetPovrsinaWithSesterokut(){
        IzracunavanjaService i = new IzracunavanjaService();
        HashSet<IzracunavanjaService.Vector2D> koordinate = new HashSet<>();
        koordinate.add(new IzracunavanjaService.Vector2D(2,0));
        koordinate.add(new IzracunavanjaService.Vector2D(4,0));
        koordinate.add(new IzracunavanjaService.Vector2D(0,2));
        koordinate.add(new IzracunavanjaService.Vector2D(6,2));
        koordinate.add(new IzracunavanjaService.Vector2D(2,4));
        koordinate.add(new IzracunavanjaService.Vector2D(4,4));
        koordinate.add(new IzracunavanjaService.Vector2D(3,2));
        double expected = 16;
        double actual = i.getPovrsina(koordinate);

        assertEquals(expected, actual, 1E-8);
    }

    @Test
    public void testGetPovrsinaWithSesterokut2(){
        IzracunavanjaService i = new IzracunavanjaService();
        HashSet<IzracunavanjaService.Vector2D> koordinate = new HashSet<>();
        koordinate.add(new IzracunavanjaService.Vector2D(-5,-3));
        koordinate.add(new IzracunavanjaService.Vector2D(-2,-2));
        koordinate.add(new IzracunavanjaService.Vector2D(-3,2));
        koordinate.add(new IzracunavanjaService.Vector2D(-7,3));
        koordinate.add(new IzracunavanjaService.Vector2D(-8,1));
        koordinate.add(new IzracunavanjaService.Vector2D(-7,-2));
        double expected = 25;
        double actual = i.getPovrsina(koordinate);

        assertEquals(expected, actual, 1E-8);
    }

    @Test
    public void testGetPovrsinaWithDvadeseterokut(){
        IzracunavanjaService i = new IzracunavanjaService();
        HashSet<IzracunavanjaService.Vector2D> koordinate = new HashSet<>();
        koordinate.add(new IzracunavanjaService.Vector2D(-8,2));
        koordinate.add(new IzracunavanjaService.Vector2D(-8,-2));
        koordinate.add(new IzracunavanjaService.Vector2D(-6,-4));
        koordinate.add(new IzracunavanjaService.Vector2D(-2,-6));
        koordinate.add(new IzracunavanjaService.Vector2D(2,-4));
        koordinate.add(new IzracunavanjaService.Vector2D(-4,2));

        double expected = 48;
        double actual = i.getPovrsina(koordinate);

        assertEquals(expected, actual, 1E-8);
    }

    @Test
    public void testPretvorbaKoordUMetre(){
        IzracunavanjaService i = new IzracunavanjaService();
        List<Double> lista = new ArrayList<>();
        lista.add(5.0);
        lista.add(-8.0);
        HashSet<IzracunavanjaService.Vector2D> expected = new HashSet<>();
        expected.add(new IzracunavanjaService.Vector2D(720441.70,9115166.53));
        HashSet<IzracunavanjaService.Vector2D> actual = i.pretvoriKoordUMetre(lista);
        IzracunavanjaService.Vector2D a = expected.iterator().next();
        IzracunavanjaService.Vector2D b = actual.iterator().next();
        assertEquals(a.getX(), b.getX(), 1E-8);
        assertEquals(a.getY(), b.getY(), 1E-8);
    }

    @Test
    public void testPretvorbaKoordUMetre2(){
        IzracunavanjaService i = new IzracunavanjaService();
        List<Double> lista = new ArrayList<>();
        lista.add(0.0);
        lista.add(0.0);
        HashSet<IzracunavanjaService.Vector2D> expected = new HashSet<>();
        expected.add(new IzracunavanjaService.Vector2D(166021.44,0.0));
        HashSet<IzracunavanjaService.Vector2D> actual = i.pretvoriKoordUMetre(lista);
        IzracunavanjaService.Vector2D a = expected.iterator().next();
        IzracunavanjaService.Vector2D b = actual.iterator().next();
        assertEquals(a.getX(), b.getX(), 1E-8);
        assertEquals(a.getY(), b.getY(), 1E-8);
    }

    @Test
    public void testPretvorbaKoordUMetre3(){
        IzracunavanjaService i = new IzracunavanjaService();
        List<Double> lista = new ArrayList<>();
        lista.add(90.0);
        lista.add(90.0);
        HashSet<IzracunavanjaService.Vector2D> expected = new HashSet<>();
        expected.add(new IzracunavanjaService.Vector2D(500000.00,9997964.94));
        HashSet<IzracunavanjaService.Vector2D> actual = i.pretvoriKoordUMetre(lista);
        IzracunavanjaService.Vector2D a = expected.iterator().next();
        IzracunavanjaService.Vector2D b = actual.iterator().next();
        assertEquals(a.getX(), b.getX(), 1E-2);
        assertEquals(a.getY(), b.getY(), 1E-2);
    }

    @Test
    public void testPretvorbaKoordUMetre4(){
        IzracunavanjaService i = new IzracunavanjaService();
        List<Double> lista = new ArrayList<>();
        lista.add(45.0);
        lista.add(45.0);
        HashSet<IzracunavanjaService.Vector2D> expected = new HashSet<>();
        expected.add(new IzracunavanjaService.Vector2D(500000.00,4982950.40));
        HashSet<IzracunavanjaService.Vector2D> actual = i.pretvoriKoordUMetre(lista);
        IzracunavanjaService.Vector2D a = expected.iterator().next();
        IzracunavanjaService.Vector2D b = actual.iterator().next();
        assertEquals(a.getX(), b.getX(), 2E-2);
        assertEquals(a.getY(), b.getY(), 2E-2);
    }

    @Test
    public void testPretvorbaKoordUMetre5(){
        IzracunavanjaService i = new IzracunavanjaService();
        List<Double> lista = new ArrayList<>();
        lista.add(85.0);
        lista.add(-36.0);
        HashSet<IzracunavanjaService.Vector2D> expected = new HashSet<>();
        expected.add(new IzracunavanjaService.Vector2D(319733.34,6014201.79));
        HashSet<IzracunavanjaService.Vector2D> actual = i.pretvoriKoordUMetre(lista);
        IzracunavanjaService.Vector2D a = expected.iterator().next();
        IzracunavanjaService.Vector2D b = actual.iterator().next();
        assertEquals(a.getX(), b.getX(), 1E-1);
        assertEquals(a.getY(), b.getY(), 1E-1);
    }

    @Test
    public void testPretvorbaKoordUMetre6(){
        IzracunavanjaService i = new IzracunavanjaService();
        List<Double> lista = new ArrayList<>();
        lista.add(-5.0);
        lista.add(-67.0);
        HashSet<IzracunavanjaService.Vector2D> expected = new HashSet<>();
        expected.add(new IzracunavanjaService.Vector2D(412807.44,2567218.89));
        HashSet<IzracunavanjaService.Vector2D> actual = i.pretvoriKoordUMetre(lista);
        IzracunavanjaService.Vector2D a = expected.iterator().next();
        IzracunavanjaService.Vector2D b = actual.iterator().next();
        assertEquals(a.getX(), b.getX(), 1E-2);
        assertEquals(a.getY(), b.getY(), 1E-2);
    }

    @Test
    public void testPretvorbaKoordUMetre7(){
        IzracunavanjaService i = new IzracunavanjaService();
        List<Double> lista = new ArrayList<>();
        lista.add(-44.0);
        lista.add(17.0);
        HashSet<IzracunavanjaService.Vector2D> expected = new HashSet<>();
        expected.add(new IzracunavanjaService.Vector2D(606447.75,1879826.65));
        HashSet<IzracunavanjaService.Vector2D> actual = i.pretvoriKoordUMetre(lista);
        IzracunavanjaService.Vector2D a = expected.iterator().next();
        IzracunavanjaService.Vector2D b = actual.iterator().next();
        assertEquals(a.getX(), b.getX(), 1E-2);
        assertEquals(a.getY(), b.getY(), 1E-2);
    }
}
