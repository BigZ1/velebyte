package hr.fer.Geofighter.domain;

import com.sun.mail.imap.protocol.ID;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

@Data
@Table(name = "Kolekcija")
public class Kolekcija implements Serializable {

	private double trenutniMultiplier;

	@OneToOne
	@JoinColumn(name="idkorisnika")
	private Korisnik korisnik;
	
	@OneToOne
	@JoinColumn(name="idkarte")
	private Karta karte;

	public Kolekcija() {}

	public Kolekcija(Karta karte, Korisnik korisnik, double trenutniMultiplier){
		this.karte = karte;
		this.korisnik = korisnik;
		this.trenutniMultiplier = trenutniMultiplier;
	}

	public double getTrenutniMultiplier() {
		return trenutniMultiplier;
	}

	public void setTrenutniMultiplier(double trenutniMultiplier) {
		this.trenutniMultiplier = trenutniMultiplier;
	}

	public Korisnik getKorisnik() {
		return korisnik;
	}

	public void setKorisnik(Korisnik korisnik) {
		this.korisnik = korisnik;
	}

	public Karta getKarte() {
		return karte;
	}

	public void setKarte(Karta karte) {
		this.karte = karte;
	}
}
