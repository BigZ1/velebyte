package hr.fer.Geofighter.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Data
@Table(name = "Drzava")
public class Drzava {
	
	@Id
	@GeneratedValue(strategy = IDENTITY)
	private long IDDrzave;	
	
	private String imeDrzave;

	public Drzava(){}

	public Drzava(long IDDrzave, String imeDrzave){
		this.IDDrzave = IDDrzave;
		this.imeDrzave = imeDrzave;
	}

	public long getIDDrzave() {
		return IDDrzave;
	}

	public void setIDDrzave(long IDDrzave) {
		this.IDDrzave = IDDrzave;
	}

	public String getImeDrzave() {
		return imeDrzave;
	}

	public void setImeDrzave(String imeDrzave) {
		this.imeDrzave = imeDrzave;
	}
}
