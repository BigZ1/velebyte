package hr.fer.Geofighter.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Data
@Table(name = "Raritet")
public class Raritet {

	@Id
	@GeneratedValue(strategy = IDENTITY)
	private long IDRariteta;
	
	@Column(unique = true)
	private String nazivRariteta;

	private double multiplier;

	public Raritet(){}

	public Raritet(long IDRariteta, String nazivRariteta, double multiplier){
		this.IDRariteta = IDRariteta;
		this.nazivRariteta = nazivRariteta;
		this.multiplier = multiplier;
	}

	public long getIDRariteta() {
		return IDRariteta;
	}

	public void setIDRariteta(long IDRariteta) {
		this.IDRariteta = IDRariteta;
	}

	public String getNazivRariteta() {
		return nazivRariteta;
	}

	public void setNazivRariteta(String nazivRariteta) {
		this.nazivRariteta = nazivRariteta;
	}

	public double getMultiplier() {
		return multiplier;
	}

	public void setMultiplier(double multiplier) {
		this.multiplier = multiplier;
	}
}
