package hr.fer.Geofighter.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

import java.awt.*;
import java.awt.geom.Point2D;
import java.io.Serializable;
import java.util.Arrays;

import static javax.persistence.GenerationType.IDENTITY;

@Data
@Entity
@Table(name = "Lokacija")
public class Lokacija {

	@Id
	@GeneratedValue(strategy = IDENTITY)
	private long IDLokacije;

	private Double geoCordlng;
	private Double geoCordlat;
	private String opisLokacije;
	private long brojPosjeta;
	private byte[] urlFotografije;
	private double radijus;

	@OneToOne
	@JoinColumn(name = "idkarte")
	private Karta karta;
	
	@OneToOne
	@JoinColumn(name = "idstatusa")
	private Status status;
	
	@ManyToOne
	@JoinColumn(name = "iddrzave")
	private Drzava drzava;
	
	@ManyToOne
	@JoinColumn(name = "idgrada")
	private Grad grad;
	
	@ManyToOne
	@JoinColumn(name = "idkontinenta")
	private Kontinent kontinent;
	
	@ManyToOne
	@JoinColumn(name = "idtipa")
	private Tip tip;

	public Lokacija() {}

	public Lokacija(long IDLokacije,Double geoCordlng, Double geoCordlat, String opisLokacije, long brojPosjeta, byte[] urlFotografije, Karta karta, Status status,
					Drzava drzava, Grad grad, Kontinent kontinent, Tip tip, double radijus) {
		this.IDLokacije = IDLokacije;
		this.geoCordlng = geoCordlng;
		this.geoCordlat = geoCordlat;
		this.opisLokacije = opisLokacije;
		this.brojPosjeta = brojPosjeta;
		this.urlFotografije = urlFotografije;
		this.karta = karta;
		this.status = status;
		this.drzava = drzava;
		this.grad = grad;
		this.kontinent = kontinent;
		this.tip = tip;
		this.radijus = radijus;
	}

	public long getIDLokacije() {
		return this.IDLokacije;
	}

	public void setIDLokacije(long IDLokacije) {
		this.IDLokacije = IDLokacije;
	}

	public Double getGeoCordlng() {
		return geoCordlng;
	}

	public void setGeoCordlng(Double geoCordlng) {
		this.geoCordlng = geoCordlng;
	}

	public Double getGeoCordlat() {
		return geoCordlat;
	}

	public void setGeoCordlat(Double geoCordlat) {
		this.geoCordlat = geoCordlat;
	}

	public Drzava getDrzava() {
		return drzava;
	}

	public void setDrzava(Drzava drzava) {
		this.drzava = drzava;
	}

	public String getOpisLokacije() {
		return opisLokacije;
	}

	public void setOpisLokacije(String opisLokacije) {
		this.opisLokacije = opisLokacije;
	}

	public long getBrojPosjeta() {
		return brojPosjeta;
	}

	public void setBrojPosjeta(long brojPosjeta) {
		this.brojPosjeta = brojPosjeta;
	}

	public byte[] getUrlFotografije() {
		return urlFotografije;
	}

	public void setUrlFotografije(byte[] urlFotografije) {
		this.urlFotografije = urlFotografije;
	}

	public Karta getKarta() {
		return karta;
	}

	public void setKarta(Karta karta) {
		this.karta = karta;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public Grad getGrad() {
		return grad;
	}

	public void setGrad(Grad grad) {
		this.grad = grad;
	}

	public Kontinent getKontinent() {
		return kontinent;
	}

	public void setKontinent(Kontinent kontinent) {
		this.kontinent = kontinent;
	}

	public Tip getTip() {
		return tip;
	}

	public void setTip(Tip tip) {
		this.tip = tip;
	}

	public double getRadijus() {
		return radijus;
	}

	public void setRadijus(double radijus) {
		this.radijus = radijus;
	}

	@Override
	public String toString() {
		return "Lokacija{" +
				"IDLokacije=" + IDLokacije +
				", geoCordlng=" + geoCordlng +
				", geoCordlat=" + geoCordlat +
				", opisLokacije='" + opisLokacije + '\'' +
				", brojPosjeta=" + brojPosjeta +
				", radijus=" + radijus +
				", karta=" + karta +
				", status=" + status +
				", drzava=" + drzava +
				", grad=" + grad +
				", kontinent=" + kontinent +
				", tip=" + tip +
				'}';
	}
}
