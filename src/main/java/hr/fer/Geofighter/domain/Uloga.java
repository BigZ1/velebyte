package hr.fer.Geofighter.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import hr.fer.Geofighter.web.rest.dto.enums.UserType;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
@Table(name = "uloga")
public class Uloga {
	
	@Id
	@GeneratedValue(strategy = IDENTITY)
	private long IDUloge;
	
	private String imeUloge;

	public long getIDUloge() {
		return IDUloge;
	}

	public void setIDUloge(long IDUloge) {
		this.IDUloge = IDUloge;
	}

	public String getImeUloge() {
		return imeUloge;
	}

	public void setImeUloge(String imeUloge) {
		this.imeUloge = imeUloge;
	}

	public UserType toUserType() {
		
		switch((int)this.IDUloge) {
			case 0:
				return UserType.Player;
			case 1:
				return UserType.Cartographer;
			case 2:
				return UserType.Administrator;
			default : throw new IllegalArgumentException();
		}
	}
}
