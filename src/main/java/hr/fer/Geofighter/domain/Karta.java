package hr.fer.Geofighter.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

import java.util.List;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Data
@Table(name = "Karta")
public class Karta {
	
	@Id
	@GeneratedValue(strategy = IDENTITY)
	private long IDKarte;
	
	private String imeKarte;
	
	@ManyToOne
	@JoinColumn(name = "idrariteta")
	private Raritet rariteta;

	public Karta(){}

	public Karta(long IDKarte, String imeKarte, Raritet raritet){
		this.IDKarte = IDKarte;
		this.imeKarte = imeKarte;
		this.rariteta = raritet;
	}

	public long getIDKarte() {
		return IDKarte;
	}

	public void setIDKarte(long IDKarte) {
		this.IDKarte = IDKarte;
	}

	public String getImeKarte() {
		return imeKarte;
	}

	public void setImeKarte(String imeKarte) {
		this.imeKarte = imeKarte;
	}

	public Raritet getIdrariteta() {
		return rariteta;
	}

	public void setIdrariteta(Raritet raritet) {
		this.rariteta = raritet;
	}
}
