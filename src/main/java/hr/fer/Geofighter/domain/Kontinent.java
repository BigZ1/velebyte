package hr.fer.Geofighter.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Data
@Table(name = "Kontinent")
public class Kontinent {
	
	@Id
	@GeneratedValue(strategy = IDENTITY)
	private long IDKontinenta;	
	
	private String imeKontinenta;

	public Kontinent(){}

	public Kontinent(long IDKontinenta, String imeKontinenta){
		this.IDKontinenta = IDKontinenta;
		this.imeKontinenta = imeKontinenta;
	}

	public long getIDKontinenta() {
		return IDKontinenta;
	}

	public void setIDKontinenta(long IDKontinenta) {
		this.IDKontinenta = IDKontinenta;
	}

	public String getImeKontinenta() {
		return imeKontinenta;
	}

	public void setImeKontinenta(String imeKontinenta) {
		this.imeKontinenta = imeKontinenta;
	}
}
