package hr.fer.Geofighter.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.scheduling.commonj.TimerManagerTaskScheduler;

import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;

import java.sql.Timestamp;
import java.util.Date;
import java.util.Objects;

@Data
@Entity
@Table(name = "Korisnik")
public class Korisnik {
	
	@Id
	@GeneratedValue(strategy = IDENTITY)
	private long IDKorisnika;
	
	@Column(unique = true)
	private String korisnickoIme;
	private byte[] urlFotografije;
	@Column(unique = true)
	private String email;
	private int ELO;
	private String password;
	private boolean verified;
	private boolean banned;
	private Timestamp exparation;
	private String IBAN;
	private byte[] slikaOsobne;

	private Long[] karteKorisnika;
	private Double[] multiplierKarte;

//	@OneToOne
// 	@JoinColumn(name = "iduloge" )
	private long ulogaKorisnika;

	public Korisnik(){ }

	public Korisnik(long IDKorisnika, String korisnickoIme, byte[] urlFotografije, String email, int ELO, String password, boolean verified,
					boolean banned, Timestamp exparation, String IBAN, byte[] slikaOsobne, long ulogaKorisnika, Long[] karteKorisnika, Double[] multiplierKarte) {

		this.IDKorisnika = IDKorisnika;
		this.korisnickoIme = korisnickoIme;
		this.urlFotografije = urlFotografije;
		this.email = email;
		this.ELO = ELO;
		this.password = password;
		this.verified = verified;
		this.banned = banned;
		this.exparation = exparation;
		this.IBAN = IBAN;
		this.slikaOsobne = slikaOsobne;
		this.ulogaKorisnika = ulogaKorisnika;
		this.karteKorisnika = karteKorisnika;
		this.multiplierKarte = multiplierKarte;
	}

	public boolean isVerified() {
		return verified;
	}
	
	public boolean isBanned() {
		return banned;
	}

	public long getIDKorisnika() {
		return IDKorisnika;
	}

	public void setIDKorisnika(long IDKorisnika) {
		this.IDKorisnika = IDKorisnika;
	}

	public String getKorisnickoIme() {
		return korisnickoIme;
	}

	public void setKorisnickoIme(String korisnickoIme) {
		this.korisnickoIme = korisnickoIme;
	}

	public byte[] getUrlFotografije() {
		return urlFotografije;
	}

	public void setUrlFotografije(byte[] urlFotografije) {
		this.urlFotografije = urlFotografije;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public int getELO() {
		return ELO;
	}

	public void setELO(int ELO) {
		this.ELO = ELO;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public void setVerified(boolean verified) {
		this.verified = verified;
	}

	public void setBanned(boolean banned) {
		this.banned = banned;
	}

	public Timestamp getExparation() {
		return exparation;
	}

	public void setExparation(Timestamp exparation) {
		this.exparation = exparation;
	}

	public String getIBAN() {
		return IBAN;
	}

	public void setIBAN(String IBAN) {
		this.IBAN = IBAN;
	}

	public byte[] getSlikaOsobne() {
		return slikaOsobne;
	}

	public void setSlikaOsobne(byte[] slikaOsobne) {
		this.slikaOsobne = slikaOsobne;
	}

	public long getUlogaKorisnika() {
		return ulogaKorisnika;
	}

	public void setUlogaKorisnika(long ulogaKorisnika) {
		this.ulogaKorisnika = ulogaKorisnika;
	}

	public Long[] getKarteKorisnika() {
		return karteKorisnika;
	}

	public void setKarteKorisnika(Long[] karteKorisnika) {
		this.karteKorisnika = karteKorisnika;
	}

	public Double[] getMultiplierKarte() {
		return multiplierKarte;
	}

	public void setMultiplierKarte(Double[] multiplierKarte) {
		this.multiplierKarte = multiplierKarte;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof Korisnik)) return false;
		Korisnik korisnik = (Korisnik) o;
		return Objects.equals(korisnickoIme, korisnik.korisnickoIme);
	}

	@Override
	public int hashCode() {
		return Objects.hash(korisnickoIme);
	}

	@Override
	public String toString() {
		return "Korisnik{" +
				"IDKorisnika=" + IDKorisnika +
				", korisnickoIme='" + korisnickoIme + '\'' +
				", email='" + email + '\'' +
				", ELO='" + ELO + '\'' +
				", password='" + password + '\'' +
				", verified=" + verified +
				", banned=" + banned +
				", exparation=" + exparation +
				", IBAN='" + IBAN + '\'' +
				", ulogaKorisnika=" + ulogaKorisnika +
				'}';
	}
}
