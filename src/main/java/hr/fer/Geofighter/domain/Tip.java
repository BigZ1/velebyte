package hr.fer.Geofighter.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Data
@Table(name = "Tip")
public class Tip {
	
	@Id
	@GeneratedValue(strategy = IDENTITY)
	private long IDTip;	
	
	private String imeTipa;

	public Tip() {}

	public Tip(long IDTip, String imeTipa){
		this.IDTip = IDTip;
		this.imeTipa = imeTipa;
	}

	public long getIDTip() {
		return IDTip;
	}

	public void setIDTip(long IDTip) {
		this.IDTip = IDTip;
	}

	public String getImeTipa() {
		return imeTipa;
	}

	public void setImeTipa(String imeTipa) {
		this.imeTipa = imeTipa;
	}
}
