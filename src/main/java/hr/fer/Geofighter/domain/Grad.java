package hr.fer.Geofighter.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Data
@Table(name = "Grad")
public class Grad {
	
	@Id
	@GeneratedValue(strategy = IDENTITY)
	private long IDGrada;	
	
	private String imeGrada;

	public Grad() {}

	public Grad(long IDGrada, String imeGrada){
		this.IDGrada = IDGrada;
		this.imeGrada = imeGrada;
	}

	public long getIDGrada() {
		return IDGrada;
	}

	public void setIDGrada(long IDGrada) {
		this.IDGrada = IDGrada;
	}

	public String getImeGrada() {
		return imeGrada;
	}

	public void setImeGrada(String imeGrada) {
		this.imeGrada = imeGrada;
	}
}
