package hr.fer.Geofighter.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Data
@Table(name = "Borba")
public class Borba {
	
	@Id
	@GeneratedValue(strategy = IDENTITY)
	private long IDBorbe;
	
	private long pobjednik;
	private int dobiveniELO;

	@ManyToOne
	@JoinColumn(name = "prvikorisnik")
	private Korisnik prviKorisnik;

	@ManyToOne
	@JoinColumn(name = "drugikorisnik")
	private Korisnik drugiKorisnik;

	public Borba() {};

	public Borba(long IDBorbe, long pobjednik, int dobiveniELO, Korisnik prviKorisnik, Korisnik drugiKorisnik){
		this.IDBorbe = IDBorbe;
		this.pobjednik = pobjednik;
		this.dobiveniELO = dobiveniELO;
		this.prviKorisnik = prviKorisnik;
		this.drugiKorisnik = drugiKorisnik;
	}


	public long getIDBorbe() {
		return IDBorbe;
	}

	public void setIDBorbe(long IDBorbe) {
		this.IDBorbe = IDBorbe;
	}

	public long getPobjednik() {
		return pobjednik;
	}

	public void setPobjednik(long pobjednik) {
		this.pobjednik = pobjednik;
	}

	public int getDobiveniELO() {
		return dobiveniELO;
	}

	public void setDobiveniELO(int dobiveniELO) {
		this.dobiveniELO = dobiveniELO;
	}

	public Korisnik getPrviKorisnik() {
		return prviKorisnik;
	}

	public void setPrviKorisnik(Korisnik prviKorisnik) {
		this.prviKorisnik = prviKorisnik;
	}

	public Korisnik getDrugiKorisnik() {
		return drugiKorisnik;
	}

	public void setDrugiKorisnik(Korisnik drugiKorisnik) {
		this.drugiKorisnik = drugiKorisnik;
	}

	@Override
	public String toString() {
		return "Borba{" +
				"IDBorbe=" + IDBorbe +
				", pobjednik=" + pobjednik +
				", dobiveniELO=" + dobiveniELO +
				", prviKorisnik=" + prviKorisnik +
				", drugiKorisnik=" + drugiKorisnik +
				'}';
	}
}