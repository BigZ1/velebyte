package hr.fer.Geofighter.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import static javax.persistence.GenerationType.IDENTITY;

@Entity
@Data
@Table(name = "Status")
public class Status {
	
	@Id
	@GeneratedValue(strategy = IDENTITY)
	private long IDStatus;	
	
	private String imeStatus;

	public Status() {}

	public Status(long IDStatus, String imeStatus) {
		this.IDStatus = IDStatus;
		this.imeStatus = imeStatus;
	}

	public long getIDStatus() {
		return IDStatus;
	}

	public void setIDStatus(long IDStatus) {
		this.IDStatus = IDStatus;
	}

	public String getImeStatus() {
		return imeStatus;
	}

	public void setImeStatus(String imeStatus) {
		this.imeStatus = imeStatus;
	}
}
