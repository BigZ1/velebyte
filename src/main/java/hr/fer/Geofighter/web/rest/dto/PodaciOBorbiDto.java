package hr.fer.Geofighter.web.rest.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data

public class PodaciOBorbiDto {

    private String korisnik1;
    private String korisnik2;
    private Long[] kartePrvog;
    private Long[] karteDrugog;
    private Boolean zapocelo;
    private String prviBacac;
    private Double povrsinaPrvog;
    private Double povrsinaDrugog;
    private Long[] baceneKartePrvog;
    private Long[] baceneKarteDrugog;

    public String getKorisnik1() {
        return korisnik1;
    }

    public void setKorisnik1(String korisnik1) {
        this.korisnik1 = korisnik1;
    }

    public String getKorisnik2() {
        return korisnik2;
    }

    public void setKorisnik2(String korisnik2) {
        this.korisnik2 = korisnik2;
    }

    public Long[] getKartePrvog() {
        return kartePrvog;
    }

    public void setKartePrvog(Long[] kartePrvog) {
        this.kartePrvog = kartePrvog;
    }

    public Long[] getKarteDrugog() {
        return karteDrugog;
    }

    public void setKarteDrugog(Long[] karteDrugog) {
        this.karteDrugog = karteDrugog;
    }

    public Boolean getZapocelo() {
        return zapocelo;
    }

    public void setZapocelo(Boolean zapocelo) {
        this.zapocelo = zapocelo;
    }

    public String getPrviBacac() {
        return prviBacac;
    }

    public void setPrviBacac(String prviBacac) {
        this.prviBacac = prviBacac;
    }

    public Double getPovrsinaPrvog() {
        return povrsinaPrvog;
    }

    public void setPovrsinaPrvog(Double povrsinaPrvog) {
        this.povrsinaPrvog = povrsinaPrvog;
    }

    public Double getPovrsinaDrugog() {
        return povrsinaDrugog;
    }

    public void setPovrsinaDrugog(Double povrsinaDrugog) {
        this.povrsinaDrugog = povrsinaDrugog;
    }

    public Long[] getBaceneKartePrvog() {
        return baceneKartePrvog;
    }

    public void setBaceneKartePrvog(Long[] baceneKartePrvog) {
        this.baceneKartePrvog = baceneKartePrvog;
    }

    public Long[] getBaceneKarteDrugog() {
        return baceneKarteDrugog;
    }

    public void setBaceneKarteDrugog(Long[] baceneKarteDrugog) {
        this.baceneKarteDrugog = baceneKarteDrugog;
    }
}
