package hr.fer.Geofighter.web.rest.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class ZahtjevKolekcijeOdgovorDto {

    private Long[] idKarata;

    public Long[] getIdKarata() {
        return idKarata;
    }

    public void setIdKarata(Long[] idKarata) {
        this.idKarata = idKarata;
    }
}
