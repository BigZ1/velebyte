package hr.fer.Geofighter.web.rest.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class RegistracijaKartografaDto {
	
	private String korisnickoIme;
	private String email;
	private String sazetakLozinke;
	private String korisnickaSlika;
	private String IBAN;
	private String slikaOsobneIskaznice;

	public String getKorisnickoIme() {
		return korisnickoIme;
	}

	public void setKorisnickoIme(String korisnickoIme) {
		this.korisnickoIme = korisnickoIme;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSazetakLozinke() {
		return sazetakLozinke;
	}

	public void setSazetakLozinke(String sazetakLozinke) {
		this.sazetakLozinke = sazetakLozinke;
	}

	public String getKorisnickaSlika() {
		return korisnickaSlika;
	}

	public void setKorisnickaSlika(String korisnickaSlika) {
		this.korisnickaSlika = korisnickaSlika;
	}

	public String getIBAN() {
		return IBAN;
	}

	public void setIBAN(String IBAN) {
		this.IBAN = IBAN;
	}

	public String getSlikaOsobneIskaznice() {
		return slikaOsobneIskaznice;
	}

	public void setSlikaOsobneIskaznice(String slikaOsobneIskaznice) {
		this.slikaOsobneIskaznice = slikaOsobneIskaznice;
	}
}
