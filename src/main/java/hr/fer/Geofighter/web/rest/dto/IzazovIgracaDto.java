package hr.fer.Geofighter.web.rest.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data

public class IzazovIgracaDto {

    private String izazivac;
    private String ciljniIgrac;
    //0 -> grad, 1 -> drzava, 2 -> kontinent i 3 -> svijet
    private int tipBorbe;
    private String grad;
    private String drzava;
    private Long kontinent;

    public Long getKontinent() {
        return kontinent;
    }

    public void setKontinent(Long kontinent) {
        this.kontinent = kontinent;
    }

    public String getGrad() {
        return grad;
    }

    public void setGrad(String grad) {
        this.grad = grad;
    }

    public String getDrzava() {
        return drzava;
    }

    public void setDrzava(String drzava) {
        this.drzava = drzava;
    }

    public int getTipBorbe() {
        return tipBorbe;
    }

    public void setTipBorbe(int tipBorbe) {
        this.tipBorbe = tipBorbe;
    }

    public String getIzazivac() {
        return izazivac;
    }

    public void setIzazivac(String izazivac) {
        this.izazivac = izazivac;
    }

    public String getCiljniIgrac() {
        return ciljniIgrac;
    }

    public void setCiljniIgrac(String ciljniIgrac) {
        this.ciljniIgrac = ciljniIgrac;
    }

    @Override
    public String toString() {
        return "IzazovIgracaDto{" +
                "izazivac='" + izazivac + '\'' +
                ", ciljniIgrac='" + ciljniIgrac + '\'' +
                ", tipBorbe=" + tipBorbe +
                ", grad='" + grad + '\'' +
                ", drzava='" + drzava + '\'' +
                ", kontinent=" + kontinent +
                '}';
    }
}
