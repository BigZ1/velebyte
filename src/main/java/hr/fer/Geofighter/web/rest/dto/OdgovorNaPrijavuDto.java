package hr.fer.Geofighter.web.rest.dto;

import hr.fer.Geofighter.web.rest.dto.enums.UserType;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class OdgovorNaPrijavuDto {

	private String korisncikoIme;
	private boolean uspijeh;
	private String opis;
	private UserType tipKorisnika;

	public String getKorisncikoIme() {
		return korisncikoIme;
	}

	public void setKorisncikoIme(String korisncikoIme) {
		this.korisncikoIme = korisncikoIme;
	}

	public boolean isUspijeh() {
		return uspijeh;
	}

	public void setUspijeh(boolean uspijeh) {
		this.uspijeh = uspijeh;
	}

	public String getOpis() {
		return opis;
	}

	public void setOpis(String opis) {
		this.opis = opis;
	}

	public UserType getTipKorisnika() {
		return tipKorisnika;
	}

	public void setTipKorisnika(UserType tipKorisnika) {
		this.tipKorisnika = tipKorisnika;
	}

	@Override
	public String toString() {
		return "OdgovorNaPrijavuDto{" +
				"korisncikoIme='" + korisncikoIme + '\'' +
				", uspijeh=" + uspijeh +
				", opis='" + opis + '\'' +
				", tipKorisnika=" + tipKorisnika +
				'}';
	}
}
