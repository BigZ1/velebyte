package hr.fer.Geofighter.web.rest.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class NoviIzazoviDto {
    //NULL - ništa, CHALLANGE (izazvan od korisnickoIme), CH_CHANGE (korisnickoIme je prihvatilo ili odbilo izazov na temelju prihvacenIzazov)
    private String kod;
    private String korisnickoIme;
    private boolean prihvacenIzazov;

    public String getKod() {
        return kod;
    }

    public void setKod(String kod) {
        this.kod = kod;
    }

    public String getKorisnickoIme() {
        return korisnickoIme;
    }

    public void setKorisnickoIme(String korisnickoIme) {
        this.korisnickoIme = korisnickoIme;
    }

    public boolean isPrihvacenIzazov() {
        return prihvacenIzazov;
    }

    public void setPrihvacenIzazov(boolean prihvacenIzazov) {
        this.prihvacenIzazov = prihvacenIzazov;
    }
}
