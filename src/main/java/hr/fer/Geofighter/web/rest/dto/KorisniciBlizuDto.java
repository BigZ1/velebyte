package hr.fer.Geofighter.web.rest.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data

public class KorisniciBlizuDto {

    private String[] korisnici;

    private Double[] cords;

    public String[] getKorisnici() {
        return korisnici;
    }

    public void setKorisnici(String[] korisnici) {
        this.korisnici = korisnici;
    }

    public Double[] getCords() {
        return cords;
    }

    public void setCords(Double[] cords) {
        this.cords = cords;
    }
}
