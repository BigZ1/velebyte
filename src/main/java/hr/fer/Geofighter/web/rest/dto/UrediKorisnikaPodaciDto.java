package hr.fer.Geofighter.web.rest.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data

public class UrediKorisnikaPodaciDto {

    private String originalnoKorisnickoIme;
    private String korisnickoIme;
    private String urlFotografije;
    private String email;
    private int ELO;
    private boolean verified;
    private String IBAN;
    private String slikaOsobne;
    private long ulogaKorisnika;


    public String getOriginalnoKorisnickoIme() {
        return originalnoKorisnickoIme;
    }

    public void setOriginalnoKorisnickoIme(String originalnoKorisnickoIme) {
        this.originalnoKorisnickoIme = originalnoKorisnickoIme;
    }

    public String getKorisnickoIme() {
        return korisnickoIme;
    }

    public void setKorisnickoIme(String korisnickoIme) {
        this.korisnickoIme = korisnickoIme;
    }

    public String getUrlFotografije() {
        return urlFotografije;
    }

    public void setUrlFotografije(String urlFotografije) {
        this.urlFotografije = urlFotografije;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getELO() {
        return ELO;
    }

    public void setELO(int ELO) {
        this.ELO = ELO;
    }

    public boolean isVerified() {
        return verified;
    }

    public void setVerified(boolean verified) {
        this.verified = verified;
    }

    public String getIBAN() {
        return IBAN;
    }

    public void setIBAN(String IBAN) {
        this.IBAN = IBAN;
    }

    public String getSlikaOsobne() {
        return slikaOsobne;
    }

    public void setSlikaOsobne(String slikaOsobne) {
        this.slikaOsobne = slikaOsobne;
    }

    public long getUlogaKorisnika() {
        return ulogaKorisnika;
    }

    public void setUlogaKorisnika(long ulogaKorisnika) {
        this.ulogaKorisnika = ulogaKorisnika;
    }
}
