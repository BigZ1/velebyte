package hr.fer.Geofighter.web.rest.dto;

import hr.fer.Geofighter.domain.Korisnik;
import hr.fer.Geofighter.web.rest.controller.PrijavaController;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class ListaKorisnikaDto {

    private String[] ulogiraniKorisniciImena;

    public String[] getUlogiraniKorisniciImena() {
        return ulogiraniKorisniciImena;
    }

    public void setUlogiraniKorisniciImena(String[] ulogiraniKorisniciImena) {
        this.ulogiraniKorisniciImena = ulogiraniKorisniciImena;
    }
}
