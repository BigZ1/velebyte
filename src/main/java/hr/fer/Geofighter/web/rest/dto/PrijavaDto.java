package hr.fer.Geofighter.web.rest.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class PrijavaDto {
	
	private String IDUredaja;
	private String korisnickoIme;
	private String sazetakLozinke;

	public String getIDUredaja() {
		return IDUredaja;
	}

	public void setIDUredaja(String IDUredaja) {
		this.IDUredaja = IDUredaja;
	}

	public String getKorisnickoIme() {
		return korisnickoIme;
	}

	public void setKorisnickoIme(String korisnickoIme) {
		this.korisnickoIme = korisnickoIme;
	}

	public String getSazetakLozinke() {
		return sazetakLozinke;
	}

	public void setSazetakLozinke(String sazetakLozinke) {
		this.sazetakLozinke = sazetakLozinke;
	}
}
