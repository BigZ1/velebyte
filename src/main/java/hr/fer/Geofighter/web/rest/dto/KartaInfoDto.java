package hr.fer.Geofighter.web.rest.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class KartaInfoDto {

    private long id;
    private String imeKarte;
    private long raritet;
    private String slika;
    private String opis;
    private double longitude;
    private double latitude;

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public String getImeKarte() {
        return imeKarte;
    }

    public void setImeKarte(String imeKarte) {
        this.imeKarte = imeKarte;
    }

    public long getRaritet() {
        return raritet;
    }

    public void setRaritet(long raritet) {
        this.raritet = raritet;
    }

    public String getSlika() {
        return slika;
    }

    public void setSlika(String slika) {
        this.slika = slika;
    }

    public String getOpis() { return opis; }

    public void setOpis(String opis) { this.opis = opis; }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
}
