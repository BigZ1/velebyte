package hr.fer.Geofighter.web.rest.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class GlobalnaStatistikaDto {

    private String[] korisnickoIme;
    private int[] ELO;

    public String[] getKorisnickoIme() {
        return korisnickoIme;
    }

    public void setKorisnickoIme(String[] korisnickoIme) {
        this.korisnickoIme = korisnickoIme;
    }

    public int[] getELO() {
        return ELO;
    }

    public void setELO(int[] ELO) {
        this.ELO = ELO;
    }
}

