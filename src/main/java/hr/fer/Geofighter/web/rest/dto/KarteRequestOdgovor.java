package hr.fer.Geofighter.web.rest.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class KarteRequestOdgovor {
    private Long[] cardIds;

    public Long[] getCardIds() {
        return cardIds;
    }

    public void setCardIds(Long[] cardIds) {
        this.cardIds = cardIds;
    }
}
