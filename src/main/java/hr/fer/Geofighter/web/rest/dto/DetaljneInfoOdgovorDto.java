package hr.fer.Geofighter.web.rest.dto;

import hr.fer.Geofighter.web.rest.dto.enums.UserType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class DetaljneInfoOdgovorDto {

    private String korisnickoIme;
    private int ELO;
    private Timestamp exparation;
    private UserType ulogaKorisnika;

    public String getKorisnickoIme() {
        return korisnickoIme;
    }

    public void setKorisnickoIme(String korisnickoIme) {
        this.korisnickoIme = korisnickoIme;
    }

    public int getELO() {
        return ELO;
    }

    public void setELO(int ELO) {
        this.ELO = ELO;
    }

    public Timestamp getExparation() {
        return exparation;
    }

    public void setExparation(Timestamp exparation) {
        this.exparation = exparation;
    }

    public UserType getUlogaKorisnika() {
        return ulogaKorisnika;
    }

    public void setUlogaKorisnika(UserType ulogaKorisnika) {
        this.ulogaKorisnika = ulogaKorisnika;
    }
}
