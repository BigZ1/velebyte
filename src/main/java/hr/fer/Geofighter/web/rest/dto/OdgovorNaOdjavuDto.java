package hr.fer.Geofighter.web.rest.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class OdgovorNaOdjavuDto {
	
	private String korisnickoIme;

	public String getKorisnickoIme() {
		return korisnickoIme;
	}

	public void setKorisnickoIme(String korisnickoIme) {
		this.korisnickoIme = korisnickoIme;
	}

	@Override
	public String toString() {
		return "OdgovorNaOdjavuDto{" +
				"korisnickoIme='" + korisnickoIme + '\'' +
				'}';
	}
}
