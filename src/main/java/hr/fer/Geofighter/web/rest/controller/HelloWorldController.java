package hr.fer.Geofighter.web.rest.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RestController
@RequestMapping("/geofighter")
@RequiredArgsConstructor
public class HelloWorldController {

	@GetMapping("/")
	public String helloWorld() {
		return "HelloWorld";
	}

}
