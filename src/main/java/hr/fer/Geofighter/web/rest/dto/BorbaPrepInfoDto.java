package hr.fer.Geofighter.web.rest.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class BorbaPrepInfoDto {

    private String protivnik;
    private int tipBorbe;
    private String imeBorilista;

    public String getProtivnik() {
        return protivnik;
    }

    public void setProtivnik(String protivnik) {
        this.protivnik = protivnik;
    }

    public int getTipBorbe() {
        return tipBorbe;
    }

    public void setTipBorbe(int tipBorbe) {
        this.tipBorbe = tipBorbe;
    }

    public String getImeBorilista() {
        return imeBorilista;
    }

    public void setImeBorilista(String imeBorilista) {
        this.imeBorilista = imeBorilista;
    }

    @Override
    public String toString() {
        return "BorbaPrepInfoDto{" +
                "protivnik='" + protivnik + '\'' +
                ", tipBorbe=" + tipBorbe +
                ", imeBorilista='" + imeBorilista + '\'' +
                '}';
    }
}
