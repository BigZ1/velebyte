package hr.fer.Geofighter.web.rest.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class UACDto {

    private String korisnickoIme;
    private String slikaOsobneIskaznice;

    public String getKorisnickoIme() {
        return korisnickoIme;
    }

    public void setKorisnickoIme(String korisnickoIme) {
        this.korisnickoIme = korisnickoIme;
    }

    public String getSlikaOsobneIskaznice() {
        return slikaOsobneIskaznice;
    }

    public void setSlikaOsobneIskaznice(String slikaOsobneIskaznice) {
        this.slikaOsobneIskaznice = slikaOsobneIskaznice;
    }
}
