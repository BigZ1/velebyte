package hr.fer.Geofighter.web.rest.controller;

import java.sql.Time;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import hr.fer.Geofighter.GeoFigtherApplication;
import hr.fer.Geofighter.repository.IgracRepository;
import hr.fer.Geofighter.service.BazaPodatakaLoaderSerivce;
import hr.fer.Geofighter.service.GeoService;
import hr.fer.Geofighter.service.KartografService;
import hr.fer.Geofighter.web.rest.dto.UserInfoDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import hr.fer.Geofighter.domain.Korisnik;
import hr.fer.Geofighter.web.rest.dto.OdgovorNaPrijavuDto;
import hr.fer.Geofighter.web.rest.dto.PrijavaDto;
import hr.fer.Geofighter.web.rest.dto.enums.UserType;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RestController
@RequestMapping("/geofighter")
@RequiredArgsConstructor
public class PrijavaController {
	
	@Autowired
	private IgracRepository igracRepository;

	private static final Logger logger = LoggerFactory.getLogger(GeoFigtherApplication.class);
	public static List<GeoService> ulogiraniKorisnici = new ArrayList<>();
	public static List<Korisnik> nepotvrdeniKartografi = new ArrayList<>();


	@GetMapping("/userInfo")
	public UserInfoDto dohvatPodatakaIgraca(@RequestParam String username){
		UserInfoDto userInfoDto = new UserInfoDto();
		Korisnik k = igracRepository.findByKorisnickoImeKorisnik(username);

		userInfoDto.setKorisnickoIme(username);
		userInfoDto.setUrlFotografije(BazaPodatakaLoaderSerivce.BytesToBase64(k.getUrlFotografije()));
		userInfoDto.setUloga(userTypeFromUlogaKorisnika(k.getUlogaKorisnika()));
		userInfoDto.setElo(k.getELO());

		return userInfoDto;
	}

	@PostMapping("/prijava")
	public OdgovorNaPrijavuDto prijavaKorisnika(@RequestBody PrijavaDto prijavaDto) {
		OdgovorNaPrijavuDto odgPrijava = new OdgovorNaPrijavuDto();

		if(igracRepository.findByKorisnickoImeKorisnik(prijavaDto.getKorisnickoIme()) == null || usernameLoggedIn(prijavaDto.getKorisnickoIme())){
			odgPrijava.setUspijeh(false);
			odgPrijava.setOpis("Neuspijela prijava");
			odgPrijava.setKorisncikoIme(prijavaDto.getKorisnickoIme());

			return odgPrijava;
		}

		List<Korisnik> tmpK = igracRepository.findByKorisnickoIme(prijavaDto.getKorisnickoIme());

		if(tmpK == null){
			odgPrijava.setUspijeh(false);
			odgPrijava.setOpis("Korisnik ne postoji");
			odgPrijava.setKorisncikoIme(prijavaDto.getKorisnickoIme());

			return odgPrijava;
		}
		logger.info("--- korisnik " + igracRepository.findByKorisnickoIme(prijavaDto.getKorisnickoIme()) + " postoji, exparation je " + igracRepository.findByKorisnickoImeKorisnik(prijavaDto.getKorisnickoIme()).getExparation() + " ---");
		Date date = new Date();
		Date currentDate = new Date();

		if(igracRepository.findByKorisnickoImeKorisnik(prijavaDto.getKorisnickoIme()).getExparation() != null ) {
			date.setTime(igracRepository.findByKorisnickoImeKorisnik(prijavaDto.getKorisnickoIme()).getExparation().getTime());
			currentDate.setTime(System.currentTimeMillis());
			logger.info("--- " + date + " " +  currentDate + " ---");
		}
		//nije isteko ban
		if(currentDate.before(date) && igracRepository.findByKorisnickoImeKorisnik(prijavaDto.getKorisnickoIme()).isBanned()){
			odgPrijava.setOpis("Korisnik je banan do " + date.toString());
			return odgPrijava;
		}
		//isteko ban
		else if(currentDate.after(date) && igracRepository.findByKorisnickoImeKorisnik(prijavaDto.getKorisnickoIme()).isBanned()){
			Korisnik korisnik = igracRepository.findByKorisnickoImeKorisnik(prijavaDto.getKorisnickoIme());
			korisnik.setExparation(null);
			korisnik.setBanned(false);
			igracRepository.delete(igracRepository.findByKorisnickoImeKorisnik(prijavaDto.getKorisnickoIme()));
			igracRepository.save(korisnik);
			logger.info("--- korisnik je unbanan ---");
		}


		if(igracRepository.findByKorisnickoImeKorisnik(prijavaDto.getKorisnickoIme()).isBanned() && igracRepository.findByKorisnickoImeKorisnik(prijavaDto.getKorisnickoIme()).getExparation() == null){
			odgPrijava.setOpis("Korisnik je zauvijek banan");
			logger.info("--- " + igracRepository.findByKorisnickoImeKorisnik(prijavaDto.getKorisnickoIme()).isBanned() + " <- banan, exparation -> " + igracRepository.findByKorisnickoImeKorisnik(prijavaDto.getKorisnickoIme()).getExparation() + " ---");
			return odgPrijava;
		}

		//ako je kartograf tada napravi ovo
		if(tmpK.get(0).getUlogaKorisnika() == 1) {

			if(nepotvrdeniKartografi.containsAll(tmpK)){
				odgPrijava.setUspijeh(false);
				odgPrijava.setOpis("Kartograf nije potvrden od administratora");
				odgPrijava.setKorisncikoIme(prijavaDto.getKorisnickoIme());

				return odgPrijava;
			}

			if(tmpK.get(0).isVerified() && prijavaDto.getSazetakLozinke().equals(tmpK.get(0).getPassword())
			&& !tmpK.get(0).isBanned()){
				odgPrijava.setUspijeh(true);
				odgPrijava.setOpis("Uspijensna prijava");
				odgPrijava.setKorisncikoIme(prijavaDto.getKorisnickoIme());
			}else{
				odgPrijava.setUspijeh(false);
				odgPrijava.setOpis("Ne uspijensna prijava");
				odgPrijava.setKorisncikoIme(prijavaDto.getKorisnickoIme());
				odgPrijava.setTipKorisnika(UserType.Cartographer);

				return odgPrijava;
			}
		}

		//TODO: staviti ovdje samo korisnika a ne cijelu listu s jednim korisnikom
		else if(!igracRepository.findByKorisnickoIme(prijavaDto.getKorisnickoIme()).isEmpty() && tmpK.get(0).isVerified()
				&& prijavaDto.getSazetakLozinke().equals(tmpK.get(0).getPassword()) && !tmpK.get(0).isBanned()) {
			odgPrijava.setUspijeh(true);
			odgPrijava.setOpis("Uspijensna prijava");
			odgPrijava.setKorisncikoIme(prijavaDto.getKorisnickoIme());
		}
		else {
			odgPrijava.setUspijeh(false);
			odgPrijava.setOpis("Neuspijela prijava");
			odgPrijava.setKorisncikoIme(prijavaDto.getKorisnickoIme());

			return odgPrijava;
		}

		ulogiraniKorisnici.add(new GeoService(igracRepository.findByKorisnickoImeKorisnik(prijavaDto.getKorisnickoIme()), 0, 0));

		UserType userType = null;
		
		for(GeoService k : ulogiraniKorisnici) {
			if (k.getKorisnik().getKorisnickoIme().equals(prijavaDto.getKorisnickoIme()))
				userType = userTypeFromUlogaKorisnika(k.getKorisnik().getUlogaKorisnika());
		}

		odgPrijava.setTipKorisnika(userType);

		return odgPrijava;
	}

	public static UserType userTypeFromUlogaKorisnika(long ulogaKorisnika){
		switch ((int)ulogaKorisnika){
			case 0 : return UserType.Player;
			case 1 : return UserType.Cartographer;
			case 2 : return UserType.Administrator;
			default: throw new IllegalArgumentException("Given ulogaKorisnika is not implemented");
		}
	}

	private static boolean usernameLoggedIn(String username){
		for(int i=0; i<ulogiraniKorisnici.size(); i++){
			if(ulogiraniKorisnici.get(i).getKorisnik().getKorisnickoIme().equals(username))
				return true;
		}

		return false;
	}

}
