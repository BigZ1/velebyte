package hr.fer.Geofighter.web.rest.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

@AllArgsConstructor
@NoArgsConstructor
@Data

public class BanKorisnikaDto {
    private String korisnickoIme;
    private boolean banned;
    private String exparation;

    public String getKorisnickoIme() {
        return korisnickoIme;
    }

    public void setKorisnickoIme(String korisncikoIme) {
        this.korisnickoIme = korisncikoIme;
    }

    public boolean isBanned() {
        return banned;
    }

    public void setBanned(boolean banned) {
        this.banned = banned;
    }

    public String getExparation() {
        return exparation;
    }

    public void setExparation(String exparation) {
        this.exparation = exparation;
    }
}
