package hr.fer.Geofighter.web.rest.controller;

import hr.fer.Geofighter.GeoFigtherApplication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.web.bind.annotation.*;
import org.springframework.http.ResponseEntity;

import hr.fer.Geofighter.service.IgracService;
import hr.fer.Geofighter.service.KartografService;
import hr.fer.Geofighter.web.rest.dto.RegistracijaIgracaDto;
import hr.fer.Geofighter.web.rest.dto.RegistracijaKartografaDto;
import hr.fer.Geofighter.web.rest.dto.RegistracijskiOdgovorDto;

import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RestController
@RequestMapping("/geofighter")
@RequiredArgsConstructor
public class RegistracijskiController {

	@Autowired
	private IgracService igracService;

	@Autowired
	private KartografService kartografService;
	private static final Logger logger = LoggerFactory.getLogger(GeoFigtherApplication.class);

	@PostMapping("/registracija")
	public RegistracijskiOdgovorDto registrirajIgraca(@RequestBody RegistracijaIgracaDto registracijaIgracaDto) {
		RegistracijskiOdgovorDto odg = new RegistracijskiOdgovorDto();

		odg.setOpis(igracService.dodajIgraca(registracijaIgracaDto));
		if(odg.getOpis() == null)
			igracService.posaljiMail(registracijaIgracaDto.getEmail(), registracijaIgracaDto.getKorisnickoIme());

		odg.setEmail(registracijaIgracaDto.getEmail());

		return odg;
	}

	@GetMapping(value = "/registracija/verifikacija", params = {"token", "korisnickoIme"})
	public String verifyEmail(@RequestParam(name = "token") String token, @RequestParam(name = "korisnickoIme") String korisnickoIme) {
		logger.info("!!! USLI smo u verifyEmail !!!" + "-> " + token + "-> " + korisnickoIme);
		if(igracService.potvrdiEmail(korisnickoIme, token))
			return "Uspijesno ste potvrdili registraciju";
		
		return "Potvrda registracije nije uspijela";
	}
	
	@PostMapping("/registracijaKartografa")
	public RegistracijskiOdgovorDto registracijaKartografa(@RequestBody RegistracijaKartografaDto registracijaKartografaDto){
		RegistracijskiOdgovorDto odg = new RegistracijskiOdgovorDto();

		odg.setOpis(kartografService.dodajKartografa(registracijaKartografaDto));
		if(odg.getOpis() == null)
			kartografService.posaljiMail(registracijaKartografaDto.getEmail(), registracijaKartografaDto.getKorisnickoIme());

		odg.setEmail(registracijaKartografaDto.getEmail());

		return odg;
	}

}
