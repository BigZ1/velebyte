package hr.fer.Geofighter.web.rest.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Arrays;

@AllArgsConstructor
@NoArgsConstructor
@Data

public class BorbaPodaciDto {

    // KOD MOŽE BITI:
    //             0: igrac karta bacena - IKBA
    //             1: karta bacena info - KBAI
    //             2: info - INFO
    //             3: kraj borbe - END
    //             4: pocetak borbe - BEGIN
    //             5: cekaj drugog - WAIT

    private String kod;
    private Long[] baceneKartePrvi;
    private Long[] baceneKarteDrugi;
    private Double povrsinaPrvog;
    private Double povrsinaDrugog;
    private String posiljatelj;
    private Long bacenaKarta;

    public String getPosiljatelj() {
        return posiljatelj;
    }

    public void setPosiljatelj(String posiljatelj) {
        this.posiljatelj = posiljatelj;
    }

    public Long getBacenaKarta() {
        return bacenaKarta;
    }

    public void setBacenaKarta(Long bacenaKarta) {
        this.bacenaKarta = bacenaKarta;
    }

    public String getKod() {
        return kod;
    }

    public void setKod(String kod) {
        this.kod = kod;
    }

    public Long[] getBaceneKartePrvi() {
        return baceneKartePrvi;
    }

    public void setBaceneKartePrvi(Long[] baceneKartePrvi) {
        this.baceneKartePrvi = baceneKartePrvi;
    }

    public Long[] getBaceneKarteDrugi() {
        return baceneKarteDrugi;
    }

    public void setBaceneKarteDrugi(Long[] baceneKarteDrugi) {
        this.baceneKarteDrugi = baceneKarteDrugi;
    }

    public Double getPovrsinaPrvog() {
        return povrsinaPrvog;
    }

    public void setPovrsinaPrvog(Double povrsinaPrvog) {
        this.povrsinaPrvog = povrsinaPrvog;
    }

    public Double getPovrsinaDrugog() {
        return povrsinaDrugog;
    }

    public void setPovrsinaDrugog(Double povrsinaDrugog) {
        this.povrsinaDrugog = povrsinaDrugog;
    }

    @Override
    public String toString() {
        return "BorbaPodaciDto{" +
                "kod='" + kod + '\'' +
                ", baceneKartePrvi=" + Arrays.toString(baceneKartePrvi) +
                ", baceneKarteDrugi=" + Arrays.toString(baceneKarteDrugi) +
                ", povrsinaPrvog=" + povrsinaPrvog +
                ", povrsinaDrugog=" + povrsinaDrugog +
                ", posiljatelj='" + posiljatelj + '\'' +
                ", bacenaKarta=" + bacenaKarta +
                '}';
    }
}