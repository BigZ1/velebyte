package hr.fer.Geofighter.web.rest.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class OdabraneLokacijeDto {

    private Long[] IDs;
    private Double[] koordinate;

    public Long[] getIDs() {
        return IDs;
    }

    public void setIDs(Long[] IDs) {
        this.IDs = IDs;
    }

    public Double[] getKoordinate() {
        return koordinate;
    }

    public void setKoordinate(Double[] koordinate) {
        this.koordinate = koordinate;
    }
}
