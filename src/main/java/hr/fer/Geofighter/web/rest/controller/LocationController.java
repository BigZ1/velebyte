package hr.fer.Geofighter.web.rest.controller;

import hr.fer.Geofighter.GeoFigtherApplication;
import hr.fer.Geofighter.domain.*;
import hr.fer.Geofighter.repository.*;
import hr.fer.Geofighter.service.BazaPodatakaLoaderSerivce;
import hr.fer.Geofighter.service.GeoService;
import hr.fer.Geofighter.web.rest.dto.*;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.*;

@Data
@RestController
@RequestMapping("/geofighter")
@RequiredArgsConstructor
public class LocationController {

    @Autowired
    private IgracRepository igracRepository;

    @Autowired
    private LokacijaRepository lokacijaRepository;

    @Autowired
    private KartaRepository kartaRepository;

    @Autowired
    private RaritetRepository raritetRepository;

    @Autowired
    private DrzavaRepository drzavaRepository;

    @Autowired
    private KontinentRepository kontinentRepository;

    @Autowired
    private StatusRepository statusRepository;

    @Autowired
    private GradRepository gradRepository;

    @Autowired
    private TipRepository tipRepository;

    public static Map<Korisnik, List<Long>> odabiriKartografa = new HashMap<Korisnik, List<Long>>();

//    @Autowired
//    private KolekcijaRepository kolekcijaRepository;

    private static final Logger logger = LoggerFactory.getLogger(GeoFigtherApplication.class);

    @PostMapping("/provjeriKoordinate")
    public ProvjeriGeoOdgovorDto posaljiGeolokaciju(@RequestBody ProvjeriGeoDto provjeriGeoDto) {
        ProvjeriGeoOdgovorDto provjeriGeoOdgovorDto = new ProvjeriGeoOdgovorDto();
        List<Lokacija> sveLokacije = lokacijaRepository.getLokacijaByStatus(statusRepository.findByIdStatus(1));

        List<MultiplierKarta> multiplierKarte = new ArrayList<>();
        List<Long> skupljeneKarte = new ArrayList<>();
        List<Karta> skupljeneKarteKarta = new ArrayList<>();
        final double vidljivostKarte = 200;
        List<Integer> karteID = new ArrayList<>();
        List<Double> koordinate = new ArrayList<>();
        Korisnik kor = igracRepository.findByKorisnickoImeKorisnik(provjeriGeoDto.getKorisnickoIme());

        for(GeoService geo : PrijavaController.ulogiraniKorisnici){
            if(geo.getKorisnik().equals(kor)){
                geo.setGeoCordLng(provjeriGeoDto.getLongitude());
                geo.setGeoCordLat(provjeriGeoDto.getLatitude());

//                logger.info("!!! NA LOKACIJI JE !!!");
//                logger.info(geo.toString());
//                logger.info("!!! PRESTANAK LOKACIJE !!!");

                break;
            }
        }

        for(Lokacija lokacija : sveLokacije){
            double distanca = radiusUMetrima(lokacija.getGeoCordlat(), lokacija.getGeoCordlng(), provjeriGeoDto.getLatitude(), provjeriGeoDto.getLongitude());
            //          logger.info("!!! DISTANCA JE -->" + distanca + "!!!");
            if(distanca <= lokacija.getRadijus()){
                if(lokacija.getKarta() == null) {
                    logger.info("!!! --- KARTA JE NULL --- !!!");
                    logger.info(lokacija.toString());
                    continue;
                }
                skupljeneKarte.add(lokacija.getKarta().getIDKarte());
                skupljeneKarteKarta.add(lokacija.getKarta());

                multiplierKarte.add(new MultiplierKarta(lokacija.getKarta().getIDKarte(), lokacija.getKarta().getIdrariteta().getMultiplier()));

                logger.info("---DODANA KARTA---");
            }
            if(distanca <= vidljivostKarte){
                if(lokacija.getKarta() == null) {
                    logger.info("!!! --- KARTA JE NULL --- !!!");
                    logger.info(lokacija.toString());
                    continue;
                }

                Karta k = lokacija.getKarta();
//                karteID.add((int) k.getIDKarte());
                int karID = -1;

                if(kor.getKarteKorisnika() != null) {
                    for (long karta : kor.getKarteKorisnika()) {
                        if (karta == k.getIDKarte()) {
                            karID = (int) karta;
                            break;
                        }
                    }
                }

                karteID.add(karID);
                koordinate.add(lokacija.getGeoCordlng());
                koordinate.add(lokacija.getGeoCordlat());
            }

        }

        if(skupljeneKarte.size() > 0) {
            Set<Long> korisnikoveKarte = new LinkedHashSet<>();
            if(kor.getKarteKorisnika() != null) {
                for (long tmp : kor.getKarteKorisnika())
                    korisnikoveKarte.add(tmp);
            }

            Set<Long> skupljeneKarteSet = new LinkedHashSet<>();
            if(kor.getKarteKorisnika() != null) {
                for (long tmp : skupljeneKarte)
                    skupljeneKarteSet.add(tmp);
            }

            skupljeneKarteSet.removeAll(korisnikoveKarte);
            korisnikoveKarte.addAll(skupljeneKarte);

//            for(int i = 0; i < multiplierKarte.size(); i++){
//                if(!skupljeneKarteSet.contains(multiplierKarte.get(i).idKarte))
//                    multiplierKarte.remove(multiplierKarte.get(i--));
//            }

            Long[] skupljeneKarteArray = new Long[skupljeneKarteSet.size()];
            skupljeneKarteArray = skupljeneKarteSet.toArray(skupljeneKarteArray);
            provjeriGeoOdgovorDto.setKartaID(skupljeneKarteArray);

            Long[] skupljeneKarteArrayBaza = new Long[korisnikoveKarte.size()];
            skupljeneKarteArrayBaza = korisnikoveKarte.toArray(skupljeneKarteArrayBaza);

            Double[] multiplierKarteArrayBaza = new Double[korisnikoveKarte.size()];

            logger.info("<--- PRIJE FOROVA ---->");

            int indexMul = 0;
            if(kor.getMultiplierKarte() != null) {
                for (Double mul : kor.getMultiplierKarte())
                    multiplierKarteArrayBaza[indexMul++] = mul;

            }

            //TODO: ANALIZIRATI ERROR NESTO NEVALJA S KOLEKCIJAMA
//            logger.info("<-- skupljeneKarteArrayBaza -->");
//            logger.info(Arrays.toString(skupljeneKarteArrayBaza));
//            logger.info("<-- multiplierKarteArrayBaza -->");
//            logger.info(Arrays.toString(multiplierKarteArrayBaza));
//            logger.info("<-- multiplierKarte -->");
//            for(MultiplierKarta tmp : multiplierKarte)
//                logger.info(tmp.toString());
//            logger.info("<-- KOR.GETKARTEKORISNIKA -->");
//            logger.info(Arrays.toString(kor.getKarteKorisnika()));
//            logger.info("<-- KOR.getMultiplierKarte -->");
//            logger.info(Arrays.toString(kor.getMultiplierKarte()));
//            logger.info("<-- skupljeneKarteSet -->");
//            logger.info(skupljeneKarteSet.toString());
//            logger.info("<-- korisnikoveKarte -->");
//            logger.info(korisnikoveKarte.toString());
//            logger.info("< !!! KRAJ !!! >");

            for (long karta : skupljeneKarteArrayBaza) {
                for (MultiplierKarta tmp : multiplierKarte) {
                    if (karta == tmp.idKarte && multiplierKarteArrayBaza.length > indexMul)
                        multiplierKarteArrayBaza[indexMul++] = tmp.mulitplier;
                }
            }

            igracRepository.updateKolekcija(skupljeneKarteArrayBaza, kor.getKorisnickoIme());
            igracRepository.updateMultipliers(multiplierKarteArrayBaza, kor.getKorisnickoIme());

//            logger.info("!!! SKUPLJENE KARTE !!!");
//            logger.info("!!! " + Arrays.toString(igracRepository.findByKorisnickoImeKorisnik(provjeriGeoDto.getKorisnickoIme()).getKarteKorisnika()) + " !!!");
//
//            logger.info("!!! MULTIPLIER KARTE !!!");
//            logger.info("!!! " + Arrays.toString(igracRepository.findByKorisnickoImeKorisnik(provjeriGeoDto.getKorisnickoIme()).getMultiplierKarte()) + " !!!");

//            Kolekcija kolekcija = new Kolekcija(skupljeneKarteKarta,
//                    igracRepository.findByKorisnickoImeKorisnik(provjeriGeoDto.getKorisnickoIme()), 5);
//
//            kolekcijaRepository.save(kolekcija);
        }

        if(karteID.size() > 0){
            Integer[] karteIDArr = new Integer[karteID.size()];
            karteIDArr = karteID.toArray(karteIDArr);
            Double[] koordinateArr = new Double[koordinate.size()];
            koordinateArr = koordinate.toArray(koordinateArr);
            provjeriGeoOdgovorDto.setKoordinateBlizu(koordinateArr);
            provjeriGeoOdgovorDto.setPoznatiID(karteIDArr);
        }

        logger.info("!!! SKUPLJENE KARTE !!!");
        logger.info("!!! " + Arrays.toString(igracRepository.findByKorisnickoImeKorisnik(provjeriGeoDto.getKorisnickoIme()).getKarteKorisnika()) + " !!!");

        logger.info("!!! MULTIPLIER KARTE !!!");
        logger.info("!!! " + Arrays.toString(igracRepository.findByKorisnickoImeKorisnik(provjeriGeoDto.getKorisnickoIme()).getMultiplierKarte()) + " !!!");

        return provjeriGeoOdgovorDto;
    }

    @GetMapping(value = "/karta", params = {"kartaID"})
    public KartaInfoDto podaciOKarti(@RequestParam(name = "kartaID")long idKarte){
        KartaInfoDto kartaInfoDto = new KartaInfoDto();
        Karta karta = kartaRepository.findById(idKarte).get();

        List<Lokacija> l = kartaRepository.dohvatiLokaciju(karta.getIDKarte());
        if(l != null && l.size() != 0)
            kartaInfoDto.setOpis(l.get(0).getOpisLokacije());
        else
            kartaInfoDto.setOpis(null);

        kartaInfoDto.setId(karta.getIDKarte());
        kartaInfoDto.setImeKarte(karta.getImeKarte());
        kartaInfoDto.setRaritet(raritetRepository.findById(karta.getIdrariteta().getIDRariteta()).get().getIDRariteta());
        kartaInfoDto.setSlika(BazaPodatakaLoaderSerivce.BytesToBase64(kartaRepository.dohvatiLokaciju(karta.getIDKarte()).get(0).getUrlFotografije()));
        kartaInfoDto.setLongitude(kartaRepository.dohvatiLokaciju(karta.getIDKarte()).get(0).getGeoCordlng());
        kartaInfoDto.setLatitude(kartaRepository.dohvatiLokaciju(karta.getIDKarte()).get(0).getGeoCordlat());

        return kartaInfoDto;
    }

    private double radiusUMetrima(double lat1, double lng1, double lat2, double lng2){
        double R = 6371E3;
        double fi1 = lat1 * Math.PI / 180;
        double fi2 = lat2 * Math.PI / 180;
        double deltafi = (lat2-lat1) * Math.PI / 180;
        double deltalambda = (lng2 - lng1) * Math.PI / 180;

        double a = Math.pow(Math.sin(deltafi / 2), 2) + Math.cos(fi1) * Math.cos(fi2) * Math.pow(Math.sin(deltalambda / 2), 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

        return R * c;
    }

    @PostMapping("/dodajLokaciju")
    public AddLocationOdgovorDto dodajLokaciju(@RequestBody AddLocationDto addLocationDto){
        AddLocationOdgovorDto addLocationOdgovorDto = new AddLocationOdgovorDto();
        Lokacija l = new Lokacija();
        l.setIDLokacije(lokacijaRepository.findAll().size()+1);
        List<Drzava> drzavaList = drzavaRepository.findAll();

        boolean found = false;
        for(Drzava drzava : drzavaList){
            if(drzava.getImeDrzave().equals(addLocationDto.getCountry())){
                found = true;
                break;
            }
        }

        if(!found){
            addLocationOdgovorDto.setPoruka("Drzava ne postoji");
            return addLocationOdgovorDto;
        }

        found = false;
        List<Kontinent> kontinentList = kontinentRepository.findAll();
        for(Kontinent kontinent : kontinentList){
            if(kontinent.getImeKontinenta().equals(pretvoriKontinent(addLocationDto.getContinent()))){
                found = true;
                break;
            }
        }

        if(!found){
            addLocationOdgovorDto.setPoruka("Kontinent ne postoji");
            return addLocationOdgovorDto;
        }

        Grad grad = null;
        found = false;
        List<Grad> gradovi = gradRepository.findAll();
        for(Grad gradTmp : gradovi){
            if(gradTmp.getImeGrada().equals(addLocationDto.getTown())){
                found = true;
                grad = gradTmp;
                break;
            }
        }

        if(!found && addLocationDto.getTown() != null && !addLocationDto.getTown().isEmpty()){
            addLocationOdgovorDto.setPoruka("Grad ne postoji");
            return addLocationOdgovorDto;
        }

        l.setKontinent(kontinentRepository.findByIdKontinent(addLocationDto.getContinent() + 1));
        l.setGeoCordlat(addLocationDto.getLat());
        l.setGeoCordlng(addLocationDto.getLng());
        l.setOpisLokacije(addLocationDto.getOpis());
        l.setBrojPosjeta(0);
        l.setDrzava(drzavaRepository.findByImeDrzave(addLocationDto.getCountry()).get(0));
        l.setKontinent(kontinentRepository.findByIdKontinent(addLocationDto.getContinent() + 1));
        l.setUrlFotografije(BazaPodatakaLoaderSerivce.Base64ToBytes(addLocationDto.getImageB64()));
        l.setRadijus(20);
        l.setKarta(null);
        l.setStatus(statusRepository.findByIdStatus((long)2));
        l.setGrad(grad);
        l.setTip(null);

        lokacijaRepository.save(l);
        logger.info(l.toString());

        addLocationOdgovorDto.setPoruka("Lokacija je dodana, ceka se potvrda kartografa!");
        return addLocationOdgovorDto;
    }

    @PostMapping("/prihvatiLokaciju")
    public AddLocationOdgovorDto prihvatiLokaciju(@RequestBody AddLocationKartografDto addLocationKartografDto){
        AddLocationOdgovorDto addLocationOdgovorDto = new AddLocationOdgovorDto();

        List<Drzava> drzava = drzavaRepository.findByImeDrzave(addLocationKartografDto.getCountry());
        if(drzava.isEmpty()){
            addLocationOdgovorDto.setPoruka("Drzava ne postoji!");
            logger.info("--- DRZAVA NE POSTOJI ---");
            return addLocationOdgovorDto;
        }

        Grad grad = gradRepository.findByIdImeGrada(addLocationKartografDto.getTown());
        if(grad == null && !(addLocationKartografDto.getTown() == null || addLocationKartografDto.getTown().isEmpty())){
            addLocationOdgovorDto.setPoruka("Grad ne postoji!");
            logger.info("--- GRAD NE POSTOJI ---");
            return addLocationOdgovorDto;
        }

        Karta k = new Karta(kartaRepository.findAll().size() + 1,addLocationKartografDto.getCardName(), raritetRepository.findByIDRariteta(addLocationKartografDto.getCardRarity() + 1));
        kartaRepository.save(k);
        Korisnik ko = igracRepository.findByKorisnickoImeKorisnik(addLocationKartografDto.getUsername());
        if(odabiriKartografa.containsKey(ko)){
            if(odabiriKartografa.get(ko) != null){
                odabiriKartografa.get(ko).remove(addLocationKartografDto.getLocationID());
            }
        }

        lokacijaRepository.updateLokacija(addLocationKartografDto.getLng(), addLocationKartografDto.getLat(), addLocationKartografDto.getOpis(),
                BazaPodatakaLoaderSerivce.Base64ToBytes(addLocationKartografDto.getImageB64()), k,
                statusRepository.findByIdStatus(1), drzavaRepository.findByIme(addLocationKartografDto.getCountry()), gradRepository.findByIdImeGrada(addLocationKartografDto.getTown()),
                kontinentRepository.findByIdKontinent(addLocationKartografDto.getContinent() + 1), tipRepository.findByIDTip(addLocationKartografDto.getLocationType() + 1), addLocationKartografDto.getLocationID(),
                addLocationKartografDto.getRadius());

        addLocationOdgovorDto.setPoruka("Lokacija uspijesno prihvacena!");

        return addLocationOdgovorDto;
    }

    @PostMapping("/zahtjevLokacije")
    public ZahtjevKartograferaLokacijeDto editLokaciju(@RequestBody KartograferZahtjevDto kartograferZahtjevDto) {
        ZahtjevKartograferaLokacijeDto zahtjevKartograferaLokacijeDto = new ZahtjevKartograferaLokacijeDto();
        logger.info("--- ID LOKACIJE JE " + kartograferZahtjevDto.getLocationid() + " ---");
        if(igracRepository.findByKorisnickoImeKorisnik(kartograferZahtjevDto.getUsername()).getUlogaKorisnika() == 0)
            return null;

        List<Lokacija> nepotvrdeneLokacije = lokacijaRepository.getLokacijaByStatus(statusRepository.findByIdStatus(2));

        if(nepotvrdeneLokacije.isEmpty() && kartograferZahtjevDto.getLocationid()<0){
            zahtjevKartograferaLokacijeDto.setLocationID(-1);
            return zahtjevKartograferaLokacijeDto;
        }

        if(kartograferZahtjevDto.getLocationid()>=0) {

            Lokacija lokacija = lokacijaRepository.getLokacijaByID(kartograferZahtjevDto.getLocationid());
            logger.info("--- DAN JE ID ---");
                if (lokacija != null) {
                    logger.info("--- LOKACIJA JE " + lokacija.toString() +  " ---");
                    zahtjevKartograferaLokacijeDto.setLocationID(lokacija.getIDLokacije());
                    zahtjevKartograferaLokacijeDto.setConititent(lokacija.getKontinent().getIDKontinenta());
                    zahtjevKartograferaLokacijeDto.setCountry(lokacija.getDrzava().getImeDrzave());
                    zahtjevKartograferaLokacijeDto.setOpis(lokacija.getOpisLokacije());
                    zahtjevKartograferaLokacijeDto.setLat(lokacija.getGeoCordlat());
                    zahtjevKartograferaLokacijeDto.setLng(lokacija.getGeoCordlng());
                    zahtjevKartograferaLokacijeDto.setImageB64(BazaPodatakaLoaderSerivce.BytesToBase64(lokacija.getUrlFotografije()));
                    if (lokacija.getGrad() != null)
                        zahtjevKartograferaLokacijeDto.setTown(lokacija.getGrad().getImeGrada());
            }
        }else{
            Lokacija lok = nepotvrdeneLokacije.get(0);
            zahtjevKartograferaLokacijeDto.setLocationID(lok.getIDLokacije());
            zahtjevKartograferaLokacijeDto.setConititent(lok.getKontinent().getIDKontinenta());
            zahtjevKartograferaLokacijeDto.setCountry(lok.getDrzava().getImeDrzave());
            zahtjevKartograferaLokacijeDto.setOpis(lok.getOpisLokacije());

            if (lok.getGrad() != null)
                zahtjevKartograferaLokacijeDto.setTown(lok.getGrad().getImeGrada());

            zahtjevKartograferaLokacijeDto.setLat(lok.getGeoCordlat());
            zahtjevKartograferaLokacijeDto.setLng(lok.getGeoCordlng());
            zahtjevKartograferaLokacijeDto.setImageB64(BazaPodatakaLoaderSerivce.BytesToBase64(lok.getUrlFotografije()));

        }

        return zahtjevKartograferaLokacijeDto;
    }

    @PostMapping("/pickedLocations")
    public OdabraneLokacijeDto posaljiLokacije(@RequestBody ZahtjevZaOdabraneLokacijeDto zahtjevZaOdabraneLokacijeDto){
        OdabraneLokacijeDto odabraneLokacijeDto = new OdabraneLokacijeDto();
        odabraneLokacijeDto.setKoordinate(null);
        odabraneLokacijeDto.setIDs(null);

        Korisnik kartograf = igracRepository.findByKorisnickoImeKorisnik(zahtjevZaOdabraneLokacijeDto.getUsername());
        if(kartograf==null){
            logger.info("--- KARTOGRAF NIJE PRONAĐEN " + zahtjevZaOdabraneLokacijeDto.getUsername() + " ---");
            return odabraneLokacijeDto;
        }

        if(!odabiriKartografa.containsKey(kartograf)){
            logger.info("--- NEMA ODABRANE LOKACIJE ---");
            return odabraneLokacijeDto;
        }

        List<Long> IDjevi = odabiriKartografa.get(kartograf);

        if(IDjevi == null || IDjevi.size() == 0){
            logger.info("--- PRAZNI IDJEVI ---");
            return odabraneLokacijeDto;
        }

        List<Double> koordinate = new ArrayList<>(IDjevi.size()*2);

        for(Long id : IDjevi){
            Lokacija lokacija = lokacijaRepository.getLokacijaByID(id);
            koordinate.add(lokacija.getGeoCordlng());
            koordinate.add(lokacija.getGeoCordlat());
        }
        Long[] longPolje = new Long[IDjevi.size()];
        Double[] doublePolje = new Double[koordinate.size()];
        longPolje = IDjevi.toArray(longPolje);
        doublePolje = koordinate.toArray(doublePolje);
        odabraneLokacijeDto.setIDs(longPolje);
        odabraneLokacijeDto.setKoordinate(doublePolje);
        logger.info("--- IMA LOKACIJE " + longPolje.length + " ---");
        return odabraneLokacijeDto;
    }

    @PostMapping("/dodajLokacijuZaPregled")
    public AddLocationOdgovorDto dodajLokacijuZaPregled(@RequestBody AddLocationKartografDto addLocationKartografDto){
        AddLocationOdgovorDto addLocationOdgovorDto = new AddLocationOdgovorDto();

        lokacijaRepository.updateLokacija(addLocationKartografDto.getLng(), addLocationKartografDto.getLat(), addLocationKartografDto.getOpis(),
                BazaPodatakaLoaderSerivce.Base64ToBytes(addLocationKartografDto.getImageB64()), null,
                statusRepository.findByIdStatus(3), drzavaRepository.findByIme(addLocationKartografDto.getCountry()), gradRepository.findByIdImeGrada(addLocationKartografDto.getTown()),
                kontinentRepository.findByIdKontinent(addLocationKartografDto.getContinent() + 1), tipRepository.findByIDTip(addLocationKartografDto.getLocationType() + 1), addLocationKartografDto.getLocationID(),
                addLocationKartografDto.getRadius());

        Korisnik kartograf = igracRepository.findByKorisnickoImeKorisnik(addLocationKartografDto.getUsername());
        if(kartograf.getUlogaKorisnika()==0){
            addLocationOdgovorDto.setPoruka("Korisnik nije kartograf!");
            return addLocationOdgovorDto;
        }
        if(odabiriKartografa.containsKey(kartograf)){
            odabiriKartografa.get(kartograf).add(addLocationKartografDto.getLocationID());
        }else{
            List<Long> listaID = new ArrayList<>();
            listaID.add(addLocationKartografDto.getLocationID());
            odabiriKartografa.put(kartograf, listaID);
        }

        addLocationOdgovorDto.setPoruka("Lokacija uspješno dodana za pregled");

        return addLocationOdgovorDto;
    }

    @PostMapping("/odbijLokaciju")
    public AddLocationOdgovorDto odbijLokaciju(@RequestBody OdbijLokacijuDto odbijLokacijuDto){
        AddLocationOdgovorDto addLocationOdgovorDto = new AddLocationOdgovorDto();

        Lokacija lokacija = lokacijaRepository.getLokacijaByID(odbijLokacijuDto.getId());
        if(lokacija == null){
            addLocationOdgovorDto.setPoruka("Lokacija ne postoji");
            return addLocationOdgovorDto;
        }

        lokacijaRepository.delete(lokacija);
        Korisnik k = igracRepository.findByKorisnickoImeKorisnik(odbijLokacijuDto.getKorisnickoIme());
        if(odabiriKartografa.containsKey(k)){
            if(odabiriKartografa.get(k)!=null){
                odabiriKartografa.get(k).remove(odbijLokacijuDto.getId());
            }
        }
        addLocationOdgovorDto.setPoruka("Lokacija je odbijena");

        return addLocationOdgovorDto;
    }

    @PostMapping("/korisniciBlizu")
    public KorisniciBlizuDto korisniciBlizu(@RequestBody DohvatiBlizuDto dohvatiBlizuDto){
        KorisniciBlizuDto korisniciBlizuDto = new KorisniciBlizuDto();

        List<GeoService> korisnici = PrijavaController.ulogiraniKorisnici;
        List<GeoService> udaljeniKor = new ArrayList<>();

        for(GeoService korisnik : korisnici){

            if(korisnik.getKorisnik().getKorisnickoIme().equals(dohvatiBlizuDto.getKorisnickoIme())){
                continue;
            }

            double udaljenost = radiusUMetrima(korisnik.getGeoCordLat(), korisnik.getGeoCordLng(),
                    dohvatiBlizuDto.getLat(), dohvatiBlizuDto.getLng());

            if(udaljenost<=50000){
                udaljeniKor.add(korisnik);
            }
        }

        if(!udaljeniKor.isEmpty()) {
            List<String> imena = new ArrayList<>();
            List<Double> koord = new ArrayList<>();
            String[] imenaKorisnika = new String[udaljeniKor.size()];
            Double[] koordKorisnika = new Double[udaljeniKor.size() * 2];

            for (GeoService korisnik : udaljeniKor) {
                imena.add(korisnik.getKorisnik().getKorisnickoIme());
                koord.add(korisnik.getGeoCordLng());
                koord.add(korisnik.getGeoCordLat());
            }

            imenaKorisnika = imena.toArray(imenaKorisnika);
            koordKorisnika = koord.toArray(koordKorisnika);
            korisniciBlizuDto.setKorisnici(imenaKorisnika);
            korisniciBlizuDto.setCords(koordKorisnika);
        }

        return korisniciBlizuDto;
    }

    private static class MultiplierKarta{
        public Long idKarte;
        public Double mulitplier;

        public MultiplierKarta(Long idKarte, Double mulitplier){
            this.idKarte = idKarte;
            this.mulitplier = mulitplier;
        }

        @Override
        public String toString() {
            return "MultiplierKarta{" +
                    "idKarte=" + idKarte +
                    ", mulitplier=" + mulitplier +
                    '}';
        }

    }

    public static String pretvoriKontinent(long br){
        switch((int)br){
            case 0:
                return "Europe";
            case 1:
                return "Asia";
            case 2:
                return "Africa";
            case 3:
                return "North America";
            case 4:
                return "South America";
            case 5:
                return "Australia";
            default:
                return "Antarctica";
        }
    }
}
