package hr.fer.Geofighter.web.rest.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class ZahtjevKolekcijeDto {

    private String korisnickoIme;

    public String getKorisnikoIme() {
        return korisnickoIme;
    }

    public void setKorisnickoIme(String korisnikoIme) {
        this.korisnickoIme = korisnikoIme;
    }
}
