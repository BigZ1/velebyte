package hr.fer.Geofighter.web.rest.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class PocetakBorbeDto {

    private String korisnickoIme;
    private Long[] odabraneKarte;

    public String getKorisnickoIme() {
        return korisnickoIme;
    }

    public void setKorisnickoIme(String korisnickoIme) {
        this.korisnickoIme = korisnickoIme;
    }

    public Long[] getOdabraneKarte() {
        return odabraneKarte;
    }

    public void setOdabraneKarte(Long[] odabraneKarte) {
        this.odabraneKarte = odabraneKarte;
    }
}
