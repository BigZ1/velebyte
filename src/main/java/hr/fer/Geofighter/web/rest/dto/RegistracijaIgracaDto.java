package hr.fer.Geofighter.web.rest.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class RegistracijaIgracaDto {
	
	private String korisnickoIme;
	private String email;
	private String sazetakLozinke;
	private String korisnickaSlika;

	public String getKorisnickoIme() {
		return korisnickoIme;
	}

	public void setKorisnickoIme(String korisnickoIme) {
		this.korisnickoIme = korisnickoIme;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getSazetakLozinke() {
		return sazetakLozinke;
	}

	public void setSazetakLozinke(String sazetakLozinke) {
		this.sazetakLozinke = sazetakLozinke;
	}

	public String getKorisnickaSlika() {
		return korisnickaSlika;
	}

	public void setKorisnickaSlika(String korisnickaSlika) {
		this.korisnickaSlika = korisnickaSlika;
	}

	@Override
	public String toString() {
		return "RegistracijaIgracaDto{" +
				"korisnickoIme='" + korisnickoIme + '\'' +
				", email='" + email + '\'' +
				", sazetakLozinke='" + sazetakLozinke + '\'' +
				'}';
	}
}
