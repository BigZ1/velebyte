package hr.fer.Geofighter.web.rest.dto.enums;

import hr.fer.Geofighter.domain.Uloga;

public enum UserType {
	Player,
	Cartographer,
	Administrator;
	
	public Uloga toUloga() {
		Uloga tmp = new Uloga();
		
		switch(this) {
		case Player:
			tmp.setIDUloge(0);
			tmp.setImeUloge("Player");
			break;
		case Cartographer:
			tmp.setIDUloge(1);
			tmp.setImeUloge("Cartographer");
			break;
		case Administrator:
			tmp.setIDUloge(2);
			tmp.setImeUloge("Administrator");
			break;
		default:
			throw new IllegalArgumentException();
		}
		
		return tmp;
	}
}
