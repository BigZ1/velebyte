package hr.fer.Geofighter.web.rest.controller;

import hr.fer.Geofighter.GeoFigtherApplication;
import hr.fer.Geofighter.domain.Korisnik;
import hr.fer.Geofighter.repository.IgracRepository;
import hr.fer.Geofighter.service.BazaPodatakaLoaderSerivce;
import hr.fer.Geofighter.web.rest.dto.*;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

@Data
@RestController
@RequestMapping("/geofighter")
@RequiredArgsConstructor
public class AdministratorController {

    @Autowired
    private IgracRepository igracRepository;

    private static final Logger logger = LoggerFactory.getLogger(GeoFigtherApplication.class);

    @GetMapping("/requestUACData")
    public UACDto potvrdaKartografaAdmin(){
        UACDto uac = new UACDto();
        uac.setKorisnickoIme(null);
        uac.setSlikaOsobneIskaznice(null);

        if(!PrijavaController.nepotvrdeniKartografi.isEmpty()){
            Korisnik k = PrijavaController.nepotvrdeniKartografi.get(0);

            uac.setKorisnickoIme(k.getKorisnickoIme());
            uac.setSlikaOsobneIskaznice(BazaPodatakaLoaderSerivce.BytesToBase64(k.getSlikaOsobne()));
        }

        return uac;
    }

    @PostMapping("/potvrdaKartografa")
    public UACOdgovorInfoDto potvrdi(@RequestBody UACOdgovorDto uacOdgovor){
        UACOdgovorInfoDto uacOdgovorInfo = new UACOdgovorInfoDto();

        Korisnik k = igracRepository.findByKorisnickoImeKorisnik(uacOdgovor.getKorisnickoIme());

        if(PrijavaController.nepotvrdeniKartografi.contains(k)) {
            if (uacOdgovor.isPrihvacen())
                uacOdgovorInfo.setPoruka("Kartografer je prihvacen");

            else {
                uacOdgovorInfo.setPoruka("Kartografer nije prihvacen");
                igracRepository.delete(k);
            }

            PrijavaController.nepotvrdeniKartografi.remove(k);
        }
        else
            uacOdgovorInfo.setPoruka("Nema kartografa za potvrditi");

        return uacOdgovorInfo;
    }

    @PostMapping("/banned")
    public BanKorisnikaDto banned(@RequestBody DetaljneInfoDto detaljneInfoDto){

        BanKorisnikaDto banKorisnikaDto = new BanKorisnikaDto();
        if (igracRepository.findByKorisnickoImeKorisnik(detaljneInfoDto.getKorisnickoIme()) == null){
            banKorisnikaDto.setKorisnickoIme(null);
            banKorisnikaDto.setExparation(null);
            banKorisnikaDto.setBanned(false);
            return banKorisnikaDto;
        }

        Korisnik k = igracRepository.findByKorisnickoImeKorisnik(detaljneInfoDto.getKorisnickoIme());

        banKorisnikaDto.setBanned(k.isBanned());
        if(k.getExparation() != null) {
            banKorisnikaDto.setExparation(k.getExparation().toString());
        }else{
            banKorisnikaDto.setExparation(null);
        }
        banKorisnikaDto.setKorisnickoIme(k.getKorisnickoIme());

        return banKorisnikaDto;
    }

    @PostMapping("/ban")
    public UACOdgovorInfoDto ban(@RequestBody BanKorisnikaDto banKorisnikaDto){
        UACOdgovorInfoDto uacOdgovorInfoDto = new UACOdgovorInfoDto();

        if (igracRepository.findByKorisnickoImeKorisnik(banKorisnikaDto.getKorisnickoIme()) == null){
            uacOdgovorInfoDto.setPoruka("Korisnik ne postoji");
            return uacOdgovorInfoDto;
        }

        Korisnik k = igracRepository.findByKorisnickoImeKorisnik(banKorisnikaDto.getKorisnickoIme());

        if(k.getUlogaKorisnika()==2){
            uacOdgovorInfoDto.setPoruka("Ne moze se banati administratora");
            return uacOdgovorInfoDto;
        }
        logger.info("--- t = " + banKorisnikaDto.getExparation() + " ---");
        Korisnik korisnik = k;
        if(banKorisnikaDto.getExparation()==null || banKorisnikaDto.getExparation().length()==0){
                k.setBanned(banKorisnikaDto.isBanned());
                k.setExparation(null);
                igracRepository.delete(korisnik);
                igracRepository.save(k);

                uacOdgovorInfoDto.setPoruka("Igrac uspjesno banan");
                return uacOdgovorInfoDto;
        }
        Timestamp t = null;
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss.SSS");
            Date parsedDate = dateFormat.parse(banKorisnikaDto.getExparation());
            t = new java.sql.Timestamp(parsedDate.getTime());
        } catch(Exception e) {
            logger.info("--- dogodio se exception, t = " + t + " ---");
            uacOdgovorInfoDto.setPoruka("Dogodila se pogreska pri bananju");
            return uacOdgovorInfoDto;
        }
        logger.info("--- t = " + t + " ---");
        k.setBanned(true);
        k.setExparation(t);

        igracRepository.delete(korisnik);
        igracRepository.save(k);

        uacOdgovorInfoDto.setPoruka("Igrac uspjesno banan");
        return uacOdgovorInfoDto;
    }

}
