package hr.fer.Geofighter.web.rest.dto;

import hr.fer.Geofighter.domain.Uloga;
import hr.fer.Geofighter.web.rest.dto.enums.UserType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data
public class UserInfoDto {

    private String korisnickoIme;
    private String urlFotografije;
    private int elo;
    private UserType uloga;

    public int getElo() {
        return elo;
    }

    public void setElo(int elo) {
        this.elo = elo;
    }

    public String getKorisnickoIme() {
        return korisnickoIme;
    }

    public void setKorisnickoIme(String korisnickoIme) {
        this.korisnickoIme = korisnickoIme;
    }

    public String getUrlFotografije() {
        return urlFotografije;
    }

    public void setUrlFotografije(String urlFotografije) {
        this.urlFotografije = urlFotografije;
    }

    public UserType getUloga() {
        return uloga;
    }

    public void setUloga(UserType uloga) {
        this.uloga = uloga;
    }

    @Override
    public String toString() {
        return "UserInfoDto{" +
                "korisnickoIme='" + korisnickoIme + '\'' +
                ", urlFotografije='" + urlFotografije + '\'' +
                ", uloga=" + uloga +
                '}';
    }
}
