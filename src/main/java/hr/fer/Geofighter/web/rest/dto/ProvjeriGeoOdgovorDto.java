package hr.fer.Geofighter.web.rest.dto;

public class ProvjeriGeoOdgovorDto {

    private Long[] kartaID;
    private Double[] koordinateBlizu;
    private Integer[] poznatiID;

    public Long[] getKartaID() {
        return kartaID;
    }

    public void setKartaID(Long[] kartaID) {
        this.kartaID = kartaID;
    }

    public Double[] getKoordinateBlizu() { return koordinateBlizu; }

    public void setKoordinateBlizu(Double[] koordinateBlizu) { this.koordinateBlizu = koordinateBlizu; }

    public Integer[] getPoznatiID() { return poznatiID; }

    public void setPoznatiID(Integer[] poznatiID) { this.poznatiID = poznatiID; }
}
