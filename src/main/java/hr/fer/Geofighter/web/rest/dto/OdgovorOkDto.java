package hr.fer.Geofighter.web.rest.dto;

public class OdgovorOkDto {

    private String poruka;

    public String getPoruka() {
        return poruka;
    }

    public void setPoruka(String poruka) {
        this.poruka = poruka;
    }
}
