package hr.fer.Geofighter.web.rest.dto;

public class KorisniciUBorbiDto {

    private String prviKorisnik;
    private String drugiKorisnik;

    public String getPrviKorisnik() {
        return prviKorisnik;
    }

    public void setPrviKorisnik(String prviKorisnik) {
        this.prviKorisnik = prviKorisnik;
    }

    public String getDrugiKorisnik() {
        return drugiKorisnik;
    }

    public void setDrugiKorisnik(String drugiKorisnik) {
        this.drugiKorisnik = drugiKorisnik;
    }
}
