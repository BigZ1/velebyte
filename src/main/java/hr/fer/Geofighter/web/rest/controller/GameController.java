package hr.fer.Geofighter.web.rest.controller;

import hr.fer.Geofighter.GeoFigtherApplication;
import hr.fer.Geofighter.domain.Borba;
import hr.fer.Geofighter.domain.Korisnik;
import hr.fer.Geofighter.repository.BorbaRepository;
import hr.fer.Geofighter.repository.IgracRepository;
import hr.fer.Geofighter.service.BazaPodatakaLoaderSerivce;
import hr.fer.Geofighter.service.GeoService;
import hr.fer.Geofighter.web.rest.dto.*;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

@Data
@RestController
@RequestMapping("/geofighter")
@RequiredArgsConstructor
public class GameController {

    @Autowired
    private IgracRepository igracRepository;

    @Autowired
    private BorbaRepository borbaRepository;

    private final int ELO_CONSTANT = 400;

    private final int K_CONSTANT = 30;

//    @Autowired
//    private KolekcijaRepository kolekcijaRepository;

    private static final Logger logger = LoggerFactory.getLogger(GeoFigtherApplication.class);

    @GetMapping("/aktivniIgraci")
    public ListaKorisnikaDto sviUlogiraniIgraci(){
        ListaKorisnikaDto ulogiraniIgraciDto = new ListaKorisnikaDto();
        String[] imenaIgraca = new String[PrijavaController.ulogiraniKorisnici.size()];

        int i = 0;
        for(GeoService k : PrijavaController.ulogiraniKorisnici){
            if(k.getKorisnik().getUlogaKorisnika() == 0)
                imenaIgraca[i++] = k.getKorisnik().getKorisnickoIme();
        }

        imenaIgraca = Arrays.copyOf(imenaIgraca, i);
        ulogiraniIgraciDto.setUlogiraniKorisniciImena(imenaIgraca);

        return ulogiraniIgraciDto;
    }

    @GetMapping("/sviKorisnici")
    public ListaKorisnikaDto sviKorisnici(){

        ListaKorisnikaDto listaKorisnikaDto = new ListaKorisnikaDto();
        List<Korisnik> sviKorisnici = igracRepository.findAll();
        String[] imenaIgraca = new String[sviKorisnici.size()];

        int i = 0;
        for(Korisnik k : sviKorisnici){
            imenaIgraca[i++] = k.getKorisnickoIme();
        }
        listaKorisnikaDto.setUlogiraniKorisniciImena(imenaIgraca);
        return listaKorisnikaDto;
    }

    @PostMapping("/izbrisiKorisnika")
    public IzbrisiKorisnikaOdgovorDto izbrisiKorisnika(@RequestBody IzbrisiKorisnikaDto izbrisiKorisnikaDto){
        IzbrisiKorisnikaOdgovorDto izbrisiKorisnikaOdgovorDto = new IzbrisiKorisnikaOdgovorDto();
        Korisnik k = igracRepository.findByKorisnickoImeKorisnik(izbrisiKorisnikaDto.getKorisnickoIme());
        if(k == null){
            izbrisiKorisnikaOdgovorDto.setPoruka("Korisnik ne postoji u bazi podataka");
        }else {
            if(k.getUlogaKorisnika()==2){
                izbrisiKorisnikaOdgovorDto.setPoruka("Dani korisnik je administrator");
            } else {
                igracRepository.delete(k);
                izbrisiKorisnikaOdgovorDto.setPoruka("Dani korisnik je izbrisan");
            }
        }

        return izbrisiKorisnikaOdgovorDto;
    }

    @PostMapping("/detaljneInformacije")
    public DetaljneInfoOdgovorDto detalji(@RequestBody DetaljneInfoDto detaljneInfoDto){
        DetaljneInfoOdgovorDto detaljneInfoOdgovorDto = new DetaljneInfoOdgovorDto();
        Korisnik k = igracRepository.findByKorisnickoImeKorisnik(detaljneInfoDto.getKorisnickoIme());

        detaljneInfoOdgovorDto.setELO(k.getELO());
        detaljneInfoOdgovorDto.setExparation(k.getExparation());
        detaljneInfoOdgovorDto.setKorisnickoIme(k.getKorisnickoIme());
        detaljneInfoOdgovorDto.setUlogaKorisnika(PrijavaController.userTypeFromUlogaKorisnika(k.getUlogaKorisnika()));

        return detaljneInfoOdgovorDto;
    }

    @PostMapping("/dohvatPodatakaZaUredivanje")
    public UrediKorisnikaPodaciDto dohvatPodataka(@RequestBody UrediKorisnikaZahtjevDto urediKorisnikaZahtjevDto){
        logger.info("--- TRAZENI KORISNIK ---" + urediKorisnikaZahtjevDto.getKorisnickoIme());
        UrediKorisnikaPodaciDto urediKorisnikaPodaciDto = new UrediKorisnikaPodaciDto();
        Korisnik k = igracRepository.findByKorisnickoImeKorisnik(urediKorisnikaZahtjevDto.getKorisnickoIme());
        if(k==null) {
            urediKorisnikaPodaciDto.setKorisnickoIme("user deleted");
            return urediKorisnikaPodaciDto;
        }
        urediKorisnikaPodaciDto.setOriginalnoKorisnickoIme(k.getKorisnickoIme());
        urediKorisnikaPodaciDto.setKorisnickoIme(k.getKorisnickoIme());
        urediKorisnikaPodaciDto.setEmail(k.getEmail());
        urediKorisnikaPodaciDto.setUlogaKorisnika(k.getUlogaKorisnika());
        urediKorisnikaPodaciDto.setIBAN(k.getIBAN());
        urediKorisnikaPodaciDto.setELO(k.getELO());
        urediKorisnikaPodaciDto.setUrlFotografije(BazaPodatakaLoaderSerivce.BytesToBase64(k.getUrlFotografije()));
        urediKorisnikaPodaciDto.setSlikaOsobne(BazaPodatakaLoaderSerivce.BytesToBase64(k.getSlikaOsobne()));
        urediKorisnikaPodaciDto.setVerified(k.isVerified());

        return urediKorisnikaPodaciDto;
    }

    @PostMapping("/dohvatiKolekciju")
    public ZahtjevKolekcijeOdgovorDto dohvatiKolekciju(@RequestBody ZahtjevKolekcijeDto zahtjevKolekcijeDto){
        ZahtjevKolekcijeOdgovorDto zahtjevKolekcijeOdgovorDto = new ZahtjevKolekcijeOdgovorDto();

        Korisnik k = igracRepository.findByKorisnickoImeKorisnik(zahtjevKolekcijeDto.getKorisnikoIme());
//        logger.info("!!! KRECE !!!");
//        logger.info(zahtjevKolekcijeDto.getKorisnikoIme());
//        logger.info(Arrays.toString(k.getKarteKorisnika()));
        zahtjevKolekcijeOdgovorDto.setIdKarata(k.getKarteKorisnika());

        return zahtjevKolekcijeOdgovorDto;
    }

    @PostMapping("/urediKorisnika")
    public UrediKorisnikaOdgovorDto urediKorisnika(@RequestBody UrediKorisnikaPodaciDto urediKorisnikaPodaciDto){
        UrediKorisnikaOdgovorDto urediKorisnikaOdgovorDto = new UrediKorisnikaOdgovorDto();
        Korisnik k = igracRepository.findByKorisnickoImeKorisnik(urediKorisnikaPodaciDto.getOriginalnoKorisnickoIme());
        logger.info("<--- INFO PRIJE--->");
        logger.info(k.toString());
        List<Korisnik> sviKorisnici = igracRepository.findAll();

        if(k==null){
            urediKorisnikaOdgovorDto.setPoruka("user deleted");
            return urediKorisnikaOdgovorDto;
        }

        for(Korisnik kor : sviKorisnici){
            if(!kor.equals(k)) {
                if (urediKorisnikaPodaciDto.getKorisnickoIme().equals(kor.getKorisnickoIme())) {
                    urediKorisnikaOdgovorDto.setPoruka("Korisnicko ime vec postoji");
                    return urediKorisnikaOdgovorDto;
                } else if (urediKorisnikaPodaciDto.getEmail().equals(kor.getEmail())) {
                    urediKorisnikaOdgovorDto.setPoruka("Email vec postoji");
                    return urediKorisnikaOdgovorDto;
                }
            }
        }

        if(k.getUlogaKorisnika()==2 && urediKorisnikaPodaciDto.getUlogaKorisnika()!=2) {
            urediKorisnikaOdgovorDto.setPoruka("Ne moze se mijenjati uloga administratora");
        }else{
            Korisnik korisnik = uredeniKorisnik(urediKorisnikaPodaciDto,k);
            igracRepository.updateKorisnik(korisnik.getKorisnickoIme(), urediKorisnikaPodaciDto.getOriginalnoKorisnickoIme(), korisnik.getEmail(), korisnik.getUlogaKorisnika(), korisnik.getELO(),
                                            korisnik.getIBAN(), korisnik.getUrlFotografije(), korisnik.getSlikaOsobne(), korisnik.isVerified());
            logger.info("<--- NAKON DODAVANJA --->");
            logger.info(korisnik.toString());
            urediKorisnikaOdgovorDto.setPoruka("Korisnik je uspjesno ureden");
        }

        return urediKorisnikaOdgovorDto;
    }

    @GetMapping("/globalnaStatistika")
    public GlobalnaStatistikaDto posaljiGlobalnuStatistiku(){
        GlobalnaStatistikaDto globalnaStatistikaDto = new GlobalnaStatistikaDto();

        List<Korisnik> top10Korisnika = igracRepository.dohvatiTop10Igraca();
        String[] korisnici = new String[10];
        int[] ELO = new int[10];

        for(int i = 0; i < top10Korisnika.size(); i++){
            korisnici[i] = top10Korisnika.get(i).getKorisnickoIme();
            ELO[i] = top10Korisnika.get(i).getELO();
        }

        globalnaStatistikaDto.setELO(ELO);
        globalnaStatistikaDto.setKorisnickoIme(korisnici);

        return globalnaStatistikaDto;
    }

    @GetMapping("/ok")
    public OdgovorOkDto okProvjera(){
        OdgovorOkDto odgovorOkDto = new OdgovorOkDto();
        odgovorOkDto.setPoruka("ok");
        return odgovorOkDto;
    }

    private static Korisnik uredeniKorisnik(UrediKorisnikaPodaciDto urediKorisnikaPodaciDto, Korisnik korisnik){
        Korisnik originalniKorisnik = korisnik;

        originalniKorisnik.setKorisnickoIme(urediKorisnikaPodaciDto.getKorisnickoIme());
        originalniKorisnik.setEmail(urediKorisnikaPodaciDto.getEmail());
        originalniKorisnik.setUlogaKorisnika(urediKorisnikaPodaciDto.getUlogaKorisnika());
        originalniKorisnik.setELO(urediKorisnikaPodaciDto.getELO());
        originalniKorisnik.setIBAN(urediKorisnikaPodaciDto.getIBAN());
        originalniKorisnik.setUrlFotografije(BazaPodatakaLoaderSerivce.Base64ToBytes(urediKorisnikaPodaciDto.getUrlFotografije()));
        originalniKorisnik.setSlikaOsobne(BazaPodatakaLoaderSerivce.Base64ToBytes(urediKorisnikaPodaciDto.getSlikaOsobne()));
        originalniKorisnik.setVerified(urediKorisnikaPodaciDto.isVerified());

        return originalniKorisnik;
    }

    private void modificirajELO(Korisnik igrac1, Korisnik igrac2, boolean prviIgracPobjedio){
        double mogucnostDaPrviPobjedi = mogucnostPobjede(igrac2.getELO(), igrac1.getELO());
        double mogucnostDaDrugiPobjedi = mogucnostPobjede(igrac1.getELO(), igrac2.getELO());

        if(prviIgracPobjedio){
            igracRepository.updateELO(igrac1.getKorisnickoIme(), (int)(igrac1.getELO() + K_CONSTANT * (1 - mogucnostDaPrviPobjedi)));
            igracRepository.updateELO(igrac2.getKorisnickoIme(), (int)(igrac2.getELO() + K_CONSTANT * (0 - mogucnostDaDrugiPobjedi)));
        }else{
            igracRepository.updateELO(igrac1.getKorisnickoIme(), (int)(igrac1.getELO() + K_CONSTANT * (0 - mogucnostDaPrviPobjedi)));
            igracRepository.updateELO(igrac2.getKorisnickoIme(), (int)(igrac2.getELO() + K_CONSTANT * (1 - mogucnostDaDrugiPobjedi)));
        }
    }

    private double mogucnostPobjede(double ELO1, double ELO2){
        return 1.0 / (1.0 + (Math.pow(10, (ELO1 - ELO2) / ELO_CONSTANT)));
    }
}
