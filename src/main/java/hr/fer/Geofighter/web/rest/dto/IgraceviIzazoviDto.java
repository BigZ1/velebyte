package hr.fer.Geofighter.web.rest.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data

public class IgraceviIzazoviDto {

    private String[] korisnickaImena;

    public String[] getKorisnickaImena() {
        return korisnickaImena;
    }

    public void setKorisnickaImena(String[] korisnickaImena) {
        this.korisnickaImena = korisnickaImena;
    }
}
