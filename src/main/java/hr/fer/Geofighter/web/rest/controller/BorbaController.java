package hr.fer.Geofighter.web.rest.controller;

import hr.fer.Geofighter.GeoFigtherApplication;
import hr.fer.Geofighter.domain.Borba;
import hr.fer.Geofighter.domain.Kontinent;
import hr.fer.Geofighter.domain.Korisnik;
import hr.fer.Geofighter.repository.IgracRepository;
import hr.fer.Geofighter.repository.KontinentRepository;
import hr.fer.Geofighter.service.BorbaService;
import hr.fer.Geofighter.service.GeoService;
import hr.fer.Geofighter.service.IzracunavanjaService;
import hr.fer.Geofighter.web.rest.dto.*;
import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;

@Data
@RestController
@RequestMapping("/geofighter")
@RequiredArgsConstructor

public class BorbaController {

    @Autowired
    private IgracRepository igracRepository;

    @Autowired
    private KontinentRepository kontinentRepository;

    private static final Logger logger = LoggerFactory.getLogger(GeoFigtherApplication.class);

    @PostMapping("/izazoviIgraca")
    public BorbaInfoDto izazoviIgraca(@RequestBody IzazovIgracaDto izazovIgracaDto){
        BorbaInfoDto borbaInfoDto = new BorbaInfoDto();
        List<GeoService> korisnici = PrijavaController.ulogiraniKorisnici;
        Korisnik ciljaniIgrac = igracRepository.findByKorisnickoImeKorisnik(izazovIgracaDto.getCiljniIgrac());
        Korisnik izazivac = igracRepository.findByKorisnickoImeKorisnik(izazovIgracaDto.getIzazivac());

        logger.info("!!!     CILJNI IGRAC     !!!");
        logger.info(ciljaniIgrac.toString());
        logger.info("!!!    IZAZIVAC     !!!");
        logger.info(izazivac.toString());

        boolean prijavljen = false;

        for(int i = 0; i < PrijavaController.ulogiraniKorisnici.size(); i++) {
            Korisnik k = PrijavaController.ulogiraniKorisnici.get(i).getKorisnik();

            if(k.equals(ciljaniIgrac)) {
                prijavljen = true;
                break;
            }
        }

        if (!prijavljen) {
            borbaInfoDto.setPoruka("Igrac nije prijavljen");
            return borbaInfoDto;
        }

        for(Korisnik korisnik : BorbaService.izazoviKorisnika.keySet()) {
            HashSet<Korisnik> izazovi = BorbaService.izazoviKorisnika.get(korisnik);
            if (izazovi != null) {
                if (izazovi.contains(izazivac)) {
                    borbaInfoDto.setPoruka("Igrac neuspjesno izazvan");
                    return borbaInfoDto;
                }
            }
        }
        if(!BorbaService.izazoviKorisnika.containsKey(ciljaniIgrac)){
            BorbaService.izazoviKorisnika.put(ciljaniIgrac, new HashSet<>());
        }
        if(BorbaService.izazoviKorisnika.get(ciljaniIgrac) == null){
            BorbaService.izazoviKorisnika.put(ciljaniIgrac, new HashSet<>());
        }

        BorbaService.izazoviKorisnika.get(ciljaniIgrac).add(izazivac);
        logger.info("!!!! STAVLJANJE U SET !!!!");
        logger.info("" + BorbaService.izazoviKorisnika.get(ciljaniIgrac).size());
        borbaInfoDto.setPoruka("Igrac uspjesno izazvan");
        BorbaService.podaciIzazova.put(izazivac, izazovIgracaDto);
        logger.info("<--- !!! STVARANJE IZAZOVA IGRACA !!! --->");
        logger.info(izazovIgracaDto.toString());

        if(!BorbaService.redCekanja.containsKey(ciljaniIgrac))
            BorbaService.redCekanja.put(ciljaniIgrac, new LinkedList<>());

        BorbaService.redCekanja.get(ciljaniIgrac).add(izazivac);

        return borbaInfoDto;
    }

    @PostMapping("/mojiIzazovi")
    public IgraceviIzazoviDto igraceviIzazovi(@RequestBody ZahtjevIgracevihIzazovaDto zahtjevIgracevihIzazovaDto){
        IgraceviIzazoviDto igraceviIzazoviDto = new IgraceviIzazoviDto();
        Korisnik igrac = igracRepository.findByKorisnickoImeKorisnik(zahtjevIgracevihIzazovaDto.getKorisnickoIme());
        logger.info("!!!!!! Igrac" + igrac.toString() + " !!!!!!!!!!!!!!");

        if(!BorbaService.izazoviKorisnika.containsKey(igrac)){
            logger.info("<--- PRVI IF --->");

            logger.info(BorbaService.izazoviKorisnika.size() + "");

            for(Korisnik k : BorbaService.izazoviKorisnika.keySet()){
                logger.info("!!!!" + k.toString() + " !!!!" + "--- PRVI FOR ---");
                for(Korisnik k2 : BorbaService.izazoviKorisnika.get(k)){
                    logger.info("!!!!" + k2.toString() + " !!!!" + "--- DRUGI FOR ---");
                }
            }

            igraceviIzazoviDto.setKorisnickaImena(null);
            return igraceviIzazoviDto;
        }

        if(BorbaService.izazoviKorisnika.get(igrac) == null) {
            logger.info("<--- DRUGI IF --->");
            igraceviIzazoviDto.setKorisnickaImena(null);
            return igraceviIzazoviDto;
        }

        logger.info("<--- NAKON IFOVA --->");
        String[] izazivaci = new String[BorbaService.izazoviKorisnika.get(igrac).size()];

        int i = 0;
        for(Korisnik tmp : BorbaService.izazoviKorisnika.get(igrac))
            izazivaci[i++] = tmp.getKorisnickoIme();
//        igraceviIzazoviDto.setKorisnickaImena(new String[BorbaService.izazoviKorisnika.get(igrac).size()]);
//        igraceviIzazoviDto.setKorisnickaImena(BorbaService.izazoviKorisnika.get(igrac).toArray(igraceviIzazoviDto.getKorisnickaImena()));
        igraceviIzazoviDto.setKorisnickaImena(izazivaci);

        return igraceviIzazoviDto;
    }

    @PostMapping("/provjeriIzazove")
    public NoviIzazoviDto provjeraIzazova(@RequestBody ZahtjevIgracevihIzazovaDto zahtjevIgracevihIzazovaDto){
        NoviIzazoviDto noviIzazoviDto = new NoviIzazoviDto();
        Korisnik korisnik = igracRepository.findByKorisnickoImeKorisnik(zahtjevIgracevihIzazovaDto.getKorisnickoIme());

        if(BorbaService.odbijeneBorbe.containsKey(korisnik)){

            noviIzazoviDto.setKod("BATTLE_REJECTED");
            noviIzazoviDto.setKorisnickoIme(BorbaService.odbijeneBorbe.get(korisnik).getKorisnickoIme());
            noviIzazoviDto.setPrihvacenIzazov(false);

            BorbaService.odbijeneBorbe.remove(korisnik);

            return noviIzazoviDto;
        }

        if(BorbaService.pocetakBorbe.containsKey(korisnik) && BorbaService.pocetakBorbe.get(korisnik) != null){

            noviIzazoviDto.setKod("BATTLE_BEGIN");
            noviIzazoviDto.setKorisnickoIme(BorbaService.pocetakBorbe.get(korisnik).getKorisnickoIme());

            if(BorbaService.izazoviKorisnika.containsKey(korisnik))
                BorbaService.izazoviKorisnika.get(korisnik).remove(BorbaService.izazoviKorisnika.get(BorbaService.pocetakBorbe.get(korisnik)));

            BorbaPrepInfoDto borbaPrepInfoDto = new BorbaPrepInfoDto();
            IzazovIgracaDto izazovIgracaDto = BorbaService.podaciIzazova.get(korisnik);

            if(izazovIgracaDto == null)
                izazovIgracaDto = BorbaService.podaciIzazova.get(BorbaService.pocetakBorbe.get(korisnik));

            else{
                int poklapanje = 0;
                if(korisnik.getKorisnickoIme().equals(izazovIgracaDto.getCiljniIgrac()) || korisnik.getKorisnickoIme().equals(izazovIgracaDto.getIzazivac()))
                    poklapanje++;

                String drugiKorisnik = BorbaService.pocetakBorbe.get(korisnik).getKorisnickoIme();
                if(drugiKorisnik.equals(izazovIgracaDto.getCiljniIgrac()) || drugiKorisnik.equals(izazovIgracaDto.getIzazivac()))
                    poklapanje++;

                if(poklapanje != 2)
                    izazovIgracaDto = BorbaService.podaciIzazova.get(BorbaService.pocetakBorbe.get(korisnik));
            }

            borbaPrepInfoDto.setImeBorilista(nadiBoriliste(izazovIgracaDto));
            borbaPrepInfoDto.setTipBorbe(izazovIgracaDto.getTipBorbe());
            if(!korisnik.getKorisnickoIme().equals(izazovIgracaDto.getIzazivac()))
                borbaPrepInfoDto.setProtivnik(izazovIgracaDto.getIzazivac());

            else
                borbaPrepInfoDto.setProtivnik(izazovIgracaDto.getCiljniIgrac());

            BorbaService.pocetakBorbe.remove(korisnik);
            BorbaService.podaciOBorbi.put(korisnik, borbaPrepInfoDto);
            logger.info("<--- !!! PODACI O BORBI U MAPU !!! --->");
            logger.info(borbaPrepInfoDto.toString());
            logger.info("<--- !!! PODACI IZAZOV IGRACA !!! --->");
            logger.info(izazovIgracaDto.toString());
            BorbaService.podaciOBorbi.put(igracRepository.findByKorisnickoImeKorisnik(izazovIgracaDto.getIzazivac()), borbaPrepInfoDto);

            return noviIzazoviDto;
        }

        if(BorbaService.prihvacenIzazov.containsKey(korisnik) && BorbaService.prihvacenIzazov.get(korisnik) != null){
            noviIzazoviDto.setKorisnickoIme(BorbaService.prihvacenIzazov.get(korisnik).getFirst().getKorisnickoIme());
            noviIzazoviDto.setPrihvacenIzazov(BorbaService.prihvacenIzazov.get(korisnik).getSecond());
            noviIzazoviDto.setKod("CH_CHALLENGE");

            //krenula borba
            if(noviIzazoviDto.isPrihvacenIzazov()) {
                IzazovIgracaDto izazovIgracaDto = BorbaService.podaciIzazova.get(korisnik);
                BorbaService borbaService = new BorbaService(korisnik, BorbaService.prihvacenIzazov.get(korisnik).getFirst(), izazovIgracaDto.getTipBorbe(), nazivLokacije(izazovIgracaDto));

                BorbaService.pocetakBorbe.put(korisnik, BorbaService.prihvacenIzazov.get(korisnik).getFirst());
                BorbaService.pocetakBorbe.put(BorbaService.prihvacenIzazov.get(korisnik).getFirst(), korisnik);
                if(BorbaService.borba.get(korisnik) != null){
                    BorbaService.borba.get(korisnik).setKorisnik2(BorbaService.prihvacenIzazov.get(korisnik).getFirst().getKorisnickoIme());
                }else{
                    PodaciOBorbiDto podaciOBorbiDto1 = new PodaciOBorbiDto();
                    podaciOBorbiDto1.setKorisnik1(korisnik.getKorisnickoIme());
                    podaciOBorbiDto1.setKorisnik2(BorbaService.prihvacenIzazov.get(korisnik).getFirst().getKorisnickoIme());
                    BorbaService.borba.put(korisnik, podaciOBorbiDto1);
                }
                if(BorbaService.borba.get(BorbaService.prihvacenIzazov.get(korisnik).getFirst()) != null){
                    BorbaService.borba.get(BorbaService.prihvacenIzazov.get(korisnik).getFirst()).setKorisnik2(korisnik.getKorisnickoIme());
                }else{
                    PodaciOBorbiDto podaciOBorbiDto2 = new PodaciOBorbiDto();
                    podaciOBorbiDto2.setKorisnik1(BorbaService.prihvacenIzazov.get(korisnik).getFirst().getKorisnickoIme());
                    podaciOBorbiDto2.setKorisnik2(korisnik.getKorisnickoIme());
                    BorbaService.borba.put(BorbaService.prihvacenIzazov.get(korisnik).getFirst(), podaciOBorbiDto2);
                }
//                borbaService.run();
            }

            BorbaService.prihvacenIzazov.remove(korisnik);
            return noviIzazoviDto;
        }

        if(BorbaService.redCekanja.containsKey(korisnik) && BorbaService.redCekanja.get(korisnik) != null
                && BorbaService.redCekanja.get(korisnik).peek() != null){
            noviIzazoviDto.setKorisnickoIme(BorbaService.redCekanja.get(korisnik).poll().getKorisnickoIme());
            noviIzazoviDto.setKod("CHALLENGE");
            return noviIzazoviDto;
        }

        noviIzazoviDto.setKod("NULL");
        return noviIzazoviDto;
    }

    @PostMapping("/odgovorNaIzazov")
    public BorbaInfoDto odgovorNaIzazov(@RequestBody OdgovorNaIzazovIgracaDto odgovorNaIzazovIgracaDto){
        BorbaInfoDto borbaInfoDto = new BorbaInfoDto();
        Korisnik izazvani = igracRepository.findByKorisnickoImeKorisnik(odgovorNaIzazovIgracaDto.getCi());
        Korisnik izazivac = igracRepository.findByKorisnickoImeKorisnik(odgovorNaIzazovIgracaDto.getIz());

        logger.info("---- PRIJE IFOVA -----");
        logger.info(odgovorNaIzazovIgracaDto.getCi());
        logger.info(odgovorNaIzazovIgracaDto.getIz());

        if(izazvani == null){
            logger.info("---- PRVI IF ----");
            borbaInfoDto.setPoruka("Challenger does not exist");
            BorbaService.podaciIzazova.remove(izazivac);
            return borbaInfoDto;
        }

        if(izazivac != null || BorbaService.izazoviKorisnika.get(izazvani).contains(izazivac)){
                if(odgovorNaIzazovIgracaDto.isPrihvacen())
                    borbaInfoDto.setPoruka("Challenge accepted");

                else{
                    borbaInfoDto.setPoruka("Challenge rejected");
                    BorbaService.podaciIzazova.remove(izazivac);
                }

                BorbaService.prihvacenIzazov.put(izazivac, new IzracunavanjaService.Pair<>(izazvani, odgovorNaIzazovIgracaDto.isPrihvacen()));
                BorbaService.izazoviKorisnika.get(izazvani).remove(izazivac);

                return borbaInfoDto;

        }

        logger.info("---- NAKON IFOVA -----");
        logger.info(izazivac.toString());
        logger.info(izazvani.toString());

        borbaInfoDto.setPoruka("Challenger does not exist");
        BorbaService.podaciIzazova.remove(izazivac);

        return borbaInfoDto;
    }

    @PostMapping("/ponistiBorbu")
    public PorukaDto ponistiBorbu(@RequestBody KorisniciUBorbiDto korisniciUBorbiDto){
        PorukaDto porukaDto = new PorukaDto();

        logger.info("!!! PONISTENJE BORBE !!!");
        logger.info(korisniciUBorbiDto.getPrviKorisnik());
        logger.info(korisniciUBorbiDto.getDrugiKorisnik());

        BorbaService.odbijeneBorbe.put(igracRepository.findByKorisnickoImeKorisnik(korisniciUBorbiDto.getDrugiKorisnik()),
                igracRepository.findByKorisnickoImeKorisnik(korisniciUBorbiDto.getPrviKorisnik()));

        porukaDto.setPoruka("Borba je uspijesno ponistena");

        return porukaDto;
    }

    @PostMapping("/preparacijeBorba")
    public BorbaPrepInfoDto pripremaBorbe(@RequestBody DetaljneInfoDto detaljneInfoDto){
        Korisnik k = igracRepository.findByKorisnickoImeKorisnik(detaljneInfoDto.getKorisnickoIme());
        logger.info("!!! PREPARACIJA BORBE !!!");
        logger.info(k.toString());

        BorbaPrepInfoDto borbaPrepInfoDto = BorbaService.podaciOBorbi.get(k);
        logger.info("!!!   STO SE SALJE ADAMU   !!!");
        logger.info(borbaPrepInfoDto.toString());

        BorbaService.podaciOBorbi.remove(k);

        return borbaPrepInfoDto;
    }

    @PostMapping("/zapocniBorbu")
    public PorukaDto pokretanjeBorbe(@RequestBody PocetakBorbeDto pocetakBorbeDto){
        PorukaDto porukaDto = new PorukaDto();
        Korisnik korisnik = igracRepository.findByKorisnickoImeKorisnik(pocetakBorbeDto.getKorisnickoIme());
        logger.info("--- KORISNIK: " + pocetakBorbeDto.getKorisnickoIme() + " ---");
        BorbaService.prihvaceneBorbe.put(korisnik, true);
        porukaDto.setPoruka("You are going into battle wait for another player to accept!");

        BorbaService.karteMapa.put(korisnik, pocetakBorbeDto.getOdabraneKarte());
        logger.error("--- STAVLJENE KARTE: " + pocetakBorbeDto.getKorisnickoIme() + " ---");
        PodaciOBorbiDto borbaPodaciDto = new PodaciOBorbiDto();
        if(BorbaService.borba.get(korisnik) != null) {
            BorbaService.borba.get(korisnik).setKartePrvog(pocetakBorbeDto.getOdabraneKarte());
        }else{
            PodaciOBorbiDto podaciOBorbiDto = new PodaciOBorbiDto();
            podaciOBorbiDto.setKorisnik1(korisnik.getKorisnickoIme());
            podaciOBorbiDto.setKartePrvog(pocetakBorbeDto.getOdabraneKarte());
            BorbaService.borba.put(korisnik, podaciOBorbiDto);
        }
        return porukaDto;
    }

    @PostMapping("/borba")
    public BorbaPodaciDto borba(@RequestBody BorbaPodaciDto borbaPodaciDto){
        BorbaPodaciDto borbaPodaciOdgovorDto = new BorbaPodaciDto();
        Korisnik igrac = igracRepository.findByKorisnickoImeKorisnik(borbaPodaciDto.getPosiljatelj());
        logger.info("--- BORBA PODACI: " + borbaPodaciDto.toString() + " ---");
//        if(BorbaService.prihvaceneBorbe.get(igrac)) {
//            BorbaService.prihvaceneBorbe.remove(igrac);
//
//            borbaPodaciOdgovorDto.setKod("WAIT");
//            return borbaPodaciOdgovorDto;
//        }
//
//        borbaPodaciOdgovorDto = BorbaService.borba.get(igrac);
//        //salje se kao posiljatelj tko igra prvi
//        if(borbaPodaciOdgovorDto != null) {
//            if (borbaPodaciOdgovorDto.getKod().equals("BEGIN")) {
//                BorbaService.borba.remove(igrac);
//                return borbaPodaciOdgovorDto;
//            }
//        } else {
//            BorbaService.borba.put(igracRepository.findByKorisnickoImeKorisnik(borbaPodaciDto.getPosiljatelj()), borbaPodaciDto);
//
//        }

        if(borbaPodaciDto.getKod().equals("INFO")){
            logger.info("--- USAO U INFO ---");
            if(BorbaService.karteMapa.get(igrac) != null){
                logger.info("--- STAVIO WAIT ---");
                borbaPodaciOdgovorDto.setKod("WAIT");
                BorbaService.karteMapa.remove(igrac);
                return borbaPodaciOdgovorDto;
            }
        }



        return borbaPodaciOdgovorDto;
    }

    private String nazivLokacije(IzazovIgracaDto izazovIgracaDto){
        String naziv;
        int id = izazovIgracaDto.getTipBorbe();

        switch(id){
            case 0 : naziv = izazovIgracaDto.getGrad();
                break;
            case 1 : naziv = izazovIgracaDto.getDrzava();
                break;
            case 2 : naziv = LocationController.pretvoriKontinent(izazovIgracaDto.getKontinent());
                break;
            case 3: naziv = "World";
                break;
            default: naziv = null;
        }

        return naziv;
    }

    private String nadiBoriliste(IzazovIgracaDto izazovIgracaDto){
        if(izazovIgracaDto.getTipBorbe() == 0)
            return izazovIgracaDto.getGrad();

        if(izazovIgracaDto.getTipBorbe() == 1)
            return izazovIgracaDto.getDrzava();

        long kontinent  = izazovIgracaDto.getKontinent();
        if(izazovIgracaDto.getTipBorbe() == 2)
            return LocationController.pretvoriKontinent(kontinent);

        return "World";
    }

    private int nadiTipBorilista(IzazovIgracaDto izazovIgracaDto){
        if(izazovIgracaDto.getGrad() != null && !izazovIgracaDto.getGrad().isEmpty())
            return 0;

        if(izazovIgracaDto.getDrzava() != null && !izazovIgracaDto.getDrzava().isEmpty())
            return 1;

        long kontinent  = izazovIgracaDto.getKontinent();
        if(kontinent >= 0 && kontinent <= 6)
            return 2;

        return 3;

    }
}
