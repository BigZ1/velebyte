package hr.fer.Geofighter.web.rest.controller;

import hr.fer.Geofighter.GeoFigtherApplication;
import hr.fer.Geofighter.domain.Korisnik;
import hr.fer.Geofighter.repository.BorbaRepository;
import hr.fer.Geofighter.repository.IgracRepository;
import hr.fer.Geofighter.service.BazaPodatakaLoaderSerivce;
import hr.fer.Geofighter.service.BorbaService;
import hr.fer.Geofighter.service.GeoService;
import hr.fer.Geofighter.service.IzracunavanjaService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import hr.fer.Geofighter.web.rest.dto.OdgovorNaOdjavuDto;
import hr.fer.Geofighter.web.rest.dto.OdjavaDto;

import lombok.Data;
import lombok.RequiredArgsConstructor;


@Data
@RestController
@RequestMapping("/geofighter")
@RequiredArgsConstructor
public class OdjavaController {

	private static final Logger logger = LoggerFactory.getLogger(String.valueOf(GeoFigtherApplication.class));

	@Autowired
	private BazaPodatakaLoaderSerivce bazaPodatakaLoaderSerivce;

	@Autowired
	private IgracRepository igracRepository;

	@Autowired
	private BorbaRepository borbaRepository;


	//TODO: za pocetak ostavi samo kao getMapping inace stavi validaciju da samo to admin moze napraviti
	@GetMapping("/odjaviSve")
	public String odjaviSveKorisnike(){
		PrijavaController.ulogiraniKorisnici.clear();
		return "Svi korisnici su odlogirani";
	}

	@GetMapping("/izbrisiSve")
	public String izbrisiSve(){
		borbaRepository.deleteAll();
		igracRepository.deleteAll();
		bazaPodatakaLoaderSerivce.dodajSveKorisnike();
		PrijavaController.nepotvrdeniKartografi.clear();
		PrijavaController.ulogiraniKorisnici.clear();
		return "Izbrisani su svi korisnici u bazi";
	}

	@SuppressWarnings("unlikely-arg-type")
	@PostMapping("/odjava")
	public OdgovorNaOdjavuDto odjaviKorisnika(@RequestBody OdjavaDto odjavaDto) {
		OdgovorNaOdjavuDto odg = new OdgovorNaOdjavuDto();

		//makni tog ulogiranog korisnika
		for(int i = 0; i < PrijavaController.ulogiraniKorisnici.size(); i++){
			if(odjavaDto.getKorisnickoIme().equals(PrijavaController.ulogiraniKorisnici.get(i).getKorisnik().getKorisnickoIme())){

				BorbaService.prihvacenIzazov.remove(PrijavaController.ulogiraniKorisnici.get(i).getKorisnik());
				BorbaService.redCekanja.remove(PrijavaController.ulogiraniKorisnici.get(i).getKorisnik());

				if(BorbaService.izazoviKorisnika.containsKey(PrijavaController.ulogiraniKorisnici.get(i).getKorisnik()) &&
						BorbaService.izazoviKorisnika.get(PrijavaController.ulogiraniKorisnici.get(i).getKorisnik()) != null){

					for(Korisnik korisnik : BorbaService.izazoviKorisnika.get(PrijavaController.ulogiraniKorisnici.get(i).getKorisnik())){
						BorbaService.podaciIzazova.remove(korisnik);
						BorbaService.prihvacenIzazov.put(korisnik, new IzracunavanjaService.Pair<>(PrijavaController.ulogiraniKorisnici.get(i).getKorisnik(), false));
					}
				}

				BorbaService.izazoviKorisnika.remove(PrijavaController.ulogiraniKorisnici.get(i).getKorisnik());

				for(Korisnik korisnik : BorbaService.izazoviKorisnika.keySet()){
					BorbaService.izazoviKorisnika.get(korisnik).remove(PrijavaController.ulogiraniKorisnici.get(i).getKorisnik());
				}

				PrijavaController.ulogiraniKorisnici.remove(i);

				odg.setKorisnickoIme(odjavaDto.getKorisnickoIme());
				return odg;
			}
		}

		odg.setKorisnickoIme("");

		return odg;
	}


}
