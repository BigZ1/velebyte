package hr.fer.Geofighter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EnableJpaRepositories
public class GeoFigtherApplication {

	public static void main(String[] args) {
		SpringApplication.run(GeoFigtherApplication.class, args);
	}

}
