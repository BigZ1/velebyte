package hr.fer.Geofighter.service;

import org.springframework.stereotype.Service;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.*;
import java.util.stream.Collectors;
@Service
public class IzracunavanjaService {

    public Double getPovrsina(HashSet<Vector2D> koord){
        if(koord==null){
            return 0.0;
        }
        ConvexHull ch = new ConvexHull();
        HashSet<Vector2D> poligon = ch.getHull(koord);
        List<Vector2D> lista = new ArrayList<>(poligon);
        ch.sortListCounterClockwize(lista);
        Double povrsina = new Double(0.0);
        for(int i = 2; i < lista.size(); i++){
            Double a = distance(lista.get(0),lista.get(i-1));
            Double b = distance(lista.get(i-1),lista.get(i));
            Double c = distance(lista.get(i),lista.get(0));
            Double s = (a+b+c)/2;
            povrsina += Math.sqrt(s*(s-a)*(s-b)*(s-c));
        }
        return povrsina;
    }

    public HashSet<Vector2D> pretvoriKoordUMetre(List<Double> koordinate){
        HashSet<Vector2D> koord = new HashSet<>();
        for(int i = 0; i < koordinate.size(); i = i + 2){
            Vector2D list = pretvorba(koordinate.get(i), koordinate.get(i+1));
            koord.add(list);
        }

        return koord;
    }

    public Double distance(Vector2D a, Vector2D b){
        return Math.sqrt((a.x-b.x)*(a.x- b.x)+(a.y-b.y)*(a.y-b.y));
    }

    private Vector2D pretvorba(Double lng, Double lat){
        double Easting;
        double Northing;
        int Zone;
        Zone= (int) Math.floor(lng/6+31);
        Easting=0.5*Math.log((1+Math.cos(lat*Math.PI/180)*Math.sin(lng*Math.PI/180-(6*Zone-183)*Math.PI/180))
                /(1-Math.cos(lat*Math.PI/180)*Math.sin(lng*Math.PI/180-(6*Zone-183)*Math.PI/180)))
                *0.9996*6399593.62/Math.pow((1+Math.pow(0.0820944379, 2)*Math.pow(Math.cos(lat*Math.PI/180), 2)), 0.5)
                *(1+ Math.pow(0.0820944379,2)/2*Math.pow((0.5*Math.log((1+Math.cos(lat*Math.PI/180)
                *Math.sin(lng*Math.PI/180-(6*Zone-183)*Math.PI/180))/(1-Math.cos(lat*Math.PI/180)*Math.sin(lng*Math.PI/180-(6*Zone-183)
                *Math.PI/180)))),2)*Math.pow(Math.cos(lat*Math.PI/180),2)/3)+500000;
        Easting=Math.round(Easting*100)*0.01;
        Northing = (Math.atan(Math.tan(lat*Math.PI/180)/Math.cos((lng*Math.PI/180-(6*Zone -183)*Math.PI/180)))-lat*Math.PI/180)
                *0.9996*6399593.625/Math.sqrt(1+0.006739496742*Math.pow(Math.cos(lat*Math.PI/180),2))
                *(1+0.006739496742/2*Math.pow(0.5*Math.log((1+Math.cos(lat*Math.PI/180)*Math.sin((lng*Math.PI/180-(6*Zone -183)
                *Math.PI/180)))/(1-Math.cos(lat*Math.PI/180)*Math.sin((lng*Math.PI/180-(6*Zone -183)*Math.PI/180)))),2)
                *Math.pow(Math.cos(lat*Math.PI/180),2))+0.9996*6399593.625*(lat*Math.PI/180-0.005054622556
                *(lat*Math.PI/180+Math.sin(2*lat*Math.PI/180)/2)+4.258201531e-05*(3*(lat*Math.PI/180+Math.sin(2*lat*Math.PI/180)/2)
                +Math.sin(2*lat*Math.PI/180)*Math.pow(Math.cos(lat*Math.PI/180),2))/4-1.674057895e-07*(5*(3*(lat*Math.PI/180+Math.sin(2
                *lat*Math.PI/180)/2)+Math.sin(2*lat*Math.PI/180)* Math.pow(Math.cos(lat*Math.PI/180),2))/4+Math.sin(2*lat*Math.PI/180)*
                Math.pow(Math.cos(lat*Math.PI/180),2) *Math.pow(Math.cos(lat*Math.PI/180),2))/3);
        if(lat<0)
            Northing = Northing + 10000000;
        Northing=Math.round(Northing*100)*0.01;
        Vector2D lista = new Vector2D(Easting, Northing);
        return lista;
    }

    public static class Pair<F, S> {
        private F first; //first member of pair
        private S second; //second member of pair

        public Pair() {
            this.first = null;
            this.second = null;
        }

        public Pair(F first, S second) {
            this.first = first;
            this.second = second;
        }

        public void setFirst(F first) {
            this.first = first;
        }

        public void setSecond(S second) {
            this.second = second;
        }

        public F getFirst() {
            return first;
        }

        public S getSecond() {
            return second;
        }
    }

    private class ConvexHull {
        public List<Vector2D> verts;
        public HashSet<Vector2D> getHull(HashSet<Vector2D> vs) {

            if (vs.size() < 3)
                return null;

            HashSet<Vector2D> hull = new HashSet<Vector2D>();

            //find most left (x-) vertex and most right (x+) vertex
            Vector2D A = new Vector2D(Double.MAX_VALUE, Double.MAX_VALUE), B = new Vector2D(-Double.MAX_VALUE, -Double.MAX_VALUE);

            for (Vector2D v : vs) {
                if (A.x > v.getX()) {
                    A = v;
                }
                if (B.x < v.getX()) {
                    B = v;
                }
            }

        /*//Either all points are the same or are in line
        if (A == B)
            return null;*/

            //A and B are surely in hull
            hull.add(A);
            hull.add(B);

            //intersect set vs with A - B line
            HashSet<Vector2D> vs2;

            Pair<HashSet<Vector2D>, HashSet<Vector2D>> pair;
            pair = splitSetWithLine(vs, A, B);
            vs = pair.getFirst();
            vs2 = pair.getSecond();
            //(vs, vs2) = splitSetWithLine(vs, A, B);

            findHull(vs, A, B, hull);
            findHull(vs2, B, A, hull);

            return hull;
        }

        Double findSide(Vector2D p1, Vector2D p2, Vector2D p)
        {
            Double val = (p.y - p1.y) * (p2.x - p1.x) -
                    (p2.y - p1.y) * (p.x - p1.x);

            return val;
        }

        //Splits given set into two between points p and q
        Pair<HashSet<Vector2D>, HashSet<Vector2D>> splitSetWithLine(HashSet<Vector2D> s, Vector2D p, Vector2D q)
        {
            HashSet<Vector2D> s1 = new HashSet<Vector2D>(), s2 = new HashSet<Vector2D>();
            for(Vector2D v : s)
            {
                if (findSide(p, q, v) > 0.0)
                {
                    s1.add(v);
                } else
                {
                    s2.add(v);
                }
            }
            Pair<HashSet<Vector2D>, HashSet<Vector2D>> pair = new Pair<>(s1, s2);
            return pair;
            //return (s1, s2);
        }

        Double pointLineDistance(Vector2D p1, Vector2D p2, Vector2D p)
        {
            if (p1.equals(p2))
                return 0.0;
            return Math.abs((p2.y - p1.y) * p.x - (p2.x - p1.x) * p.y + p2.x * p1.y - p2.y * p1.x) / (Math.sqrt((p2.y - p1.y) * (p2.y - p1.y) - (p2.x) * (p1.x)));
        }

        void findHull(HashSet<Vector2D> s, Vector2D p, Vector2D q, HashSet<Vector2D> hull)
        {
            if (s.size() < 1)
                return;

            Pair<Vector2D, Double> pd = new Pair<>(new Vector2D(0,0), 0.0);
            //(Vector2D p, Double dist) pd = (Vector2D.zero, 0.0);

            for (Vector2D v : s)
            {
                pd = new Pair<>(v, pointLineDistance(p, q, v));
                //pd = (v, pointLineDistance(p, q, v));
                break;
            }

            for(Vector2D v : s)
            {
                Double d = pointLineDistance(p, q, v);
                if (pd.getSecond() < d)
                {
                    pd = new Pair<>(v, d);
                }
            }

            hull.add(pd.getFirst());

            //HashSet<Vector2D> s0, s1, s2;

            Pair<HashSet<Vector2D>, HashSet<Vector2D>> s10 = splitSetWithLine(s, p, pd.getFirst());
            Pair<HashSet<Vector2D>, HashSet<Vector2D>> s20 = splitSetWithLine(s, pd.getFirst(), q);
            //(s1, s0) = splitSetWithLine(s, p, pd.p);
            //(s2, s0) = splitSetWithLine(s, pd.p, q);

            findHull(s10.getFirst(), p, pd.getFirst(), hull);
            findHull(s20.getFirst(), pd.getFirst(), q, hull);

        }

        public void sortListCounterClockwize(List<Vector2D> vs)
        {
            if(vs != null)
            {

                Vector2D center = new Vector2D(0,0);

                for(int i = 0; i < vs.size(); i++)
                {
                    center.add(vs.get(i));
                }

                center.scale(1.0/ vs.size());
                //center /= vs.Count;

                vs.sort(new Comparator<Vector2D>() {
                    @Override
                    public int compare(Vector2D a, Vector2D b) {
                        return (int)-Math.ceil((Math.toDegrees(Math.atan2(a.x - center.x, a.y - center.y)) - Math.toDegrees(Math.atan2(b.x - center.x, b.y - center.y))));

                    }
                });
            }
        }


        }

    public static class Vector2D {

        private double x;
        private double y;

        public Vector2D(double x, double y) {
            this.x = x;
            this.y = y;
        }

        public double getX() {
            return this.x;
        }

        public double getY() {
            return this.y;
        }

        public void add(Vector2D offset) {
            if(offset == null) throw new NullPointerException();

            this.x += offset.x;
            this.y += offset.y;
        }

        public Vector2D added(Vector2D offset) {
            if(offset == null) throw new NullPointerException();

            Vector2D tmp = copy();
            tmp.add(offset);

            return tmp;
        }

        public void rotate(double angle) {
            double x2 = Math.cos(angle) * this.x - Math.sin(angle) * this.y;
            double y2 = Math.sin(angle) * this.x + Math.cos(angle) * this.y;

            this.x = x2;
            this.y = y2;
        }

        public Vector2D rotated(double angle) {
            Vector2D tmp = copy();
            tmp.rotate(angle);

            return tmp;
        }

        public void scale(double scaler) {
            this.x *= scaler;
            this.y *= scaler;
        }

        public Vector2D scaled(double scaler) {
            Vector2D tmp = copy();
            tmp.scale(scaler);

            return tmp;
        }

        public Vector2D copy() {
            return new Vector2D(this.x, this.y);
        }

        @Override
        public String toString() {
            DecimalFormatSymbols otherSymbols = new DecimalFormatSymbols(Locale.ENGLISH);
            otherSymbols.setDecimalSeparator('.');
            otherSymbols.setGroupingSeparator(',');
            DecimalFormat df = new DecimalFormat("#.########", otherSymbols);

            return "x: " + df.format(this.x) + ", y: " + df.format(this.y);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            Vector2D vector2D = (Vector2D) o;
            return Double.compare(vector2D.x, x) == 0 && Double.compare(vector2D.y, y) == 0;
        }

        @Override
        public int hashCode() {
            return Objects.hash(x, y);
        }
    }
}
