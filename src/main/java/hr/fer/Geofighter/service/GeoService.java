package hr.fer.Geofighter.service;

import hr.fer.Geofighter.domain.Korisnik;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

public class GeoService {

    private Korisnik korisnik;
    private double geoCordLng;
    private double geoCordLat;

    public GeoService(Korisnik korisnik, double geoCordLng, double geoCordLat){
        this.korisnik = korisnik;
        this.geoCordLng = geoCordLng;
        this.geoCordLat = geoCordLat;
    }

    public Korisnik getKorisnik() {
        return korisnik;
    }

    public void setKorisnik(Korisnik korisnik) {
        this.korisnik = korisnik;
    }

    public double getGeoCordLng() {
        return geoCordLng;
    }

    public void setGeoCordLng(double geoCordLng) {
        this.geoCordLng = geoCordLng;
    }

    public double getGeoCordLat() {
        return geoCordLat;
    }

    public void setGeoCordLat(double geoCordLat) {
        this.geoCordLat = geoCordLat;
    }

    @Override
    public String toString() {
        return "GeoService{" +
                "korisnik=" + korisnik +
                ", geoCordLng=" + geoCordLng +
                ", geoCordLat=" + geoCordLat +
                '}';
    }
}
