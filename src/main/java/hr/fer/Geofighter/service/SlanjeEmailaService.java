package hr.fer.Geofighter.service;

import hr.fer.Geofighter.GeoFigtherApplication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

@Component
//@Service
public class SlanjeEmailaService {
	
//	@Value("${server.developer.address}")
	public String pocetniURLServera = "geofighterserverreal.herokuapp.com/";

//	@Value("${server.servlet.context-path}")
	public String putanjaKonteksta = "geofighter";
	
	@Autowired
	private JavaMailSender posiljateljEmaila;

	private static final Logger logger = LoggerFactory.getLogger(GeoFigtherApplication.class);

	public SlanjeEmailaService(JavaMailSender posiljateljEmaila) {
		this.posiljateljEmaila = posiljateljEmaila;
	}
		
	public void posaljiPotvrduRegistracije(String primatelj, int token, String korisnickoIme) {
		
		SimpleMailMessage poruka = new SimpleMailMessage();

		poruka.setTo(primatelj);
		poruka.setSubject("Molimo potvrdu ovog racuna");
		poruka.setText("Kako biste potvrdili registraciju pritisnite "
				+ "link -> " + pocetniURLServera + putanjaKonteksta + "/registracija/verifikacija?token="
				+ token + "&korisnickoIme=" + korisnickoIme);
		
//		poruka.setText("proslo");

		posiljateljEmaila.send(poruka);
	}
	
}
