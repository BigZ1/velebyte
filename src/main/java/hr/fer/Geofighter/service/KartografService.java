package hr.fer.Geofighter.service;

import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import hr.fer.Geofighter.GeoFigtherApplication;
import hr.fer.Geofighter.repository.IgracRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Service;

import hr.fer.Geofighter.domain.Korisnik;
import hr.fer.Geofighter.domain.Uloga;
import hr.fer.Geofighter.web.rest.dto.RegistracijaKartografaDto;

//@AllArgsConstructor
@Service
@EnableJpaRepositories
public class KartografService extends IgracService{

	@Autowired
	private IgracRepository igracRepository;

	private final Logger logger = LoggerFactory.getLogger(GeoFigtherApplication.class);

	public KartografService() {
		super();
	}

	public String dodajKartografa(RegistracijaKartografaDto registracijaKartografaDto) {
		if(postojiEmail(registracijaKartografaDto.getEmail()))
			return "Postoji email";
		
		if(postojiKorisnik(registracijaKartografaDto.getKorisnickoIme()))
			return "Postoji korisnicko ime";

		Korisnik k = new Korisnik();
		k.setKorisnickoIme(registracijaKartografaDto.getKorisnickoIme());
		k.setPassword(registracijaKartografaDto.getSazetakLozinke());
		k.setEmail(registracijaKartografaDto.getEmail());
		k.setUrlFotografije(BazaPodatakaLoaderSerivce.Base64ToBytes(registracijaKartografaDto.getKorisnickaSlika()));
		k.setSlikaOsobne(BazaPodatakaLoaderSerivce.Base64ToBytes(registracijaKartografaDto.getSlikaOsobneIskaznice()));
		k.setIBAN(registracijaKartografaDto.getIBAN());
		k.setBanned(false);
		k.setELO(0);
		k.setExparation(null);
		k.setIDKorisnika(igracRepository.findAll().size() + 1);
		k.setUlogaKorisnika(1);
		k.setVerified(false);
		k.setKarteKorisnika(null);
		k.setMultiplierKarte(null);


		igracRepository.save(k);

		return null;
	}

}
