package hr.fer.Geofighter.service;

import java.util.List;

import hr.fer.Geofighter.GeoFigtherApplication;
import hr.fer.Geofighter.web.rest.controller.PrijavaController;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.stereotype.Service;

import hr.fer.Geofighter.domain.Korisnik;
import hr.fer.Geofighter.domain.Uloga;
import hr.fer.Geofighter.repository.IgracRepository;
import hr.fer.Geofighter.web.rest.dto.RegistracijaIgracaDto;
import lombok.AllArgsConstructor;

//@AllArgsConstructor
@Service
@EnableJpaRepositories
public class IgracService {

	@Autowired
	private SlanjeEmailaService slanjeEmaila;

	@Autowired
	private IgracRepository igracRepository;
	private static final Logger logger = LoggerFactory.getLogger(GeoFigtherApplication.class);

	public IgracService() {}

	public boolean postojiKorisnik(String korisnickoIme) {
		List<Korisnik> korisnici = igracRepository.findByKorisnickoIme(korisnickoIme);

		if(korisnici != null)
			return !korisnici.isEmpty();

		return false;
	}
	
	public boolean postojiEmail(String email) {
		List<Korisnik> korisnici = igracRepository.findByEmail(email);

		if(korisnici != null)
			return !korisnici.isEmpty();

		return false;
	}
	
	public boolean verificiraniKorisnik(String korisnickoIme) {
		
		List<Korisnik> k = igracRepository.findByKorisnickoIme(korisnickoIme);
		
		if(!k.isEmpty())
			return k.get(0).isVerified();
			
		return false;
	}
	
	public boolean bananiKorisnik(String korisnickoIme) {
		List<Korisnik> k = igracRepository.findByKorisnickoIme(korisnickoIme);
		
		if(!k.isEmpty())
			return k.get(0).isBanned();
		
		return false;
	}
	
	public void posaljiMail(String email, String korisnickoIme) {
		slanjeEmaila.posaljiPotvrduRegistracije(email, korisnickoIme.hashCode(), korisnickoIme);
	}
	
	public boolean potvrdiEmail(String korisnickoIme, String token) {
		//vec sam pretopostavio da je korisnik dodan u bazu tako da maknuti to i tek kada se potvrdi
		//email staviti ga u bazu ovdje
		Korisnik k = igracRepository.findByKorisnickoImeKorisnik(korisnickoIme);

		if(korisnickoIme.hashCode() == Integer.parseInt(token)){
			logger.info("!!!PRIJE UPDATE VERIFIED");
			igracRepository.updateVerified(korisnickoIme);
			PrijavaController.nepotvrdeniKartografi.add(k);
			logger.info("!!!POSLIJE UPDATE VERIFIED");
			return true;
		}

		return false;
	}
	
	public String dodajIgraca(RegistracijaIgracaDto registracijaIgracaDto) {
		if(postojiKorisnik(registracijaIgracaDto.getKorisnickoIme()))
			return "Postoji korisnicko ime";

		if(postojiEmail(registracijaIgracaDto.getEmail()))
			return "Postoji email";

		Korisnik k = new Korisnik();
		k.setKorisnickoIme(registracijaIgracaDto.getKorisnickoIme());
		k.setPassword(registracijaIgracaDto.getSazetakLozinke());
		k.setEmail(registracijaIgracaDto.getEmail());
		k.setUrlFotografije(BazaPodatakaLoaderSerivce.Base64ToBytes(registracijaIgracaDto.getKorisnickaSlika()));
		k.setBanned(false);
		k.setELO(100);
		k.setExparation(null);
		k.setIDKorisnika(igracRepository.findAll().size() + 1);
		k.setUlogaKorisnika(0);
		k.setVerified(false);
		k.setKarteKorisnika(null);
		k.setMultiplierKarte(null);

		igracRepository.save(k);

		return null;
	}


	
}
