package hr.fer.Geofighter.service;

import hr.fer.Geofighter.GeoFigtherApplication;
import hr.fer.Geofighter.domain.Karta;
import hr.fer.Geofighter.domain.Korisnik;
import hr.fer.Geofighter.web.rest.dto.BorbaPodaciDto;
import hr.fer.Geofighter.web.rest.dto.BorbaPrepInfoDto;
import hr.fer.Geofighter.web.rest.dto.IzazovIgracaDto;
import hr.fer.Geofighter.web.rest.dto.PodaciOBorbiDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

public class BorbaService extends Thread {

    private Korisnik prviKorisnik;
    private Korisnik drugiKorisnik;
    private List<Karta> kartePrvog; //grad = 8, drzava = 12, kontinent = 20, svijet = 32 TODO: PROVJERI
    private List<Karta> karteDrugog;
    //0 -> grad, 1 -> drzava, 2 -> kontitent, 3 -> svijet
    private int lokacija;
    private String nazivLokacije;  // npr ime grada, ime kontinenta, ...

    private static final Logger logger = LoggerFactory.getLogger(GeoFigtherApplication.class);

    volatile boolean pocetakBorbeOdabir = true;

    public static HashMap<Korisnik, Boolean> prihvaceneBorbe = new HashMap<>();
    public static HashMap<Korisnik, Korisnik> odbijeneBorbe = new HashMap<>();
    public static HashMap<Korisnik, BorbaPrepInfoDto> podaciOBorbi = new HashMap<>();
    public static HashMap<Korisnik, HashSet<Korisnik>> izazoviKorisnika = new HashMap<>();
    public static HashMap<Korisnik, Queue<Korisnik>> redCekanja = new HashMap<>();
    public static HashMap<Korisnik, IzracunavanjaService.Pair<Korisnik, Boolean>> prihvacenIzazov = new HashMap<>();      //prvi je izazivac, drugi je izazvani
    public static HashMap<Korisnik, Korisnik> pocetakBorbe = new HashMap<>();
    public static HashMap<Korisnik, IzazovIgracaDto> podaciIzazova = new HashMap<>();
    public static HashMap<Korisnik, Long[]> karteMapa = new HashMap<>();
    public static HashMap<Korisnik, PodaciOBorbiDto> borba = new HashMap<>();

    public BorbaService(Korisnik prviKorisnik, Korisnik drugiKorisnik, int lokacija, String nazivLokacije){
        this.prviKorisnik = prviKorisnik;
        this.drugiKorisnik = drugiKorisnik;
        this.kartePrvog = null;
        this.karteDrugog = null;
        this.lokacija = lokacija;
        this.nazivLokacije = nazivLokacije;
    }

    public Korisnik getPrviKorisnik() { return prviKorisnik; }

    public void setPrviKorisnik(Korisnik prviKorisnik) { this.prviKorisnik = prviKorisnik; }

    public Korisnik getDrugiKorisnik() { return drugiKorisnik; }

    public void setDrugiKorisnik(Korisnik drugiKorisnik) { this.drugiKorisnik = drugiKorisnik; }

    public List<Karta> getKartePrvog() { return kartePrvog; }

    public void setKartePrvog(List<Karta> kartePrvog) { this.kartePrvog = kartePrvog; }

    public List<Karta> getKarteDrugog() { return karteDrugog; }

    public void setKarteDrugog(List<Karta> karteDrugog) { this.karteDrugog = karteDrugog; }

    /*@Override
    public void run(){
        //TODO: Borba -> po sekvencijskom dijagramu
        //TODO: Kraj brobe u mape za poruke staviti rezultate borbe i spremiti rezultate brobe u bazu (elo se dodaje pomocu funckije u BorbaService)

        while(true){
            try {
                //ako je true i true tj. oba su prihvatili borbu
                if(prihvaceneBorbe.get(prviKorisnik) && prihvaceneBorbe.get(drugiKorisnik))
                    break;

                Thread.sleep(1000);
            } catch (InterruptedException e) {
                logger.error("!!! INTERRUPT ERROR U DRETVI !!!");
            }
        }

        if(pocetakBorbeOdabir){
            Random random = new Random();
            int tkoPrviIgra = random.nextInt(1);
            BorbaPodaciDto borbaPodaciDto = new BorbaPodaciDto();

            borbaPodaciDto.setKod("BEGIN");
            borbaPodaciDto.setPosiljatelj(tkoPrviIgra == 0 ? prviKorisnik.getKorisnickoIme() : drugiKorisnik.getKorisnickoIme());

            borba.put(tkoPrviIgra == 0 ? prviKorisnik : drugiKorisnik, borbaPodaciDto);
            pocetakBorbeOdabir = false;
        }

        while(true){
            try {
                int count = 75;
                while(count > 0) {
                    count--;
                    Thread.sleep(1000);
                }


            } catch (InterruptedException e) {
                logger.error("!!! INTERRUPT ERROR U DRETVI !!!");
            }
        }

    }*/
}
