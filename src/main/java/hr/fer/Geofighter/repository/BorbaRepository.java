package hr.fer.Geofighter.repository;

import hr.fer.Geofighter.domain.Borba;
import hr.fer.Geofighter.domain.Drzava;
import hr.fer.Geofighter.domain.Korisnik;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BorbaRepository extends JpaRepository<Borba, Long> {

    @Query("SELECT b FROM Borba b WHERE b.pobjednik = ?1")
    List<Borba> findAllByPobjednik(long pobjednikID);

    @Query("SELECT b FROM Borba b WHERE b.prviKorisnik = ?1 OR b.drugiKorisnik = ?2")
    List<Borba> findAllByIdSveBorbe(long igracID, long igracID2);

}
