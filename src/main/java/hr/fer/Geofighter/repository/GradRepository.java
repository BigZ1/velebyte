package hr.fer.Geofighter.repository;

import hr.fer.Geofighter.domain.Grad;
import hr.fer.Geofighter.domain.Kontinent;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface GradRepository extends JpaRepository<Grad, Long> {
    @Query("SELECT u FROM Grad u WHERE u.imeGrada = ?1")
    Grad findByIdImeGrada(String imeGrada);
}
