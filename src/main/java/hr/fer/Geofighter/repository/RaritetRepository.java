package hr.fer.Geofighter.repository;

import hr.fer.Geofighter.domain.Kontinent;
import hr.fer.Geofighter.domain.Korisnik;
import hr.fer.Geofighter.domain.Raritet;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface RaritetRepository extends JpaRepository<Raritet, Long> {
    @Query("SELECT u FROM Raritet u WHERE u.IDRariteta = ?1")
    Raritet findByIDRariteta(long id);
}
