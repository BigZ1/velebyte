package hr.fer.Geofighter.repository;

import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import hr.fer.Geofighter.domain.Uloga;
import org.hibernate.query.NativeQuery;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Repository;

import hr.fer.Geofighter.domain.Korisnik;

import javax.persistence.Column;
import javax.transaction.Transactional;

@Repository
public interface IgracRepository extends JpaRepository<Korisnik, Long> {
	
	//trazimo jednog korisnika ali kako bismo mogli pozvati metode nad
	//Listama stavljamo jednog korisnika u listu
	@Query("SELECT u FROM Korisnik u WHERE u.korisnickoIme = ?1")
	List<Korisnik> findByKorisnickoIme(String korisnickoIme);

	@Query("SELECT u FROM Korisnik u WHERE u.email= ?1")
	List<Korisnik> findByEmail(String email);

	@Query("SELECT u FROM Korisnik u WHERE u.IDKorisnika = ?1")
	Korisnik findByIDKorisnika(Long ID);

	@Query("SELECT u FROM Korisnik u WHERE u.korisnickoIme = ?1")
	Korisnik findByKorisnickoImeKorisnik(String korisnickoIme);

	@Query("SELECT u FROM Korisnik u WHERE u.ulogaKorisnika = ?1")
	List<Korisnik> findAllByUloga(long uloga);

	@Modifying
	@Transactional
	@Query("UPDATE Korisnik u SET u.verified = true WHERE u.korisnickoIme = ?1")
	void updateVerified(String korisnickoIme);

	@Modifying
	@Transactional
	@Query("UPDATE Korisnik u SET u.karteKorisnika = ?1 WHERE u.korisnickoIme = ?2")
	void updateKolekcija(Long[] karteKorisnika, String korisnickoIme);

	@Modifying
	@Transactional
	@Query("UPDATE Korisnik u SET u.multiplierKarte = ?1 WHERE u.korisnickoIme = ?2")
	void updateMultipliers(Double[] multiplierKarte, String korisnickoIme);

	@Modifying
	@Transactional
	@Query("UPDATE Korisnik u SET u.korisnickoIme = ?1, u.email = ?3, u.ulogaKorisnika = ?4, u.ELO = ?5, u.IBAN = ?6, u.urlFotografije = ?7," +
			"u.slikaOsobne = ?8, u.verified = ?9 WHERE u.korisnickoIme = ?2")
	void updateKorisnik(String korisnickoIme, String originaloKorisnikoIme, String email, long ulogaKorisnika, int ELO, String IBAN, byte[] urlFotografije,
						byte[] slikaOsobne, boolean verified);

	@Modifying
	@Transactional
	@Query("UPDATE Korisnik u SET u.ELO = ?2 WHERE u.korisnickoIme = ?1")
	void updateELO(String korisnickoIme, long ELO);

	@Query(nativeQuery = true, value = "SELECT * FROM Korisnik u WHERE u.uloga_korisnika <> 1 ORDER BY u.ELO DESC LIMIT 10")
	List<Korisnik> dohvatiTop10Igraca();
}
