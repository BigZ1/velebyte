package hr.fer.Geofighter.repository;

import hr.fer.Geofighter.domain.Raritet;
import hr.fer.Geofighter.domain.Tip;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface TipRepository extends JpaRepository<Tip, Long> {
    @Query("SELECT u FROM Tip u WHERE u.IDTip = ?1")
    Tip findByIDTip(long id);
}
