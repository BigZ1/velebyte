package hr.fer.Geofighter.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import hr.fer.Geofighter.domain.Korisnik;
import hr.fer.Geofighter.domain.Uloga;

@Repository
public interface KartografRepository extends IgracRepository{

//	@Query("SELECT u FROM Korisnik u WHERE u.uloga = ?1")
	List<Korisnik> findAllByUloga(Uloga uloga);

}
