package hr.fer.Geofighter.repository;

import hr.fer.Geofighter.domain.Kontinent;
import hr.fer.Geofighter.domain.Korisnik;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface KontinentRepository extends JpaRepository<Kontinent, Long> {
    @Query("SELECT u FROM Kontinent u WHERE u.IDKontinenta = ?1")
    Kontinent findByIdKontinent(long id);
}
