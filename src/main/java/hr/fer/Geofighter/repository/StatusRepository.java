package hr.fer.Geofighter.repository;

import hr.fer.Geofighter.domain.Kontinent;
import hr.fer.Geofighter.domain.Status;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface StatusRepository extends JpaRepository<Status, Long> {
    @Query("SELECT u FROM Status u WHERE u.IDStatus = ?1")
    Status findByIdStatus(long id);
}
