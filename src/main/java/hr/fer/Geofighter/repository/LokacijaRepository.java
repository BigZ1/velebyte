package hr.fer.Geofighter.repository;

import hr.fer.Geofighter.domain.*;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
public interface LokacijaRepository extends JpaRepository<Lokacija, Long> {
    @Modifying
    @Transactional
    @Query("UPDATE Lokacija u SET u.geoCordlng = ?1, u.geoCordlat = ?2, u.opisLokacije = ?3, u.urlFotografije = ?4, u.karta = ?5, u.status = ?6," +
            "u.drzava = ?7, u.grad = ?8, u.kontinent = ?9, u.tip = ?10, u.radijus = ?12 WHERE u.IDLokacije = ?11")
    void updateLokacija(double geoCordLng, double geoCordLat, String opisLokacije, byte[] urlFotografije, Karta karta, Status status,
                        Drzava drzava, Grad grad, Kontinent kontinent, Tip tip, long IDLokacije, double radijus);

    @Query("SELECT u FROM Lokacija u WHERE u.status = ?1")
    List<Lokacija> getLokacijaByStatus(Status status);

    @Query("SELECT u FROM Lokacija u WHERE u.IDLokacije = ?1")
    Lokacija getLokacijaByID(long IDLokacije);
}
