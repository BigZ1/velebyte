package hr.fer.Geofighter.repository;

import hr.fer.Geofighter.domain.Karta;
import hr.fer.Geofighter.domain.Lokacija;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface KartaRepository extends JpaRepository <Karta, Long> {

    @Query("SELECT l FROM Lokacija l WHERE l.karta.IDKarte = ?1")
    List<Lokacija> dohvatiLokaciju(long ID);

    @Query("SELECT l FROM Karta l WHERE l.IDKarte = ?1")
    Karta findByIDKarte(long ID);

}
