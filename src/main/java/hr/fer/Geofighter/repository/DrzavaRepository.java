package hr.fer.Geofighter.repository;

import hr.fer.Geofighter.domain.Drzava;
import hr.fer.Geofighter.domain.Kontinent;
import hr.fer.Geofighter.domain.Korisnik;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface DrzavaRepository extends JpaRepository<Drzava, Long> {

    @Query("SELECT u FROM Drzava u WHERE u.imeDrzave = ?1")
    List<Drzava> findByImeDrzave(String imeDrzave);

    @Query("SELECT u FROM Drzava u WHERE u.imeDrzave = ?1")
    Drzava findByIme(String imeDrzave);

}
