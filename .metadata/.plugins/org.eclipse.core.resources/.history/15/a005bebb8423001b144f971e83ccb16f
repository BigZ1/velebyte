package hr.fer.Geofighter.web.rest.controller;


import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import hr.fer.Geofighter.service.IgracService;
import hr.fer.Geofighter.service.KartografService;
import hr.fer.Geofighter.web.rest.dto.RegistracijaIgracaDto;
import hr.fer.Geofighter.web.rest.dto.RegistracijaKartografaDto;
import hr.fer.Geofighter.web.rest.dto.RegistracijskiOdgovorDto;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RestController
@RequestMapping("/geofighter")
@RequiredArgsConstructor
public class RegistracijskiController {
	
	private IgracService igracService;
	private KartografService kartografService;
	
	@PostMapping("/registracija")
	public RegistracijskiOdgovorDto registrirajIgraca(@RequestBody RegistracijaIgracaDto registracijaIgracaDto) {
		
		igracService.dodajIgraca(registracijaIgracaDto);
		igracService.posaljiMail(registracijaIgracaDto.getEmail(), registracijaIgracaDto.getKorisnickoIme());
		
		RegistracijskiOdgovorDto odg = new RegistracijskiOdgovorDto();
		odg.setEmail(registracijaIgracaDto.getEmail());
		
		boolean emailPostoji = igracService.postojiEmail(registracijaIgracaDto.getEmail());
		boolean korisnickoImePostoji = igracService.postojiKorisnik(registracijaIgracaDto.getKorisnickoIme());
		
		if(emailPostoji)
			odg.setOpis("Postoji email!");
		else if(korisnickoImePostoji)
			odg.setOpis("Postoji korisnicko ime!");
		else
			odg.setOpis(null);
		
		return odg;	
	}
	
	@GetMapping(value = "/verification", params = {"token", "username"})
	public String verifyEmail(@RequestParam(name = "token") String token, @RequestParam(name = "korisnickoIme") String korisnickoIme) {
		if(igracService.potvrdiEmail(korisnickoIme, token))
			return "Uspijesno ste potvrdili registraciju";
		
		return "Potvrda registracije nije uspijela";
	}
	
	@PostMapping("/registracijaKartografa")
	public RegistracijskiOdgovorDto registracijaKartografa(@RequestBody RegistracijaKartografaDto registracijaKartografaDto){
		
		kartografService.dodajKartografa(registracijaKartografaDto);
		kartografService.posaljiMail(registracijaKartografaDto.getEmail(), registracijaKartografaDto.getKorisnickoIme());
		
		RegistracijskiOdgovorDto odg = new RegistracijskiOdgovorDto();
		odg.setEmail(registracijaKartografaDto.getEmail());
		
		boolean emailPostoji = igracService.postojiEmail(registracijaKartografaDto.getEmail());
		boolean korisnickoImePostoji = igracService.postojiKorisnik(registracijaKartografaDto.getKorisnickoIme());		
		
		if(emailPostoji)
			odg.setOpis("Postoji email!");
		else if(korisnickoImePostoji)
			odg.setOpis("Postoji korisnicko ime!");
		else
			odg.setOpis(null);
		
		return odg;
	}

}
