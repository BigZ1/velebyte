package hr.fer.Geofigther.service;

import java.util.List;

import org.springframework.stereotype.Service;

import hr.fer.Geofighter.domain.Korisnik;
import hr.fer.Geofighter.domain.Uloga;
import hr.fer.Geofighter.repository.IgracRepository;
import hr.fer.Geofighter.web.rest.dto.RegistracijaIgracaDto;
import lombok.AllArgsConstructor;

//@AllArgsConstructor
@Service
public class IgracService {
	
	private SlanjeEmailaService slanjeEmaila;
	private IgracRepository igracRepository;
	
	public IgracService() {
	}

	public boolean postojiKorisnik(String korisnickoIme) {
		return igracRepository.findByKorisnickoIme(korisnickoIme).isEmpty();
	}
	
	public boolean postojiEmail(String email) {
		return igracRepository.findByEmail(email).isEmpty();
	}
	
	public boolean verificiraniKorisnik(String korisnickoIme) {
		
		List<Korisnik> k = igracRepository.findByKorisnickoIme(korisnickoIme);
		
		//TODO: pitati zasto nema gettera i settera iako smo dodatli @Data iznad Korisnika
		if(!k.isEmpty())
			return k.get(0).isVerified();
			
		return false;
	}
	
	public boolean bananiKorisnik(String korisnickoIme) {
		List<Korisnik> k = igracRepository.findByKorisnickoIme(korisnickoIme);
		
		if(!k.isEmpty())
			return k.get(0).isBanned();
		
		return false;
	}
	
	public void posaljiMail(String email, String korisnickoIme) {
		slanjeEmaila.posaljiPotvrduRegistracije(email, korisnickoIme.hashCode(), korisnickoIme);
	}
	
	public boolean potvrdiEmail(String korisnickoIme, String token) {
		Korisnik k = igracRepository.findByKorisnickoIme(korisnickoIme).get(0);
		
		if(korisnickoIme.hashCode() == Integer.parseInt(token))
			k.setVerified(true);
		
		return igracRepository.save(k).isVerified();
	}
	
	public void dodajIgraca(RegistracijaIgracaDto registracijaIgracaDto) {
		if(!postojiKorisnik(registracijaIgracaDto.getKorisnickoIme()))
			throw new IllegalArgumentException();
		
		if(!postojiEmail(registracijaIgracaDto.getEmail()))
			throw new IllegalArgumentException();
		
		Korisnik k = new Korisnik();
		k.setKorisnickoIme(registracijaIgracaDto.getKorisnickoIme());
		k.setPassword(registracijaIgracaDto.getSazetakLozinke());
		k.setEmail(registracijaIgracaDto.getEmail());
		k.setUrlFotografije(registracijaIgracaDto.getKorisnickaSlika());
		k.setBanned(false);
		k.setELO("100");
		k.setExparation(null);
		k.setIDKorisnika(igracRepository.findAll().size() + 1);
		
		Uloga uloga = new Uloga();
		uloga.setIDUloge(0);
		uloga.setImeUloge("Player");
		
		k.setUlogaKorisnika(uloga);
		k.setVerified(false);
	}
	
}
