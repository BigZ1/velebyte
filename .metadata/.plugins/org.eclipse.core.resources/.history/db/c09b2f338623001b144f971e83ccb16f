package hr.fer.Geofigther.domain;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

import javafx.util.Pair;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Lokacija {

	private Pair<Double, Double> geoCord;
	private String opisLokacije;
	private long brojPosjeta;
	private String urlFotografije;
	
	@OneToOne
	private Karta karta;
	
	@OneToOne
	private Status status;
	
	@ManyToOne
	private Drzava drazava;
	
	@ManyToOne
	private Grad grad;
	
	@ManyToOne
	private Kontinent kontinent;
	
	@ManyToOne
	private Tip tip;

	public Pair<Double, Double> getGeoCord() {
		return geoCord;
	}

	public void setGeoCord(Pair<Double, Double> geoCord) {
		this.geoCord = geoCord;
	}

	public String getOpisLokacije() {
		return opisLokacije;
	}

	public void setOpisLokacije(String opisLokacije) {
		this.opisLokacije = opisLokacije;
	}

	public long getBrojPosjeta() {
		return brojPosjeta;
	}

	public void setBrojPosjeta(long brojPosjeta) {
		this.brojPosjeta = brojPosjeta;
	}

	public String getUrlFotografije() {
		return urlFotografije;
	}

	public void setUrlFotografije(String urlFotografije) {
		this.urlFotografije = urlFotografije;
	}

	public Karta getKarta() {
		return karta;
	}

	public void setKarta(Karta karta) {
		this.karta = karta;
	}

	public Status getStatus() {
		return status;
	}

	public void setStatus(Status status) {
		this.status = status;
	}

	public Drzava getDrazava() {
		return drazava;
	}

	public void setDrazava(Drzava drazava) {
		this.drazava = drazava;
	}

	public Grad getGrad() {
		return grad;
	}

	public void setGrad(Grad grad) {
		this.grad = grad;
	}

	public Kontinent getKontinent() {
		return kontinent;
	}

	public void setKontinent(Kontinent kontinent) {
		this.kontinent = kontinent;
	}

	public Tip getTip() {
		return tip;
	}

	public void setTip(Tip tip) {
		this.tip = tip;
	}
	
}
